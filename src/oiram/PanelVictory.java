/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author Hendardi
 */
public class PanelVictory extends javax.swing.JPanel {

    /**
     * Creates new form PanelVictory
     */
    int ctrchat=0;
    int turnchat=0;
    int CTRGerakMario0=0;
    int waktu=0;
    int ctrjalankredit=0;
    int ctrevolve=0;
    public PanelVictory() {
        initComponents();
        this.setSize(850,650);
        
        Credit.setVisible(false);
        Nama.setVisible(false);
        Nama2.setVisible(false);
        Nama3.setVisible(false);
        
        LabelBackground.setSize(850, 650);
        LabelBackground.setLocation(0, 0);
        ImageIcon rawImg = new ImageIcon("Image/Black.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
        
        Percakapan.setText("");
        
        LabelMario.setSize(50,50);
        LabelMario.setLocation(0,580);
        rawImg = new ImageIcon("Image/Mario/dmariokanan0.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelMario.setIcon(finalimg);
        
        LabelMario2.setSize(50,50);
        LabelMario2.setLocation(800,580);
        rawImg = new ImageIcon("Image/Mario/mariokiri0.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelMario2.getWidth(), LabelMario2.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelMario2.setIcon(finalimg);
        
        LabelTalk.setSize(300,100);
        rawImg = new ImageIcon("Image/chatroom.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelTalk.getWidth(), LabelTalk.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelTalk.setIcon(finalimg);
        LabelTalk.setLocation(0, 480);
    }
    
    public void cetakfinalchat()
    {
        if(turnchat==0)
        {
        String []tampung={"We"," Did"," It ", "Mario"};
        Percakapan.setLocation(20, 470);
        Percakapan.setFont(new Font("Consolas", Font.BOLD, 30));
        Percakapan.setSize(500,100);
        Percakapan.setText("");
            Timer timerChattingMario = new Timer(300,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 Percakapan.setText(Percakapan.getText()+(tampung[ctrchat]));
                 ctrchat++;
                 if(ctrchat==4)
                 {
                     ((Timer)e.getSource()).stop();
                     ctrchat=0;
                 }
            }
        });
        timerChattingMario.start();
        turnchat++;
        }
        else if(turnchat==1)
        {
            LabelTalk.setSize(400,100);
            Percakapan.setText(" ");
            String []tampung={"Yes ","We ","Did ","It ", "Oiram"};
            ImageIcon rawImg = new ImageIcon("Image/chatroom1.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelTalk.getWidth(), LabelTalk.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelTalk.setIcon(finalimg);
            LabelTalk.setLocation(440, 480);
            
            
            Percakapan.setLocation(470, 470);
            Percakapan.setFont(new Font("Consolas", Font.BOLD, 30));
            Percakapan.setSize(500,100);
            Percakapan.setText("");
            Timer timerChattingMario = new Timer(300,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 Percakapan.setText(Percakapan.getText()+(tampung[ctrchat]));
                 ctrchat++;
                 if(ctrchat==5)
                 {
                     ((Timer)e.getSource()).stop();
                     ctrchat=0;
                 }
            }
        });
        timerChattingMario.start();
        turnchat++;
        }
        else if(turnchat==2)
        {
            LabelTalk.setSize(300,100);
            ImageIcon rawImg = new ImageIcon("Image/chatroom.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelTalk.getWidth(), LabelTalk.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelTalk.setIcon(finalimg);
            LabelTalk.setLocation(0, 480);
            
            Percakapan.setText("");
            String []tampung={"The ","Earth ","Is ", "Safe"};
            Percakapan.setLocation(0, 470);
            Percakapan.setFont(new Font("Consolas", Font.BOLD, 30));
            Percakapan.setSize(500,100);
            Percakapan.setText("");
                Timer timerChattingMario = new Timer(300,new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                     Percakapan.setText(Percakapan.getText()+(tampung[ctrchat]));
                     ctrchat++;
                     if(ctrchat==4)
                     {
                         ((Timer)e.getSource()).stop();
                         ctrchat=0;
                     }
                }
            });
            timerChattingMario.start();
            turnchat++;
        }
        else if(turnchat==3)
        {
            Percakapan.setText("");
            String []tampung={"For ","Now "};
            Percakapan.setLocation(10, 470);
            Percakapan.setFont(new Font("Consolas", Font.BOLD, 30));
            Percakapan.setSize(500,100);
            Percakapan.setText("");
                Timer timerChattingMario = new Timer(300,new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                     Percakapan.setText(Percakapan.getText()+(tampung[ctrchat]));
                     ctrchat++;
                     if(ctrchat==2)
                     {
                         ((Timer)e.getSource()).stop();
                         ctrchat=0;
                     }
                }
            });
            timerChattingMario.start();
            turnchat++;
        }
        else if(turnchat==4)
        {
            LabelTalk.setSize(200,100);
            Percakapan.setText(" ");
            ImageIcon rawImg = new ImageIcon("Image/chatroom1.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelTalk.getWidth(), LabelTalk.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelTalk.setIcon(finalimg);
            LabelTalk.setLocation(620, 480);
            
            Percakapan.setText("");
            String []tampung={"It's ","Okay "};
            Percakapan.setLocation(630, 470);
            Percakapan.setFont(new Font("Consolas", Font.BOLD, 30));
            Percakapan.setSize(500,100);
            Percakapan.setText("");
                Timer timerChattingMario = new Timer(300,new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                     Percakapan.setText(Percakapan.getText()+(tampung[ctrchat]));
                     ctrchat++;
                     if(ctrchat==2)
                     {
                         ((Timer)e.getSource()).stop();
                         ctrchat=0;
                     }
                }
            });
            timerChattingMario.start();
            turnchat++;
        }
        else if(turnchat==5)
        {
            LabelTalk.setSize(400,100);
            Percakapan.setText(" ");
            ImageIcon rawImg = new ImageIcon("Image/chatroom1.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelTalk.getWidth(), LabelTalk.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelTalk.setIcon(finalimg);
            LabelTalk.setLocation(450, 480);
            
            Percakapan.setText("");
            String []tampung={"Let's ","Combine ","Our ","Power"};
            Percakapan.setLocation(460, 470);
            Percakapan.setFont(new Font("Consolas", Font.BOLD, 30));
            Percakapan.setSize(500,100);
            Percakapan.setText("");
                Timer timerChattingMario = new Timer(300,new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                     Percakapan.setText(Percakapan.getText()+(tampung[ctrchat]));
                     ctrchat++;
                     if(ctrchat==4)
                     {
                         ((Timer)e.getSource()).stop();
                         ctrchat=0;
                     }
                }
            });
            timerChattingMario.start();
            turnchat++;
        }
        else if(turnchat==6)
        {
            Percakapan.setText("");
            String []tampung={"To ","Protect ","Our ","World "};
            Percakapan.setLocation(460, 470);
            Percakapan.setFont(new Font("Consolas", Font.BOLD, 30));
            Percakapan.setSize(500,100);
            Percakapan.setText("");
                Timer timerChattingMario = new Timer(300,new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                     Percakapan.setText(Percakapan.getText()+(tampung[ctrchat]));
                     ctrchat++;
                     if(ctrchat==4)
                     {
                         ((Timer)e.getSource()).stop();
                         ctrchat=0;
                     }
                }
            });
            timerChattingMario.start();
            turnchat++;
        }
        else if(turnchat==7)
        {
            Percakapan.setText("");
            LabelTalk.setSize(0, 0);
            Timer jalanbersama = new Timer(100,new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    LabelMario.setLocation(LabelMario.getX()+10, LabelMario.getY());
                    ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan"+CTRGerakMario0+".png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelMario.setIcon(finalimg);
                    
                    LabelMario2.setLocation(LabelMario2.getX()-10, LabelMario.getY());
                    rawImg = new ImageIcon("Image/Mario/mariokiri"+CTRGerakMario0+".png");
                    img = rawImg.getImage();
                    newImg = img.getScaledInstance(LabelMario2.getWidth(), LabelMario2.getHeight(), Image.SCALE_SMOOTH);
                    finalimg = new ImageIcon(newImg);
                    LabelMario2.setIcon(finalimg);

                    CTRGerakMario0++;
                    
                    if(CTRGerakMario0==5)
                    {
                        CTRGerakMario0=0;
                    }
                    if(LabelMario.getX()==LabelMario2.getX())
                    {
                        ((Timer)e.getSource()).stop();
                        Timer evolve = new Timer(100,new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                LabelMario2.setSize(0, 0);
                                LabelMario.setSize(70,70);
                                LabelMario.setLocation(LabelMario.getX(),560);
                                ImageIcon rawImg = new ImageIcon("Image/Mario/starmariokanan"+CTRGerakMario0+".png");
                                Image img = rawImg.getImage();
                                Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                ImageIcon finalimg = new ImageIcon(newImg);
                                LabelMario.setIcon(finalimg);
                                CTRGerakMario0++;
                                if(waktu==19)
                                {
                                    rawImg = new ImageIcon("Image/White.png");
                                    img = rawImg.getImage();
                                    newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
                                    finalimg = new ImageIcon(newImg);
                                    LabelBackground.setIcon(finalimg);
                                }
                                if(waktu==20)
                                {
                                    ((Timer)e.getSource()).stop();
                                    waktu=0;
                                    rawImg = new ImageIcon("Image/Mario/starmariokanan"+CTRGerakMario0+".png");
                                    img = rawImg.getImage();
                                    newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                    finalimg = new ImageIcon(newImg);
                                    LabelMario.setIcon(finalimg);
                                    
                                }
                                if(CTRGerakMario0==10)
                                {
                                    CTRGerakMario0=0;
                                }
                                waktu++;
                            }
                        });
                        evolve.start();
                    }
                }
            });
            jalanbersama.start();
            /*Percakapan.setText("");
            String []tampung={"From ","Bowser ","And ","His ","Servants"};
            Percakapan.setLocation(460, 470);
            Percakapan.setFont(new Font("Consolas", Font.BOLD, 30));
            Percakapan.setSize(500,100);
            Percakapan.setText("");
                Timer timerChattingMario = new Timer(300,new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                     Percakapan.setText(Percakapan.getText()+(tampung[ctrchat]));
                     ctrchat++;
                     if(ctrchat==5)
                     {
                         ((Timer)e.getSource()).stop();
                         ctrchat=0;
                     }
                }
            });
            timerChattingMario.start();
            turnchat++;*/
        turnchat++;
        }
        else if(turnchat==8)
        {
            
            Nama.setVisible(true);
            Nama2.setVisible(true);
            Nama3.setVisible(true);
            Credit.setVisible(true);
            
            LabelMario.setSize(0,0);
            
            
            Credit.setSize(200,100);
            Credit.setLocation(-300, 30);
            Credit.setText("Credit To");
            Credit.setFont(new Font("Courier New", Font.BOLD, 30));
            Credit.setForeground(Color.black);
            
            
            Nama.setSize(500,100);
            Nama.setText("Gidion Septian K.");
            Nama.setFont(new Font("Courier New", Font.BOLD, 30));
            Nama.setLocation(-300, 130);
            
            Nama2.setSize(500,100);
            Nama2.setText("Hendardi Agung C.");
            Nama2.setFont(new Font("Courier New", Font.BOLD, 30));
            Nama2.setLocation(-300, 230);
            
            Nama3.setSize(500,100);
            Nama3.setText("Michael Christopher ");
            Nama3.setFont(new Font("Courier New", Font.BOLD, 30));
            Nama3.setLocation(-300,330);
            
            Timer jalancredit = new Timer(100, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                        if(Credit.getX()<320)
                        {
                            Credit.setLocation(Credit.getX()+20, Credit.getY());
                        }
                        if(Nama.getX()<260)
                        {
                            Nama.setLocation(Nama.getX()+20, Nama.getY());
                        }
                        if(Nama2.getX()<260)
                        {
                            Nama2.setLocation(Nama2.getX()+20, Nama2.getY());
                        }
                        if(Nama3.getX()<260)
                        {
                            Nama3.setLocation(Nama3.getX()+20, Nama3.getY());
                        }
                        
                        
                        if(Credit.getX()==320)
                        {
                            Credit.setLocation(320, 30);
                        }
                        if(Nama.getX()==320)
                        {
                            Nama.setLocation(320,Nama.getY());
                        }
                        if(Nama2.getX()==320)
                        {
                            Nama2.setLocation(320,Nama2.getY());                            
                        }
                        if(Nama3.getX()==320)
                        {
                            Nama3.setLocation(320, Nama3.getY());
                        }
                    ctrjalankredit++;
                }
            });
            jalancredit.start();

            
            
            
            
            
                        
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Nama3 = new javax.swing.JLabel();
        Nama2 = new javax.swing.JLabel();
        Nama = new javax.swing.JLabel();
        Credit = new javax.swing.JLabel();
        Percakapan = new javax.swing.JLabel();
        LabelTalk = new javax.swing.JLabel();
        LabelMario2 = new javax.swing.JLabel();
        LabelMario = new javax.swing.JLabel();
        LabelBackground = new javax.swing.JLabel();

        setLayout(null);

        Nama3.setText("Nama3");
        add(Nama3);
        Nama3.setBounds(320, 180, 50, 20);

        Nama2.setText("Nama2");
        add(Nama2);
        Nama2.setBounds(250, 250, 50, 20);

        Nama.setText("Nama");
        add(Nama);
        Nama.setBounds(250, 180, 120, 70);

        Credit.setText("Credit To");
        add(Credit);
        Credit.setBounds(260, 110, 130, 80);

        Percakapan.setText("Percakapan");
        add(Percakapan);
        Percakapan.setBounds(170, 230, 70, 20);

        LabelTalk.setText("LabelTalk");
        add(LabelTalk);
        LabelTalk.setBounds(30, 230, 67, 20);

        LabelMario2.setText("LabelMario2");
        add(LabelMario2);
        LabelMario2.setBounds(290, 10, 100, 90);

        LabelMario.setText("LabelMario");
        add(LabelMario);
        LabelMario.setBounds(10, 10, 100, 90);

        LabelBackground.setText("LabelBackground");
        add(LabelBackground);
        LabelBackground.setBounds(140, 90, 100, 20);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Credit;
    private javax.swing.JLabel LabelBackground;
    private javax.swing.JLabel LabelMario;
    private javax.swing.JLabel LabelMario2;
    private javax.swing.JLabel LabelTalk;
    private javax.swing.JLabel Nama;
    private javax.swing.JLabel Nama2;
    private javax.swing.JLabel Nama3;
    private javax.swing.JLabel Percakapan;
    // End of variables declaration//GEN-END:variables
}
