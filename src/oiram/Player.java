/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.io.Serializable;

/**
 *
 * @author Hendardi
 */
public class Player implements Serializable {
    int hp;
    int hpMax;
    int attack;
    int x;
    int y;
    int uang;
    //hari = 0 = siang, hari = 1 = malam
    int hari;
    int waktu = 0;
    double waktuDecimal;
    boolean boolClearFireDungeon;
    boolean boolClearIceDungeon;
    boolean coldDrink;
    int arrItem[] = new int[3];
    boolean arrQuest[] = new boolean[3];
    
    Player(int x, int y){
        this.hari = 1;
        this.waktu = 0;
        this.waktuDecimal = 0;
        this.hp = 100;
        this.hpMax = 100;
        this.attack = 20;
        this.x = x;
        this.y = y;
        this.uang = 600;
        this.coldDrink = false;
        this.boolClearFireDungeon = false;
        this.boolClearIceDungeon = false;
        for (int i = 0; i < arrItem.length; i++) {
            this.arrItem[i] = 0;
        }
        for (int i = 0; i < arrQuest.length; i++) {
            this.arrQuest[i] = false;
        }
    }
    
    Player(Player p){
        this.hari = p.hari;
        this.waktu = p.waktu;
        this.waktuDecimal = p.waktuDecimal;
        this.hp = p.hp;
        this.hpMax = p.hpMax;
        this.attack = p.attack;
        this.x = p.x;
        this.y = p.y;
        this.uang = p.uang;
        this.coldDrink = p.coldDrink;
        this.boolClearFireDungeon = p.boolClearFireDungeon;
        this.boolClearIceDungeon = p.boolClearIceDungeon;
        for (int i = 0; i < arrItem.length; i++) {
            this.arrItem[i] = p.arrItem[i];
        }
        for (int i = 0; i < arrQuest.length; i++) {
            this.arrQuest[i] = p.arrQuest[i];
        }
    }
    
    public void InstantKill(Musuh m){
        
    }
}