/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Hendardi
 */
public class MainMenu extends javax.swing.JPanel {

    /**
     * Creates new form MainMenu
     */
    int panjang = 850;
    int lebar = 650;
    boolean ganti = false;
    MainMenuThread MainMenuThread = new MainMenuThread();
    
    public MainMenu() {
        initComponents();
        this.setSize(panjang,lebar);
        
        //LabelBackground
        LabelBackground.setSize(panjang,lebar);
        LabelBackground.setLocation(0,0);
        ImageIcon rawImg = new ImageIcon("Image/Main Menu.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
        
        //LabelNewgame
        LabelNewGame.setSize(500,100);
        LabelNewGame.setLocation(panjang/2-120,lebar/2-35);
        LabelNewGame.setText("NEW GAME");
        LabelNewGame.setFont(new Font("Courier New", Font.BOLD, 50));
        LabelNewGame.setForeground(Color.white);
        
        //LabelContinue
        LabelContinue.setSize(500,100);
        LabelContinue.setLocation(panjang/2-120,lebar/2+35);
        LabelContinue.setText("CONTINUE");
        LabelContinue.setFont(new Font("Courier New", Font.BOLD, 50));
        LabelContinue.setForeground(Color.black);
        
        //LabelSelect
        LabelSelect.setSize(70,70);
        LabelSelect.setLocation(panjang/2-190,lebar/2-25);
        rawImg = new ImageIcon("Image/Select.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelSelect.getWidth(), LabelSelect.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelSelect.setIcon(finalimg);
        
        //LabelUnit
        LabelUnit.setSize(50,50);
        LabelUnit.setLocation(-250,lebar-155);
        rawImg = new ImageIcon("Image/Mario/mariokanan0.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelUnit.getWidth(), LabelUnit.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelUnit.setIcon(finalimg);
        
        //Thread
        MainMenuThread.start();
    }
    
    class MainMenuThread extends Thread{
        public void run(){
            int langkah = 0;
            
            try {
                while(true){
                    LabelUnit.setLocation(LabelUnit.getX()+5, LabelUnit.getY());
                    
                    if (ganti==false) {
                        if (langkah%9 == 0) {
                            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokanan0.png");
                            Image img = rawImg.getImage();
                            Image newImg = img.getScaledInstance(LabelUnit.getWidth(), LabelUnit.getHeight(), Image.SCALE_SMOOTH);
                            ImageIcon finalimg = new ImageIcon(newImg);
                            LabelUnit.setIcon(finalimg);
                        }
                        else if (langkah%9 == 4) {
                            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokanan1.png");
                            Image img = rawImg.getImage();
                            Image newImg = img.getScaledInstance(LabelUnit.getWidth(), LabelUnit.getHeight(), Image.SCALE_SMOOTH);
                            ImageIcon finalimg = new ImageIcon(newImg);
                            LabelUnit.setIcon(finalimg);
                        }
                        else if (langkah%9 == 8) {
                            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokanan2.png");
                            Image img = rawImg.getImage();
                            Image newImg = img.getScaledInstance(LabelUnit.getWidth(), LabelUnit.getHeight(), Image.SCALE_SMOOTH);
                            ImageIcon finalimg = new ImageIcon(newImg);
                            LabelUnit.setIcon(finalimg);
                        }
                        
                        if (LabelUnit.getX() >= panjang+100) {
                            LabelUnit.setLocation(-50, LabelUnit.getY());
                            ganti = true;
                        }
                        Thread.sleep(50);
                    }
                    else {
                        ImageIcon rawImg = new ImageIcon("Image/Unit/mushroom.png");
                        Image img = rawImg.getImage();
                        Image newImg = img.getScaledInstance(LabelUnit.getWidth(), LabelUnit.getHeight(), Image.SCALE_SMOOTH);
                        ImageIcon finalimg = new ImageIcon(newImg);
                        LabelUnit.setIcon(finalimg);
                        
                        if(LabelUnit.getX()>=panjang+100){
                            LabelUnit.setLocation(-50, LabelUnit.getY());
                            ganti = false;
                        }
                        Thread.sleep(30);
                    }
                    langkah++;
                }
            }
            catch (Exception e) {
                
            }
        }
    }
    
    //Function
    public int getYSelect(){
        return LabelSelect.getY();
    }
    
    //Procedure
    public void Atas(){
        LabelSelect.setLocation(panjang/2-190,lebar/2-25);
        LabelNewGame.setForeground(Color.white);
        LabelContinue.setForeground(Color.black);
    }
    
    public void Bawah(){
        LabelSelect.setLocation(panjang/2-190,lebar/2+45);
        LabelNewGame.setForeground(Color.black);
        LabelContinue.setForeground(Color.white);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LabelNewGame = new javax.swing.JLabel();
        LabelContinue = new javax.swing.JLabel();
        LabelSelect = new javax.swing.JLabel();
        LabelUnit = new javax.swing.JLabel();
        LabelBackground = new javax.swing.JLabel();

        setLayout(null);

        LabelNewGame.setText("LabelNewGame");
        add(LabelNewGame);
        LabelNewGame.setBounds(10, 40, 110, 20);

        LabelContinue.setText("LabelContinue");
        add(LabelContinue);
        LabelContinue.setBounds(10, 70, 100, 20);

        LabelSelect.setText("LabelSelect");
        add(LabelSelect);
        LabelSelect.setBounds(10, 100, 80, 20);

        LabelUnit.setText("LabelUnit");
        add(LabelUnit);
        LabelUnit.setBounds(10, 130, 80, 20);

        LabelBackground.setText("LabelBackground");
        add(LabelBackground);
        LabelBackground.setBounds(10, 10, 120, 20);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelBackground;
    private javax.swing.JLabel LabelContinue;
    private javax.swing.JLabel LabelNewGame;
    private javax.swing.JLabel LabelSelect;
    private javax.swing.JLabel LabelUnit;
    // End of variables declaration//GEN-END:variables
}
