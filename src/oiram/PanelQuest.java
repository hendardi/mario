/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author Hendardi
 */
public class PanelQuest extends javax.swing.JPanel {

    /**
     * Creates new form WorldMap
     */
    int panjang = 850;
    int lebar = 650;
    int countdown = MainFrame.waktuReset;
    
    Timer timerCountdown = new Timer(100, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelCountdown.setText("Countdown : "+(countdown - MainFrame.player.waktu)+"s Left");
        }
    });
    
    public PanelQuest() {
        initComponents();
        this.setSize(panjang, lebar);
        
        //LabelBackground
        LabelBackground.setSize(panjang, lebar);
        ImageIcon rawImg = new ImageIcon("Image/Map/Quest.jpg");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
        LabelBackground.setLocation(0,0);
        
        //LabelQuestList
        LabelQuestList.setSize(300, 70);
        LabelQuestList.setLocation(275, 0);
        LabelQuestList.setText("Quest List");
        LabelQuestList.setFont(new Font("Courier New", Font.BOLD, 50));
        LabelQuestList.setForeground(Color.black);
        
        //LabelExploreFire
        LabelExploreFire.setSize(400, 100);
        LabelExploreFire.setLocation(150, 100);
        LabelExploreFire.setText("Enter Fire Dungeon");
        LabelExploreFire.setFont(new Font("Courier New", Font.BOLD, 25));
        LabelExploreFire.setForeground(Color.black);
        
        //LabelExploreIce
        LabelExploreIce.setSize(400, 100);
        LabelExploreIce.setLocation(150, 150);
        LabelExploreIce.setText("Enter Ice Dungeon");
        LabelExploreIce.setFont(new Font("Courier New", Font.BOLD, 25));
        LabelExploreIce.setForeground(Color.black);
        
        //LabelExploreBoss
        LabelExploreBoss.setSize(400, 100);
        LabelExploreBoss.setLocation(150, 200);
        LabelExploreBoss.setText("Enter Boss Dungeon");
        LabelExploreBoss.setFont(new Font("Courier New", Font.BOLD, 25));
        LabelExploreBoss.setForeground(Color.black);
        
        //LabelTakeExploreFire
        LabelTakeExploreFire.setSize(100, 50);
        LabelTakeExploreFire.setLocation(600, 125);
        LabelTakeExploreFire.setText("TAKE");
        LabelTakeExploreFire.setFont(new Font("Courier New", Font.BOLD, 25));
        LabelTakeExploreFire.setForeground(Color.black);
        
        //LabelTakeExploreIce
        LabelTakeExploreIce.setSize(100, 50);
        LabelTakeExploreIce.setLocation(600, 175);
        LabelTakeExploreIce.setText("TAKE");
        LabelTakeExploreIce.setFont(new Font("Courier New", Font.BOLD, 25));
        LabelTakeExploreIce.setForeground(Color.black);
        
        //LabelTakeExploreBoss
        LabelTakeExploreBoss.setSize(100, 50);
        LabelTakeExploreBoss.setLocation(600, 225);
        LabelTakeExploreBoss.setText("TAKE");
        LabelTakeExploreBoss.setFont(new Font("Courier New", Font.BOLD, 25));
        LabelTakeExploreBoss.setForeground(Color.black);
        
        //LabelCountDown
        LabelCountdown.setSize(500, 50);
        LabelCountdown.setLocation(150, 500);
        LabelCountdown.setText("Countdown : 0s Left");
        LabelCountdown.setFont(new Font("Courier New", Font.BOLD, 25));
        LabelCountdown.setForeground(Color.black);
        
        //LabelExit
        LabelExit.setSize(100, 50);
        LabelExit.setLocation(600, 500);
        LabelExit.setText("EXIT");
        LabelExit.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelExit.setForeground(Color.black);
    }
    
    public void resetQuest(){
        timerCountdown.stop();
        LabelCountdown.setText("Countdown : 0s Left");
        for (int i = 0; i < MainFrame.player.arrQuest.length; i++) {
            MainFrame.player.arrQuest[i] = false;
        }
        LabelExploreFire.setForeground(Color.black);
        LabelExploreIce.setForeground(Color.black);
        LabelExploreBoss.setForeground(Color.black);
        LabelTakeExploreFire.setEnabled(true);
        LabelTakeExploreIce.setEnabled(true);
        LabelTakeExploreBoss.setEnabled(true);
    }
    
    public void resetFire(){
        LabelExploreFire.setForeground(Color.black);
        LabelTakeExploreFire.setEnabled(true);
    }
    
    public void resetIce(){
        LabelExploreIce.setForeground(Color.black);
        LabelTakeExploreIce.setEnabled(true);
    }
    
    public void resetBoss(){
        LabelExploreBoss.setForeground(Color.black);
        LabelTakeExploreBoss.setEnabled(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LabelCountdown = new javax.swing.JLabel();
        LabelQuestList = new javax.swing.JLabel();
        LabelExploreFire = new javax.swing.JLabel();
        LabelExploreIce = new javax.swing.JLabel();
        LabelExploreBoss = new javax.swing.JLabel();
        LabelTakeExploreFire = new javax.swing.JLabel();
        LabelTakeExploreIce = new javax.swing.JLabel();
        LabelTakeExploreBoss = new javax.swing.JLabel();
        LabelExit = new javax.swing.JLabel();
        LabelBackground = new javax.swing.JLabel();

        setLayout(null);

        LabelCountdown.setText("LabelCountdown");
        add(LabelCountdown);
        LabelCountdown.setBounds(20, 260, 118, 20);

        LabelQuestList.setText("LabelQuestList");
        add(LabelQuestList);
        LabelQuestList.setBounds(160, 20, 102, 20);

        LabelExploreFire.setText("LabelExploreFire");
        add(LabelExploreFire);
        LabelExploreFire.setBounds(20, 60, 120, 20);

        LabelExploreIce.setText("LabelExploreIce");
        add(LabelExploreIce);
        LabelExploreIce.setBounds(20, 90, 120, 20);

        LabelExploreBoss.setText("LabelExploreBoss");
        add(LabelExploreBoss);
        LabelExploreBoss.setBounds(20, 120, 130, 20);

        LabelTakeExploreFire.setText("LabelTakeExploreFire");
        LabelTakeExploreFire.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelTakeExploreFireMouseClicked(evt);
            }
        });
        add(LabelTakeExploreFire);
        LabelTakeExploreFire.setBounds(230, 60, 150, 20);

        LabelTakeExploreIce.setText("LabelTakeExploreIce");
        LabelTakeExploreIce.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelTakeExploreIceMouseClicked(evt);
            }
        });
        add(LabelTakeExploreIce);
        LabelTakeExploreIce.setBounds(230, 90, 150, 20);

        LabelTakeExploreBoss.setText("LabelTakeExploreBoss");
        LabelTakeExploreBoss.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelTakeExploreBossMouseClicked(evt);
            }
        });
        add(LabelTakeExploreBoss);
        LabelTakeExploreBoss.setBounds(230, 120, 160, 20);

        LabelExit.setText("LabelExit");
        LabelExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelExitMouseClicked(evt);
            }
        });
        add(LabelExit);
        LabelExit.setBounds(320, 260, 70, 20);

        LabelBackground.setText("LabelBackground");
        add(LabelBackground);
        LabelBackground.setBounds(20, 20, 120, 20);
    }// </editor-fold>//GEN-END:initComponents

    private void LabelExitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelExitMouseClicked
        MainFrame MainFrame = (MainFrame)this.getParent().getParent().getParent().getParent();
        MainFrame.ExitInventory();
    }//GEN-LAST:event_LabelExitMouseClicked

    private void LabelTakeExploreFireMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelTakeExploreFireMouseClicked
        LabelTakeExploreFire.setEnabled(false);
        LabelExploreFire.setForeground(Color.white);
        MainFrame.player.arrQuest[0] = true;
        if (!timerCountdown.isRunning()) {
            timerCountdown.start();
        }
    }//GEN-LAST:event_LabelTakeExploreFireMouseClicked

    private void LabelTakeExploreIceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelTakeExploreIceMouseClicked
        LabelTakeExploreIce.setEnabled(false);
        LabelExploreIce.setForeground(Color.white);
        MainFrame.player.arrQuest[1] = true;
        if (!timerCountdown.isRunning()) {
            timerCountdown.start();
        }
    }//GEN-LAST:event_LabelTakeExploreIceMouseClicked

    private void LabelTakeExploreBossMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelTakeExploreBossMouseClicked
        LabelTakeExploreBoss.setEnabled(false);
        LabelExploreBoss.setForeground(Color.white);
        MainFrame.player.arrQuest[2] = true;
        if (!timerCountdown.isRunning()) {
            timerCountdown.start();
        }
    }//GEN-LAST:event_LabelTakeExploreBossMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelBackground;
    private javax.swing.JLabel LabelCountdown;
    private javax.swing.JLabel LabelExit;
    private javax.swing.JLabel LabelExploreBoss;
    private javax.swing.JLabel LabelExploreFire;
    private javax.swing.JLabel LabelExploreIce;
    private javax.swing.JLabel LabelQuestList;
    private javax.swing.JLabel LabelTakeExploreBoss;
    private javax.swing.JLabel LabelTakeExploreFire;
    private javax.swing.JLabel LabelTakeExploreIce;
    // End of variables declaration//GEN-END:variables
}