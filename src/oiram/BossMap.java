/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author Hendardi
 */
public class BossMap extends javax.swing.JPanel {

    /**
     * Creates new form BossMap
     */
    String BossMapTerrain[][] = {
            {"0","0"," "," ","0","0","0","0","E","0","0","0","0"," "," ","0","0"},
            {"0","0"," "," ","0","0","0"," "," "," ","0","0","0"," "," ","0","0"},
            {" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
            {"0","0"," "," ","0","0"," "," "," "," "," ","0","0"," "," ","0","0"},
            {"0","0"," "," ","0","0"," "," "," "," "," ","0","0"," "," ","0","0"},
            {"0","0","0","0","0","0","0"," "," "," ","0","0","0","0","0","0","0"},
            {"0","0","0","0","0","0","0"," "," "," ","0","0","0","0","0","0","0"},
            {"0","0"," "," ","0","0","0"," "," "," ","0","0","0"," "," ","0","0"},
            {"0"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","0"},
            {"0"," "," ","0"," "," "," "," "," "," "," "," "," ","0"," "," ","0"},
            {"0"," "," ","0"," "," "," "," "," "," "," "," "," ","0"," "," ","0"},
            {"0"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","0"},
            {"0"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","0"},
    };
    int panjang = 850;
    int lebar = 650;
    int CTRGerak = 0;
    
    Timer timerGerakPlayerAtas = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() - 10);
            ImageIcon rawImg = new ImageIcon("Image/Mario/marioup"+CTRGerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerBawah = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() + 10);
            ImageIcon rawImg = new ImageIcon("Image/Mario/marioup"+CTRGerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerKiri = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX() - 10, LabelPlayer.getY());
            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokiri"+CTRGerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerKanan = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX() + 10, LabelPlayer.getY());
            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokanan"+CTRGerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTRGerak++;
        }
    });
    
    public BossMap() {
        initComponents();
        this.setSize(panjang, lebar);
        
        //LabelBackground
        LabelBackground.setSize(panjang,lebar);
        LabelBackground.setLocation(0,0);
        ImageIcon rawImg = new ImageIcon("Image/Map/Boss Map.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
        
        //LabelPlayer
        LabelPlayer.setSize(50, 50);
        rawImg = new ImageIcon("Image/Mario/marioup0.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelPlayer.setIcon(finalimg);
        
        //LabelHole
        LabelHole.setSize(150, 150);
        LabelHole.setLocation(350, 0);
        rawImg = new ImageIcon("Image/Transparent.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelHole.getWidth(), LabelHole.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelHole.setIcon(finalimg);
        
        //Dynamic Component
        LabelPlayer.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt){
                System.out.println("click");
           }
        });
    }
    
    //Procedure
    public void setXYLabelPlayer(){
        LabelPlayer.setLocation(MainFrame.player.x * 50, MainFrame.player.y * 50);
    }
    
    public void Atas(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerAtas.start();
    }
    
    public void Bawah(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerBawah.start();
    }
    
    public void Kiri(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerKiri.start();
    }
    
    public void Kanan(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerKanan.start();
    }
    
    public void setHariSiang(){
        ImageIcon rawImg = new ImageIcon("Image/Map/Boss Map.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
    }
    
    public void setHariMalam(){
        ImageIcon rawImg = new ImageIcon("Image/Map/Boss Map Dark.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
    }
    
    public void setImageHole(){
        ImageIcon rawImg = new ImageIcon("Image/Hole.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelHole.getWidth(), LabelHole.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelHole.setIcon(finalimg);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LabelPlayer = new javax.swing.JLabel();
        LabelHole = new javax.swing.JLabel();
        LabelBackground = new javax.swing.JLabel();

        setLayout(null);

        LabelPlayer.setText("LabelPlayer");
        add(LabelPlayer);
        LabelPlayer.setBounds(20, 50, 80, 20);

        LabelHole.setText("LabelHole");
        add(LabelHole);
        LabelHole.setBounds(20, 80, 70, 20);

        LabelBackground.setText("LabelBackground");
        add(LabelBackground);
        LabelBackground.setBounds(20, 20, 130, 20);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelBackground;
    private javax.swing.JLabel LabelHole;
    private javax.swing.JLabel LabelPlayer;
    // End of variables declaration//GEN-END:variables
}
