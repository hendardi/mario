/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author Hendardi
 */
public class FirePuzzle extends javax.swing.JPanel {

    /**
     * Creates new form FirePuzzle
     */
    String FirePuzzleTerrain[][] = {
        {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"},
        {"0", " ", " ", " ", " ", " ", "D", " ", " ", " ", "D", "D", "D", " ", "D", " ", "0"},
        {"0", " ", "D", "D", "D", "D", "D", "D", " ", " ", " ", " ", " ", " ", "D", " ", "0"},
        {"0", " ", "D", " ", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", " ", "0"},
        {"0", "D", "D", " ", "D", " ", "D", "D", "D", "D", "D", "D", "D", "D", "D", " ", "0"},
        {"0", "D", "D", " ", "D", " ", "D", "D", "E", "E", "D", " ", " ", "D", "D", " ", "0"},
        {"0", " ", "D", " ", "D", " ", "D", "D", "E", "E", "D", " ", " ", "D", "D", " ", "0"},
        {"0", " ", "D", "D", "D", "D", "D", "D", " ", "D", "D", "D", " ", "D", "D", " ", "0"},
        {"0", " ", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", " ", "D", "D", " ", "0"},
        {"0", " ", "D", " ", " ", " ", " ", "D", "D", "D", "D", "D", " ", "D", " ", " ", "0"},
        {"0", " ", "D", " ", " ", " ", " ", "D", " ", " ", "D", "D", " ", "D", "D", " ", "0"},
        {"0", "D", "D", "D", "D", "D", "D", "D", " ", " ", "D", "D", " ", " ", " ", " ", "0"},
        {"0", "0", "0", "0", "0", "0", "0", " ", " ", " ", " ", "0", "0", "0", "0", "0", "0"},
    };
    
    int panjang = 850;
    int lebar = 650;
    int CTRGerak = 0;
    int CTRGerakL = 0;
    
    Timer timerGerakPlayerAtas = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() - 10);
            ImageIcon rawImg = new ImageIcon("Image/Mario/marioup"+CTRGerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerBawah = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() + 10);
            ImageIcon rawImg = new ImageIcon("Image/Mario/marioup"+CTRGerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerKiri = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX() - 10, LabelPlayer.getY());
            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokiri"+CTRGerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerKanan = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX() + 10, LabelPlayer.getY());
            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokanan"+CTRGerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerLoncatKiri = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokiri0.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            
            if (CTRGerakL <= 5) {
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() - 10);
                if (CTRGerakL == 5) {
                    MainFrame.player.y -= 1;
                }
            }
            else if (CTRGerakL <= 10) {
                LabelPlayer.setLocation(LabelPlayer.getX() - 10, LabelPlayer.getY());
                if (CTRGerakL == 10) {
                    MainFrame.player.x -= 1;
                }
            }
            else if (CTRGerakL <= 15) {
                LabelPlayer.setLocation(LabelPlayer.getX() - 10, LabelPlayer.getY());
                if (CTRGerakL == 15) {
                    MainFrame.player.x -= 1;
                }
            }
            else if (CTRGerakL <= 20) {
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() + 10);
                if (CTRGerakL == 20) {
                    MainFrame.player.y += 1;
                }
            }
            CTRGerakL++;
        }
    });
    
    Timer timerGerakPlayerLoncatKanan = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokanan0.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            
            if (CTRGerakL <= 5) {
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() - 10);
                if (CTRGerakL == 5) {
                    MainFrame.player.y -= 1;
                }
            }
            else if (CTRGerakL <= 10) {
                LabelPlayer.setLocation(LabelPlayer.getX() + 10, LabelPlayer.getY());
                if (CTRGerakL == 10) {
                    MainFrame.player.x += 1;
                }
            }
            else if (CTRGerakL <= 15) {
                LabelPlayer.setLocation(LabelPlayer.getX() + 10, LabelPlayer.getY());
                if (CTRGerakL == 15) {
                    MainFrame.player.x += 1;
                }
            }
            else if (CTRGerakL <= 20) {
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() + 10);
                if (CTRGerakL == 20) {
                    MainFrame.player.y += 1;
                }
            }
            CTRGerakL++;
        }
    });
    
    public FirePuzzle() {
        initComponents();
        this.setSize(panjang, lebar);
        
        //LabelBackground
        LabelBackground.setSize(panjang, lebar);
        ImageIcon rawImg = new ImageIcon("Image/Map/Fire Puzzle.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
        LabelBackground.setLocation(0,0);
        
        //LabelPlayer
        LabelPlayer.setSize(50, 50);
        rawImg = new ImageIcon("Image/Mario/marioup0.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelPlayer.setIcon(finalimg);
    }

    //Procedure
    public void setXYLabelPlayer(){
        LabelPlayer.setLocation(MainFrame.player.x * 50, MainFrame.player.y * 50);
    }
    
    public void Atas(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerAtas.start();
    }
    
    public void Bawah(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerBawah.start();
    }
    
    public void Kiri(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerKiri.start();
    }
    
    public void Kanan(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerKanan.start();
    }
    
    public void LoncatKiri(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerLoncatKiri.start();
    }
    
    public void LoncatKanan(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerLoncatKanan.start();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LabelPlayer = new javax.swing.JLabel();
        LabelBackground = new javax.swing.JLabel();

        setLayout(null);

        LabelPlayer.setText("LabelPlayer");
        add(LabelPlayer);
        LabelPlayer.setBounds(20, 50, 80, 20);

        LabelBackground.setText("LabelBackground");
        add(LabelBackground);
        LabelBackground.setBounds(15, 16, 120, 20);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelBackground;
    private javax.swing.JLabel LabelPlayer;
    // End of variables declaration//GEN-END:variables
}
