/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author AsusPC
 */
public class IcePuzzle extends javax.swing.JPanel {

    /**
     * Creates new form Puzzle_ice
     */
    String IcePuzzleTerrain[][] = {
        {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"},
        {"0", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "0", "0", "0", "0"},
        {"0", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "0", "E", "0", "0"},
        {"0", " ", "D", "D", "D", "D", "D", "D", " ", "D", "D", "D", " ", "D", "D", "D", "0"},
        {"0", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "0"},
        {"0", "D", "D", "D", " ", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", " ", "0"},
        {"0", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "0"},
        {"0", "D", "D", "D", "D", "D", "D", "D", " ", "D", "D", "D", "D", "D", "D", "D", "0"},
        {"0", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "0"},
        {"0", "D", "D", "D", " ", "D", "D", "D", "D", "D", "D", "D", "D", "D", " ", "D", "0"},
        {"0", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "0"},
        {"0", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", " ", "D", "D", " ", "0"},
        {"0", "0", "0", "0", "0", "0", "0", "0", " ", "0", "0", "0", "0", "0", "0", "0", "0"},
    };
    int panjang = 850;
    int lebar = 650;
    boolean CTRGerak = true;
    int CTR = 0;
    
    Timer timerGerakPlayerAtas = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() - 10);
            ImageIcon rawImg = new ImageIcon("Image/Mario/marioup0.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTR++;
            if(CTR == 5){
                cek_slide_atas();      
                CTR = 0;
            }
        }
    });

    Timer timerGerakPlayerBawah = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() + 10);
            ImageIcon rawImg = new ImageIcon("Image/Mario/marioup0.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTR++;
            if(CTR == 5){
                cek_slide_bawah();
                CTR = 0;
            }
        }
    });

    Timer timerGerakPlayerKiri = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX() - 10, LabelPlayer.getY());
            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokiri0.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTR++;
            if(CTR == 5){
                cek_slide_kiri();
                CTR = 0;
            }
        }
    });

    Timer timerGerakPlayerKanan = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX() + 10, LabelPlayer.getY());
            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokanan0.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTR++;
            if(CTR == 5){
                cek_slide_kanan();
                CTR = 0;
            }
        }
    });

    public IcePuzzle() {
        initComponents();
        this.setSize(panjang, lebar);

        //LabelBackground
        LabelBackground.setSize(panjang, lebar);
        LabelBackground.setLocation(0, 0);
        ImageIcon rawImg = new ImageIcon("Image/Map/Ice Puzzle.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);

        //LabelPlayer
        LabelPlayer.setSize(50, 50);
        rawImg = new ImageIcon("Image/Mario/marioup0.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelPlayer.setIcon(finalimg);

        //Dynamic Component
        LabelPlayer.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                System.out.println("click");
            }
        });
    }

    public void setXYLabelPlayer() {
        LabelPlayer.setLocation(MainFrame.player.x * 50, MainFrame.player.y * 50);
    }

    public void Atas() {
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = true;
        timerGerakPlayerAtas.start();
    }

    public void Bawah() {
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = true;
        timerGerakPlayerBawah.start();
    }

    public void Kiri() {
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = true;
        timerGerakPlayerKiri.start();
    }

    public void Kanan() {
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = true;
        timerGerakPlayerKanan.start();
    }

    public void cek_slide_atas() {
        if (IcePuzzleTerrain[MainFrame.player.y - 1][MainFrame.player.x] == "D" ) {
            CTRGerak = true;
            MainFrame.player.y -= 1;
        }
        if (IcePuzzleTerrain[MainFrame.player.y - 1][MainFrame.player.x] == " " ) {
            CTRGerak = false;
            MainFrame.player.y -= 1;
        }
        if (IcePuzzleTerrain[MainFrame.player.y - 1][MainFrame.player.x] == "0" || IcePuzzleTerrain[MainFrame.player.y - 1][MainFrame.player.x] == "E" ) {
            CTRGerak = false;
        }
    }
    
    public void cek_slide_bawah() {
        if (IcePuzzleTerrain[MainFrame.player.y + 1][MainFrame.player.x] == "D" ) {
            CTRGerak = true;
            MainFrame.player.y += 1;
        }
        if (IcePuzzleTerrain[MainFrame.player.y + 1][MainFrame.player.x] == " " ) {
            CTRGerak = false;
            MainFrame.player.y += 1;
        }
        if (IcePuzzleTerrain[MainFrame.player.y + 1][MainFrame.player.x] == "0" || IcePuzzleTerrain[MainFrame.player.y + 1][MainFrame.player.x] == "E"  ) {
            CTRGerak = false;
        }
    }
    
    public void cek_slide_kiri() {
        if (IcePuzzleTerrain[MainFrame.player.y][MainFrame.player.x-1] == "D" ) {
            CTRGerak = true;
            MainFrame.player.x -= 1;
        }
        if (IcePuzzleTerrain[MainFrame.player.y][MainFrame.player.x-1] == " " ) {
            CTRGerak = false;
            MainFrame.player.x -= 1;
        }
        if (IcePuzzleTerrain[MainFrame.player.y][MainFrame.player.x-1] == "0" || IcePuzzleTerrain[MainFrame.player.y][MainFrame.player.x-1] == "E") {
            CTRGerak = false;
        }
    }
    
    public void cek_slide_kanan() {
        if (IcePuzzleTerrain[MainFrame.player.y][MainFrame.player.x+1] == "D" ) {
            CTRGerak = true;
            MainFrame.player.x += 1;
        }
        if (IcePuzzleTerrain[MainFrame.player.y][MainFrame.player.x+1] == " " ) {
            CTRGerak = false;
            MainFrame.player.x += 1;
        }
        if (IcePuzzleTerrain[MainFrame.player.y][MainFrame.player.x+1] == "0" || IcePuzzleTerrain[MainFrame.player.y][MainFrame.player.x+1] == "E" ) {
            CTRGerak = false;
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LabelPlayer = new javax.swing.JLabel();
        LabelBackground = new javax.swing.JLabel();

        setLayout(null);

        LabelPlayer.setText("LabelPlayer");
        add(LabelPlayer);
        LabelPlayer.setBounds(20, 50, 80, 20);

        LabelBackground.setText("LabelBackground");
        add(LabelBackground);
        LabelBackground.setBounds(20, 20, 120, 20);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelBackground;
    private javax.swing.JLabel LabelPlayer;
    // End of variables declaration//GEN-END:variables

}
