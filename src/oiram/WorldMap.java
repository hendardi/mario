/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author Hendardi
 */
public class WorldMap extends javax.swing.JPanel {

    /**
     * Creates new form WorldMap
     */
    String WorldMapTerrain[][] = {
            {"0"," "," "," ","0","0"," "," "," "," "," "," ","0"," "," "," ","0"},
            {"0"," "," "," ","0","0"," "," "," "," "," ","0","0"," "," "," ","0"},
            {"0"," "," "," ","0"," "," "," "," "," "," "," ","0"," "," "," ","0"},
            {"0"," "," "," ","0"," "," "," "," "," "," "," ","0"," "," "," ","0"},
            {"0"," "," "," ","0","0"," "," "," "," "," ","0","0"," "," "," ","0"},
            {"0"," "," "," ","0","0"," "," "," "," "," ","0","0"," "," "," ","0"},
            {"0"," "," "," "," ","0"," "," "," "," "," ","0"," "," "," "," ","0"},
            {"0"," "," "," "," "," "," "," ","F"," "," "," "," "," "," "," ","0"},
            {"0"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","0"},
            {"0"," "," "," "," ","N"," "," "," "," "," ","B"," "," "," "," ","0"},
            {"0"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","0"},
            {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
            {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
    };
    int panjang = 850;
    int lebar = 650;
    int CTRGreeting = 0;
    int CTRGerak = 0;
    NPCThread NPCThread = new NPCThread();
    
    Timer timerGreeting = new Timer(5000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelGreeting.setText("");
            CTRGreeting = 1;
        }
    });
    
    Timer timerGerakPlayerAtas = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() - 10);
            ImageIcon rawImg = new ImageIcon("Image/Mario/marioup"+CTRGerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerBawah = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() + 10);
            ImageIcon rawImg = new ImageIcon("Image/Mario/marioup"+CTRGerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerKiri = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX() - 10, LabelPlayer.getY());
            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokiri"+CTRGerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerKanan = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX() + 10, LabelPlayer.getY());
            ImageIcon rawImg = new ImageIcon("Image/Mario/mariokanan"+CTRGerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelPlayer.setIcon(finalimg);
            CTRGerak++;
        }
    });
    
    public WorldMap() {
        initComponents();
        this.setSize(panjang, lebar);
        
        //LabelBackground
        LabelBackground.setSize(panjang,lebar);
        LabelBackground.setLocation(0,0);
        ImageIcon rawImg = new ImageIcon("Image/Map/World Map.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
        
        //LabelPlayer & Player
        LabelPlayer.setSize(50,50);
        LabelPlayer.setText("");
        rawImg = new ImageIcon("Image/Mario/marioup0.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelPlayer.setIcon(finalimg);
        MainFrame.player = new Player(8, 9);
        LabelPlayer.setLocation(MainFrame.player.x * 50, MainFrame.player.y * 50);
        
        //LabelNPC
        LabelNPC.setSize(50,50);
        LabelNPC.setText("");
        rawImg = new ImageIcon("Image/Unit/poring0.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelNPC.getWidth(), LabelNPC.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelNPC.setIcon(finalimg);
        MainFrame.NPC = new NPC(5, 9);
        LabelNPC.setLocation(MainFrame.NPC.x * 50, MainFrame.NPC.y * 50);
        
        //LabelGreeting
        LabelGreeting.setSize(500,100);
        LabelGreeting.setLocation(MainFrame.NPC.x * 22, MainFrame.NPC.y * 42);
        LabelGreeting.setText("Welcome 2 Oiram Re-Write World");
        LabelGreeting.setFont(new Font("Courier New", Font.BOLD, 20));
        LabelGreeting.setForeground(Color.white);
        
        //Dynamic Component
        LabelPlayer.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt){
                System.out.println("click");
           }
        });
    }
    
    //Threading
    class NPCThread extends Thread{
        public void run(){
            try {
                while(true){
                    for (int i = 0; i < 4; i++) {
                        ImageIcon rawImg = new ImageIcon("Image/Unit/poring"+i+".png");
                        Image img = rawImg.getImage();
                        Image newImg = img.getScaledInstance(LabelNPC.getWidth(), LabelNPC.getHeight(), Image.SCALE_SMOOTH);
                        ImageIcon finalimg = new ImageIcon(newImg);
                        LabelNPC.setIcon(finalimg);
                        Thread.sleep(150);
                    }
                }
            }
            catch (Exception e) {
                
            }
        }
    }
    
    //Procedure
    public void setXYLabelPlayer(){
        LabelPlayer.setLocation(MainFrame.player.x * 50, MainFrame.player.y * 50);
    }
    
    public void Atas(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerAtas.start();
    }
    
    public void Bawah(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerBawah.start();
    }
    
    public void Kiri(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerKiri.start();
    }
    
    public void Kanan(){
        timerGerakPlayerAtas.stop();
        timerGerakPlayerBawah.stop();
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        CTRGerak = 0;
        timerGerakPlayerKanan.start();
    }
    public void setHariSiang(){
        ImageIcon rawImg = new ImageIcon("Image/Map/World Map.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
    }
    
    public void setHariMalam(){
        ImageIcon rawImg = new ImageIcon("Image/Map/World Map Dark.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LabelGreeting = new javax.swing.JLabel();
        LabelNPC = new javax.swing.JLabel();
        LabelPlayer = new javax.swing.JLabel();
        LabelBackground = new javax.swing.JLabel();

        setLayout(null);

        LabelGreeting.setText("LabelGreeting");
        add(LabelGreeting);
        LabelGreeting.setBounds(20, 110, 360, 20);

        LabelNPC.setText("LabelNPC");
        add(LabelNPC);
        LabelNPC.setBounds(20, 80, 67, 20);

        LabelPlayer.setText("LabelPlayer");
        add(LabelPlayer);
        LabelPlayer.setBounds(20, 20, 80, 20);

        LabelBackground.setText("LabelBackground");
        add(LabelBackground);
        LabelBackground.setBounds(20, 50, 120, 20);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelBackground;
    private javax.swing.JLabel LabelGreeting;
    private javax.swing.JLabel LabelNPC;
    private javax.swing.JLabel LabelPlayer;
    // End of variables declaration//GEN-END:variables
}