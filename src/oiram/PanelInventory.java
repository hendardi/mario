/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author AsusPC
 */
public class PanelInventory extends javax.swing.JPanel {

    /**
     * Creates new form Inventory
     */
    int panjang = 850;
    int lebar = 650;
    int countdown = MainFrame.waktuReset;
    
    Timer timerCountdown = new Timer(100, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelCountdown.setText("Next Day : "+(countdown - MainFrame.player.waktu)+"s Left");
        }
    });
    
    public PanelInventory() {
        initComponents();
        this.setSize(panjang, lebar);
        timerCountdown.start();
        
        //LabelBackground
        LabelBackground.setSize(panjang, lebar);
        LabelBackground.setLocation(0, 0);
        ImageIcon rawImg = new ImageIcon("Image/Map/Inventory.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
        
        //LabelImageHari
        LabelImageHari.setSize(120, 100);
        LabelImageHari.setLocation(400, 420);
        rawImg = new ImageIcon("Image/Sun.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelImageHari.getWidth(), LabelImageHari.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelImageHari.setIcon(finalimg);
        
        //LabelCountdown
        LabelCountdown.setSize(500, 50);
        LabelCountdown.setLocation(50, 550);
        LabelCountdown.setText("Next Day : 0s Left");
        LabelCountdown.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelCountdown.setForeground(Color.black);
        
        //LabelInventory
        LabelInventory.setSize(300, 100);
        LabelInventory.setLocation(300, 0);
        LabelInventory.setText("INVENTORY");
        LabelInventory.setFont(new Font("Courier New", Font.BOLD, 40));
        LabelInventory.setForeground(Color.black);
        
        //LabelImagePotion
        LabelImagePotion.setSize(75, 75);
        LabelImagePotion.setLocation(50, 150);
        rawImg = new ImageIcon("Image/Potion.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelImagePotion.getWidth(), LabelImagePotion.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelImagePotion.setIcon(finalimg);
        
        //LabelImageColdDrink
        LabelImageColdDrink.setSize(75, 75);
        LabelImageColdDrink.setLocation(50, 250);
        rawImg = new ImageIcon("Image/Cold Drink.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelImageColdDrink.getWidth(), LabelImageColdDrink.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelImageColdDrink.setIcon(finalimg);
        
        //LabelImageEscapeRope
        LabelImageEscapeRope.setSize(75, 100);
        LabelImageEscapeRope.setLocation(50, 350);
        rawImg = new ImageIcon("Image/Escape Rope.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelImageEscapeRope.getWidth(), LabelImageEscapeRope.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelImageEscapeRope.setIcon(finalimg);
        
        //LabelPotion
        LabelPotion.setSize(400, 100);
        LabelPotion.setLocation(150, 150);
        LabelPotion.setText("Potion");
        LabelPotion.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelPotion.setForeground(Color.black);
        
        //LabelColdDrink
        LabelColdDrink.setSize(400, 100);
        LabelColdDrink.setLocation(150, 250);
        LabelColdDrink.setText("Cold Drink");
        LabelColdDrink.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelColdDrink.setForeground(Color.black);
        
        //LabelEscapeRope
        LabelEscapeRope.setSize(400, 100);
        LabelEscapeRope.setLocation(150, 350);
        LabelEscapeRope.setText("Escape Rope");
        LabelEscapeRope.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelEscapeRope.setForeground(Color.black);
        
        //LabelQTYPotion
        LabelQTYPotion.setSize(70, 50);
        LabelQTYPotion.setLocation(750, 170);
        LabelQTYPotion.setText(MainFrame.player.arrItem[0]+"x");
        LabelQTYPotion.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelQTYPotion.setForeground(Color.black);
        
        //LabelQTYColdDrink
        LabelQTYColdDrink.setSize(70, 50);
        LabelQTYColdDrink.setLocation(750, 270);
        LabelQTYColdDrink.setText(MainFrame.player.arrItem[1]+"x");
        LabelQTYColdDrink.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelQTYColdDrink.setForeground(Color.black);
        
        //LabelQTYEscapeRope
        LabelQTYEscapeRope.setSize(70, 50);
        LabelQTYEscapeRope.setLocation(750, 370);
        LabelQTYEscapeRope.setText(MainFrame.player.arrItem[2]+"x");
        LabelQTYEscapeRope.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelQTYEscapeRope.setForeground(Color.black);
        
        //LabelUsePotion
        LabelUsePotion.setSize(100, 50);
        LabelUsePotion.setLocation(600, 175);
        LabelUsePotion.setText("USE");
        LabelUsePotion.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelUsePotion.setForeground(Color.black);
        
        //LabelUseColdDrink
        LabelUseColdDrink.setSize(100, 50);
        LabelUseColdDrink.setLocation(600, 275);
        LabelUseColdDrink.setText("USE");
        LabelUseColdDrink.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelUseColdDrink.setForeground(Color.black);
        
        //LabelUseEscapeRope
        LabelUseEscapeRope.setSize(100, 50);
        LabelUseEscapeRope.setLocation(600, 375);
        LabelUseEscapeRope.setText("USE");
        LabelUseEscapeRope.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelUseEscapeRope.setForeground(Color.black);
        
        //LabelUang
        LabelUang.setSize(200, 50);
        LabelUang.setLocation(50, 100);
        LabelUang.setText("Gold : "+MainFrame.player.uang);
        LabelUang.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelUang.setForeground(Color.black);
        
        //LabelQuest
        LabelQuest.setSize(200, 50);
        LabelQuest.setLocation(50, 470);
        LabelQuest.setText("Check Quest");
        LabelQuest.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelQuest.setForeground(Color.black);
        
        //LabelExit
        LabelExit.setSize(100, 50);
        LabelExit.setLocation(670, 550);
        LabelExit.setText("EXIT");
        LabelExit.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelExit.setForeground(Color.black);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    public void setInventory(){
        LabelQTYPotion.setText(MainFrame.player.arrItem[0]+"x");
        LabelQTYPotion.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelQTYPotion.setForeground(Color.black);
        
        LabelQTYColdDrink.setText(MainFrame.player.arrItem[1]+"x");
        LabelQTYColdDrink.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelQTYColdDrink.setForeground(Color.black);
        
        LabelQTYEscapeRope.setText(MainFrame.player.arrItem[2]+"x");
        LabelQTYEscapeRope.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelQTYEscapeRope.setForeground(Color.black);
        
        LabelUang.setText("Gold : "+MainFrame.player.uang);
        LabelUang.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelUang.setForeground(Color.black);
    }
    
    public void setHariSiang(){
        ImageIcon rawImg = new ImageIcon("Image/Sun.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelImageHari.getWidth(), LabelImageHari.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelImageHari.setIcon(finalimg);
    }
    
    public void setHariMalam(){
        ImageIcon rawImg = new ImageIcon("Image/Moon.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelImageHari.getWidth(), LabelImageHari.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelImageHari.setIcon(finalimg);
    }
    
    public void setLabelColdDrink(){
        LabelColdDrink.setForeground(Color.black);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LabelPotion = new javax.swing.JLabel();
        LabelColdDrink = new javax.swing.JLabel();
        LabelEscapeRope = new javax.swing.JLabel();
        LabelQTYPotion = new javax.swing.JLabel();
        LabelQTYColdDrink = new javax.swing.JLabel();
        LabelQTYEscapeRope = new javax.swing.JLabel();
        LabelUsePotion = new javax.swing.JLabel();
        LabelUseColdDrink = new javax.swing.JLabel();
        LabelUseEscapeRope = new javax.swing.JLabel();
        LabelExit = new javax.swing.JLabel();
        LabelUang = new javax.swing.JLabel();
        LabelImagePotion = new javax.swing.JLabel();
        LabelImageColdDrink = new javax.swing.JLabel();
        LabelImageEscapeRope = new javax.swing.JLabel();
        LabelInventory = new javax.swing.JLabel();
        LabelImageHari = new javax.swing.JLabel();
        LabelCountdown = new javax.swing.JLabel();
        LabelQuest = new javax.swing.JLabel();
        LabelBackground = new javax.swing.JLabel();

        setLayout(null);

        LabelPotion.setText("LabelPotion");
        add(LabelPotion);
        LabelPotion.setBounds(20, 70, 90, 20);

        LabelColdDrink.setText("LabelColdDrink");
        add(LabelColdDrink);
        LabelColdDrink.setBounds(20, 90, 110, 20);

        LabelEscapeRope.setText("LabelEscapeRope");
        add(LabelEscapeRope);
        LabelEscapeRope.setBounds(20, 120, 130, 20);

        LabelQTYPotion.setText("LabelQTYPotion");
        add(LabelQTYPotion);
        LabelQTYPotion.setBounds(310, 60, 120, 20);

        LabelQTYColdDrink.setText("LabelQTYColdDrink");
        add(LabelQTYColdDrink);
        LabelQTYColdDrink.setBounds(310, 90, 140, 20);

        LabelQTYEscapeRope.setText("LabelQTYEscapeRope");
        add(LabelQTYEscapeRope);
        LabelQTYEscapeRope.setBounds(310, 120, 160, 20);

        LabelUsePotion.setText("LabelUsePotion");
        LabelUsePotion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelUsePotionMouseClicked(evt);
            }
        });
        add(LabelUsePotion);
        LabelUsePotion.setBounds(150, 60, 110, 20);

        LabelUseColdDrink.setText("LabelUseColdDrink");
        LabelUseColdDrink.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelUseColdDrinkMouseClicked(evt);
            }
        });
        add(LabelUseColdDrink);
        LabelUseColdDrink.setBounds(150, 90, 133, 20);

        LabelUseEscapeRope.setText("LabelUseEscapeRope");
        LabelUseEscapeRope.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelUseEscapeRopeMouseClicked(evt);
            }
        });
        add(LabelUseEscapeRope);
        LabelUseEscapeRope.setBounds(150, 120, 150, 20);

        LabelExit.setText("LabelExit");
        LabelExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelExitMouseClicked(evt);
            }
        });
        add(LabelExit);
        LabelExit.setBounds(310, 260, 70, 20);

        LabelUang.setText("LabelUang");
        add(LabelUang);
        LabelUang.setBounds(20, 40, 80, 20);

        LabelImagePotion.setText("LabelImagePotion");
        add(LabelImagePotion);
        LabelImagePotion.setBounds(20, 150, 130, 20);

        LabelImageColdDrink.setText("LabelImageColdDrink");
        add(LabelImageColdDrink);
        LabelImageColdDrink.setBounds(20, 180, 152, 20);

        LabelImageEscapeRope.setText("LabelImageEscapeRope");
        add(LabelImageEscapeRope);
        LabelImageEscapeRope.setBounds(20, 210, 170, 20);

        LabelInventory.setText("LabelInventory");
        add(LabelInventory);
        LabelInventory.setBounds(170, 10, 110, 20);

        LabelImageHari.setText("LabelImageHari");
        add(LabelImageHari);
        LabelImageHari.setBounds(190, 150, 120, 20);

        LabelCountdown.setText("LabelCountdown");
        add(LabelCountdown);
        LabelCountdown.setBounds(190, 180, 120, 20);

        LabelQuest.setText("LabelQuest");
        LabelQuest.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelQuestMouseClicked(evt);
            }
        });
        add(LabelQuest);
        LabelQuest.setBounds(300, 220, 80, 20);

        LabelBackground.setText("LabelBackground");
        add(LabelBackground);
        LabelBackground.setBounds(20, 10, 120, 20);
    }// </editor-fold>//GEN-END:initComponents

    private void LabelExitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelExitMouseClicked
        MainFrame MainFrame = (MainFrame)this.getParent().getParent().getParent().getParent();
        MainFrame.ExitInventory();
    }//GEN-LAST:event_LabelExitMouseClicked

    private void LabelQuestMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelQuestMouseClicked
        MainFrame MainFrame = (MainFrame)this.getParent().getParent().getParent().getParent();
        MainFrame.goToQuestInventory();
    }//GEN-LAST:event_LabelQuestMouseClicked

    private void LabelUsePotionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelUsePotionMouseClicked
        MainFrame MainFrame = (MainFrame)this.getParent().getParent().getParent().getParent();
        if(MainFrame.player.arrItem[0] > 0 && MainFrame.player.hp < MainFrame.player.hpMax){
            MainFrame.player.arrItem[0] -= 1;
            MainFrame.player.hp += 50;
            if (MainFrame.player.hp > MainFrame.player.hpMax) {
                MainFrame.player.hp = MainFrame.player.hpMax;
            }
            LabelQTYPotion.setText(MainFrame.player.arrItem[0]+"x");
            MainFrame.PanelBattle.setHPmario();
        }
    }//GEN-LAST:event_LabelUsePotionMouseClicked

    private void LabelUseColdDrinkMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelUseColdDrinkMouseClicked
        MainFrame MainFrame = (MainFrame)this.getParent().getParent().getParent().getParent();
        if(MainFrame.visiblePanelBattle == false && MainFrame.player.arrItem[1] > 0){
            MainFrame.player.arrItem[1] -= 1;
            MainFrame.player.coldDrink = true;
            MainFrame.timerColdDrink.stop();
            MainFrame.timerColdDrink.start();
            LabelColdDrink.setForeground(Color.white);
            LabelQTYColdDrink.setText(MainFrame.player.arrItem[1]+"x");
        }
    }//GEN-LAST:event_LabelUseColdDrinkMouseClicked

    private void LabelUseEscapeRopeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelUseEscapeRopeMouseClicked
        MainFrame MainFrame = (MainFrame)this.getParent().getParent().getParent().getParent();
        if(MainFrame.visiblePanelBattle == false && MainFrame.player.arrItem[2] > 0){
            MainFrame.player.arrItem[2] -= 1;
            MainFrame.EscapeRope();
            LabelQTYEscapeRope.setText(MainFrame.player.arrItem[2]+"x");
        }
    }//GEN-LAST:event_LabelUseEscapeRopeMouseClicked
        

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelBackground;
    private javax.swing.JLabel LabelColdDrink;
    private javax.swing.JLabel LabelCountdown;
    private javax.swing.JLabel LabelEscapeRope;
    private javax.swing.JLabel LabelExit;
    private javax.swing.JLabel LabelImageColdDrink;
    private javax.swing.JLabel LabelImageEscapeRope;
    private javax.swing.JLabel LabelImageHari;
    private javax.swing.JLabel LabelImagePotion;
    private javax.swing.JLabel LabelInventory;
    private javax.swing.JLabel LabelPotion;
    private javax.swing.JLabel LabelQTYColdDrink;
    private javax.swing.JLabel LabelQTYEscapeRope;
    private javax.swing.JLabel LabelQTYPotion;
    private javax.swing.JLabel LabelQuest;
    private javax.swing.JLabel LabelUang;
    private javax.swing.JLabel LabelUseColdDrink;
    private javax.swing.JLabel LabelUseEscapeRope;
    private javax.swing.JLabel LabelUsePotion;
    // End of variables declaration//GEN-END:variables
}
