/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.ImageIcon;

/**
 *
 * @author Hendardi
 */
public class PanelNPC extends javax.swing.JPanel {

    /**
     * Creates new form PanelNPC
     */
    int panjang = 850;
    int lebar = 650;
    
    public PanelNPC() {
        initComponents();
        this.setSize(panjang, lebar);
        
        //LabelBackground
        LabelBackground.setSize(panjang, lebar);
        ImageIcon rawImg = new ImageIcon("Image/Map/Shop.jpg");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
        LabelBackground.setLocation(0,0);
        
        //LabelWelcome
        LabelWelcome.setSize(300, 100);
        LabelWelcome.setLocation(325, 0);
        LabelWelcome.setText("WELCOME");
        LabelWelcome.setFont(new Font("Courier New", Font.BOLD, 50));
        LabelWelcome.setForeground(Color.white);
        
        //LabelUang
        LabelUang.setSize(300, 100);
        LabelUang.setLocation(50, 70);
        LabelUang.setText("Gold : "+MainFrame.player.uang);
        LabelUang.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelUang.setForeground(Color.white);
        
        //LabelQTY
        LabelQTY.setSize(100, 100);
        LabelQTY.setLocation(750, 70);
        LabelQTY.setText("QTY");
        LabelQTY.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelQTY.setForeground(Color.white);
        
        //LabelQTYPotion
        LabelQTYPotion.setSize(50, 50);
        LabelQTYPotion.setLocation(750, 170);
        LabelQTYPotion.setText(MainFrame.player.arrItem[0]+"x");
        LabelQTYPotion.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelQTYPotion.setForeground(Color.white);
        
        //LabelQTYColdDrink
        LabelQTYColdDrink.setSize(50, 50);
        LabelQTYColdDrink.setLocation(750, 270);
        LabelQTYColdDrink.setText(MainFrame.player.arrItem[1]+"x");
        LabelQTYColdDrink.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelQTYColdDrink.setForeground(Color.white);
        
        //LabelQTYEscapeRope
        LabelQTYEscapeRope.setSize(50, 50);
        LabelQTYEscapeRope.setLocation(750, 370);
        LabelQTYEscapeRope.setText(MainFrame.player.arrItem[2]+"x");
        LabelQTYEscapeRope.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelQTYEscapeRope.setForeground(Color.white);
        
        //LabelImagePotion
        LabelImagePotion.setSize(75, 75);
        LabelImagePotion.setLocation(50, 150);
        rawImg = new ImageIcon("Image/Potion.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelImagePotion.getWidth(), LabelImagePotion.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelImagePotion.setIcon(finalimg);
        
        //LabelImageColdDrink
        LabelImageColdDrink.setSize(75, 75);
        LabelImageColdDrink.setLocation(50, 250);
        rawImg = new ImageIcon("Image/Cold Drink.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelImageColdDrink.getWidth(), LabelImageColdDrink.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelImageColdDrink.setIcon(finalimg);
        
        //LabelImageEscapeRope
        LabelImageEscapeRope.setSize(75, 100);
        LabelImageEscapeRope.setLocation(50, 350);
        rawImg = new ImageIcon("Image/Escape Rope.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelImageEscapeRope.getWidth(), LabelImageEscapeRope.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelImageEscapeRope.setIcon(finalimg);
        
        //LabelPotion
        LabelPotion.setSize(400, 100);
        LabelPotion.setLocation(150, 150);
        LabelPotion.setText("Potion - 100");
        LabelPotion.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelPotion.setForeground(Color.white);
        
        //LabelColdDrink
        LabelColdDrink.setSize(400, 100);
        LabelColdDrink.setLocation(150, 250);
        LabelColdDrink.setText("Cold Drink - 150");
        LabelColdDrink.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelColdDrink.setForeground(Color.white);
        
        //LabelEscapeRope
        LabelEscapeRope.setSize(400, 100);
        LabelEscapeRope.setLocation(150, 350);
        LabelEscapeRope.setText("Escape Rope - 200");
        LabelEscapeRope.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelEscapeRope.setForeground(Color.white);
        
        //LabelBuyPotion
        LabelBuyPotion.setSize(70, 50);
        LabelBuyPotion.setLocation(600, 175);
        LabelBuyPotion.setText("BUY");
        LabelBuyPotion.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelBuyPotion.setForeground(Color.white);
        
        //LabelBuyColdDrink
        LabelBuyColdDrink.setSize(70, 50);
        LabelBuyColdDrink.setLocation(600, 275);
        LabelBuyColdDrink.setText("BUY");
        LabelBuyColdDrink.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelBuyColdDrink.setForeground(Color.white);
        
        //LabelBuyEscapeRope
        LabelBuyEscapeRope.setSize(70, 50);
        LabelBuyEscapeRope.setLocation(600, 375);
        LabelBuyEscapeRope.setText("BUY");
        LabelBuyEscapeRope.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelBuyEscapeRope.setForeground(Color.white);
        
        //LabelSave
        LabelSave.setSize(100, 50);
        LabelSave.setLocation(250, 450);
        LabelSave.setText("SAVE");
        LabelSave.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelSave.setForeground(Color.white);
        
        //LabelLoad
        LabelLoad.setSize(100, 50);
        LabelLoad.setLocation(500, 450);
        LabelLoad.setText("LOAD");
        LabelLoad.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelLoad.setForeground(Color.white);
        
        //LabelPoring
        LabelPoring.setSize(70, 70);
        LabelPoring.setLocation(50, 550);
        rawImg = new ImageIcon("Image/Unit/NPC.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelPoring.getWidth(), LabelPoring.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelPoring.setIcon(finalimg);
        
        //FieldDialog
        FieldDialog.setSize(550, 50);
        FieldDialog.setLocation(150, 570);
        FieldDialog.setText("Hello, What Do U Wanna Do ?");
        FieldDialog.setFont(new Font("Courier New", Font.BOLD, 20));
        FieldDialog.setForeground(Color.black);
        
        //LabelExit
        LabelExit.setSize(100, 50);
        LabelExit.setLocation(750, 570);
        LabelExit.setText("EXIT");
        LabelExit.setFont(new Font("Courier New", Font.BOLD, 30));
        LabelExit.setForeground(Color.white);
    }
    
    public void setData(){
        LabelUang.setText("Gold : "+MainFrame.player.uang);
        LabelQTYPotion.setText(MainFrame.player.arrItem[0]+"x");
        LabelQTYColdDrink.setText(MainFrame.player.arrItem[1]+"x");
        LabelQTYEscapeRope.setText(MainFrame.player.arrItem[2]+"x");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LabelPoring = new javax.swing.JLabel();
        LabelPotion = new javax.swing.JLabel();
        LabelColdDrink = new javax.swing.JLabel();
        LabelEscapeRope = new javax.swing.JLabel();
        LabelWelcome = new javax.swing.JLabel();
        FieldDialog = new javax.swing.JTextField();
        LabelUang = new javax.swing.JLabel();
        LabelQTY = new javax.swing.JLabel();
        LabelQTYPotion = new javax.swing.JLabel();
        LabelQTYColdDrink = new javax.swing.JLabel();
        LabelQTYEscapeRope = new javax.swing.JLabel();
        LabelBuyPotion = new javax.swing.JLabel();
        LabelBuyColdDrink = new javax.swing.JLabel();
        LabelBuyEscapeRope = new javax.swing.JLabel();
        LabelSave = new javax.swing.JLabel();
        LabelExit = new javax.swing.JLabel();
        LabelLoad = new javax.swing.JLabel();
        LabelImagePotion = new javax.swing.JLabel();
        LabelImageColdDrink = new javax.swing.JLabel();
        LabelImageEscapeRope = new javax.swing.JLabel();
        LabelBackground = new javax.swing.JLabel();

        setLayout(null);

        LabelPoring.setText("LabelPoring");
        add(LabelPoring);
        LabelPoring.setBounds(20, 260, 90, 20);

        LabelPotion.setText("LabelPotion");
        LabelPotion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelPotionMouseClicked(evt);
            }
        });
        add(LabelPotion);
        LabelPotion.setBounds(20, 70, 90, 20);

        LabelColdDrink.setText("LabelColdDrink");
        LabelColdDrink.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelColdDrinkMouseClicked(evt);
            }
        });
        add(LabelColdDrink);
        LabelColdDrink.setBounds(20, 110, 107, 20);

        LabelEscapeRope.setText("LabelEscapeRope");
        LabelEscapeRope.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelEscapeRopeMouseClicked(evt);
            }
        });
        add(LabelEscapeRope);
        LabelEscapeRope.setBounds(20, 150, 121, 20);

        LabelWelcome.setText("LabelWelcome");
        add(LabelWelcome);
        LabelWelcome.setBounds(140, 10, 110, 20);

        FieldDialog.setEditable(false);
        FieldDialog.setText("FieldDialog");
        add(FieldDialog);
        FieldDialog.setBounds(20, 220, 190, 26);

        LabelUang.setText("LabelUang");
        add(LabelUang);
        LabelUang.setBounds(20, 20, 80, 20);

        LabelQTY.setText("LabelQTY");
        add(LabelQTY);
        LabelQTY.setBounds(300, 30, 69, 20);

        LabelQTYPotion.setText("QTYPotion");
        add(LabelQTYPotion);
        LabelQTYPotion.setBounds(300, 60, 80, 20);

        LabelQTYColdDrink.setText("QTYColdDrink");
        add(LabelQTYColdDrink);
        LabelQTYColdDrink.setBounds(320, 100, 110, 20);

        LabelQTYEscapeRope.setText("QTYEscapeRope");
        add(LabelQTYEscapeRope);
        LabelQTYEscapeRope.setBounds(340, 140, 120, 20);

        LabelBuyPotion.setText("LabelBuyPotion");
        LabelBuyPotion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelBuyPotionMouseClicked(evt);
            }
        });
        add(LabelBuyPotion);
        LabelBuyPotion.setBounds(160, 70, 108, 20);

        LabelBuyColdDrink.setText("LabelBuyColdDrink");
        LabelBuyColdDrink.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelBuyColdDrinkMouseClicked(evt);
            }
        });
        add(LabelBuyColdDrink);
        LabelBuyColdDrink.setBounds(160, 110, 133, 20);

        LabelBuyEscapeRope.setText("LabelBuyEscapeRope");
        LabelBuyEscapeRope.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelBuyEscapeRopeMouseClicked(evt);
            }
        });
        add(LabelBuyEscapeRope);
        LabelBuyEscapeRope.setBounds(160, 150, 150, 20);

        LabelSave.setText("LabelSave");
        LabelSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelSaveMouseClicked(evt);
            }
        });
        add(LabelSave);
        LabelSave.setBounds(70, 190, 70, 20);

        LabelExit.setText("LabelExit");
        LabelExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelExitMouseClicked(evt);
            }
        });
        add(LabelExit);
        LabelExit.setBounds(300, 260, 70, 20);

        LabelLoad.setText("LabelLoad");
        LabelLoad.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelLoadMouseClicked(evt);
            }
        });
        add(LabelLoad);
        LabelLoad.setBounds(240, 190, 80, 20);

        LabelImagePotion.setText("LabelImagePotion");
        add(LabelImagePotion);
        LabelImagePotion.setBounds(20, 50, 130, 20);

        LabelImageColdDrink.setText("LabelImageColdDrink");
        add(LabelImageColdDrink);
        LabelImageColdDrink.setBounds(20, 90, 160, 20);

        LabelImageEscapeRope.setText("LabelImageEscapeRope");
        add(LabelImageEscapeRope);
        LabelImageEscapeRope.setBounds(20, 130, 170, 20);

        LabelBackground.setText("LabelBackground");
        add(LabelBackground);
        LabelBackground.setBounds(140, 260, 120, 20);
    }// </editor-fold>//GEN-END:initComponents

    private void LabelBuyPotionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelBuyPotionMouseClicked
        if (MainFrame.player.uang >= 100 && MainFrame.player.arrItem[0] < 9) {
            MainFrame.player.uang -= 100;
            MainFrame.player.arrItem[0] += 1;
            LabelUang.setText("Gold : "+MainFrame.player.uang);
            LabelQTYPotion.setText(MainFrame.player.arrItem[0]+"x");
            FieldDialog.setText("Anda telah Membeli Potion");
        }
        else if (MainFrame.player.arrItem[0] >= 9) {
            FieldDialog.setText("Melebihi Batas Inventory");
        }
        else if (MainFrame.player.uang < 100) {
            FieldDialog.setText("Uang Tidak Cukup");
        }
    }//GEN-LAST:event_LabelBuyPotionMouseClicked

    private void LabelBuyColdDrinkMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelBuyColdDrinkMouseClicked
        if (MainFrame.player.uang >= 150 && MainFrame.player.arrItem[1] < 9) {
            MainFrame.player.uang -= 150;
            MainFrame.player.arrItem[1] += 1;
            LabelUang.setText("Gold : "+MainFrame.player.uang);
            LabelQTYColdDrink.setText(MainFrame.player.arrItem[1]+"x");
            FieldDialog.setText("Anda telah Membeli Cold Drink");
        }
        else if (MainFrame.player.arrItem[1] >= 9) {
            FieldDialog.setText("Melebihi Batas Inventory");
        }
        else if (MainFrame.player.uang < 150) {
            FieldDialog.setText("Uang Tidak Cukup");
        }
    }//GEN-LAST:event_LabelBuyColdDrinkMouseClicked

    private void LabelBuyEscapeRopeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelBuyEscapeRopeMouseClicked
        if (MainFrame.player.uang >= 200 && MainFrame.player.arrItem[2] < 9) {
            MainFrame.player.uang -= 200;
            MainFrame.player.arrItem[2] += 1;
            LabelUang.setText("Gold : "+MainFrame.player.uang);
            LabelQTYEscapeRope.setText(MainFrame.player.arrItem[2]+"x");
            FieldDialog.setText("Anda telah Membeli Escape Rope");
        }
        else if (MainFrame.player.arrItem[2] >= 9) {
            FieldDialog.setText("Melebihi Batas Inventory");
        }
        else if (MainFrame.player.uang < 200) {
            FieldDialog.setText("Uang Tidak Cukup");
        }
    }//GEN-LAST:event_LabelBuyEscapeRopeMouseClicked

    private void LabelExitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelExitMouseClicked
        MainFrame MainFrame = (MainFrame)this.getParent().getParent().getParent().getParent();
        MainFrame.ExitInventory();
        FieldDialog.setText("Hello, What Do U Wanna Do ?");
    }//GEN-LAST:event_LabelExitMouseClicked

    private void LabelPotionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelPotionMouseClicked
        FieldDialog.setText("Effect : Memulihkan HP Sebanyak 50");
    }//GEN-LAST:event_LabelPotionMouseClicked

    private void LabelColdDrinkMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelColdDrinkMouseClicked
        FieldDialog.setText("Effect : Menghentikan DPS Pada Fire Dungeon");
    }//GEN-LAST:event_LabelColdDrinkMouseClicked

    private void LabelEscapeRopeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelEscapeRopeMouseClicked
        FieldDialog.setText("Effect : Keluar Dari Dungeon");
    }//GEN-LAST:event_LabelEscapeRopeMouseClicked

    private void LabelSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelSaveMouseClicked
        try {
            FileOutputStream out = new FileOutputStream("Save.txt");
            ObjectOutputStream tulis = new ObjectOutputStream(out);
            tulis.writeObject(MainFrame.player);
            tulis.close(); out.close();
            FieldDialog.setText("Save Sukses");
        } catch (Exception e) {
            e.printStackTrace();
            FieldDialog.setText("Save gagal");
        }
    }//GEN-LAST:event_LabelSaveMouseClicked

    private void LabelLoadMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelLoadMouseClicked
        MainFrame MainFrame = (MainFrame)this.getParent().getParent().getParent().getParent();
        try {
            FileInputStream out = new FileInputStream("Save.txt");
            ObjectInputStream tulis = new ObjectInputStream(out);
            MainFrame.player = (Player)tulis.readObject();
            tulis.close(); out.close();
            setData();
            MainFrame.WorldMap.setXYLabelPlayer();
            if (MainFrame.player.hari == 0) {
                MainFrame.WorldMap.setHariSiang();
                MainFrame.PanelInventory.setHariSiang();
                MainFrame.FireMap.setHariSiang();
                MainFrame.IceMap.setHariSiang();
                MainFrame.BossMap.setHariSiang();
                MainFrame.FireDungeon.setHariSiang();
                MainFrame.IceDungeon.setHariSiang();
            }
            else if (MainFrame.player.hari == 1) {
                MainFrame.WorldMap.setHariMalam();
                MainFrame.PanelInventory.setHariMalam();
                MainFrame.FireMap.setHariMalam();
                MainFrame.IceMap.setHariMalam();
                MainFrame.BossMap.setHariMalam();
                MainFrame.FireDungeon.setHariMalam();
                MainFrame.IceDungeon.setHariMalam();
            }
            FieldDialog.setText("Load Sukses");
        } catch (Exception e) {
            e.printStackTrace();
            FieldDialog.setText("Load Gagal");
        }
    }//GEN-LAST:event_LabelLoadMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField FieldDialog;
    private javax.swing.JLabel LabelBackground;
    private javax.swing.JLabel LabelBuyColdDrink;
    private javax.swing.JLabel LabelBuyEscapeRope;
    private javax.swing.JLabel LabelBuyPotion;
    private javax.swing.JLabel LabelColdDrink;
    private javax.swing.JLabel LabelEscapeRope;
    private javax.swing.JLabel LabelExit;
    private javax.swing.JLabel LabelImageColdDrink;
    private javax.swing.JLabel LabelImageEscapeRope;
    private javax.swing.JLabel LabelImagePotion;
    private javax.swing.JLabel LabelLoad;
    private javax.swing.JLabel LabelPoring;
    private javax.swing.JLabel LabelPotion;
    private javax.swing.JLabel LabelQTY;
    private javax.swing.JLabel LabelQTYColdDrink;
    private javax.swing.JLabel LabelQTYEscapeRope;
    private javax.swing.JLabel LabelQTYPotion;
    private javax.swing.JLabel LabelSave;
    private javax.swing.JLabel LabelUang;
    private javax.swing.JLabel LabelWelcome;
    // End of variables declaration//GEN-END:variables
}
