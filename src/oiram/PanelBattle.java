/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 *
 * @author Hendardi
 */
public class PanelBattle extends javax.swing.JPanel {

    /**
     * Creates new form PanelBattle
     */
    int panjang = 850;
    int lebar = 650;
    int HPMusuh = 0;
    int attackMusuh = 0;
    int CTR = 0;
    int CTRMusuh = 0;
    int CTRMaju = 0;
    int musuh;
    
    JFXPanel j8 = new JFXPanel();
    String uri8 = new File("Sound/Punch.mp3").toURI().toString();
    MediaPlayer Punch = new MediaPlayer(new Media(uri8));
    
    JFXPanel j9 = new JFXPanel();
    String uri9 = new File("Sound/Voice Fight.mp3").toURI().toString();
    MediaPlayer VoiceFight = new MediaPlayer(new Media(uri9));
    Timer timerJalanMasuk = new Timer(100,new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            LabelMario.setLocation(LabelMario.getX()+10, LabelMario.getY());
            if(LabelMario.getX() == 150)
            {
                ((Timer)e.getSource()).stop();
                Timer timerMusuhMasuk = new Timer(100,new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        LabelMusuh.setLocation(LabelMusuh.getX()-10, LabelMusuh.getY());
                        if(LabelMusuh.getX() == 650)
                        {
                            ((Timer)e.getSource()).stop();
                            VoiceFight.play();
                        }
                    }
                });
                timerMusuhMasuk.start();
            }
        }
    });
    
    public PanelBattle() {
        initComponents();
        this.setSize(panjang, lebar);
        
        ProgressHPMario.repaint();
        ProgressHPMusuh.repaint();
        LabelHPMario.repaint();
        LabelHPMusuh.repaint();
        
        //LabelBackground
        LabelBackground.setSize(panjang, lebar);
        LabelBackground.setLocation(0, 0);
        ImageIcon rawImg = new ImageIcon("Image/Map/Battle Map.jpg");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
        
        LabelMario.setSize(100,100);
        LabelMario.setLocation(-100, 450);
        rawImg = new ImageIcon("Image/Mario/mariokanan0.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelMario.setIcon(finalimg);
        
        LabelMusuh.setSize(100, 100);
        LabelMusuh.setLocation(900,290);
        
        LabelHPMario.setText(MainFrame.player.hp + "");
        LabelHPMario.setSize(400, 400);
        LabelHPMario.setLocation(260, 260);
        LabelHPMario.setBackground(Color.red);
        LabelHPMario.setFont(new Font("Courier New", Font.BOLD, 50));
        
        ProgressHPMario.setMinimum(0);
        ProgressHPMario.setMaximum(100);
        ProgressHPMario.setBackground(Color.red);
        ProgressHPMario.setValue(100);
        ProgressHPMario.setLocation(80, 400);
        ProgressHPMario.setSize(200 ,30);
        
        LabelHPMusuh.setSize(400, 400);        
        LabelHPMusuh.setBackground(Color.red); 
        LabelHPMusuh.setText(HPMusuh + "");
        LabelHPMusuh.setFont(new Font("Courier New", Font.BOLD, 50));
        LabelHPMusuh.setLocation(570, 100);
        
        ProgressHPMusuh.setMinimum(0);
        ProgressHPMusuh.setMaximum(HPMusuh);
        ProgressHPMusuh.setBackground(Color.blue);
        ProgressHPMusuh.setValue(100);
        ProgressHPMusuh.setLocation(560, 240);
        ProgressHPMusuh.setSize(200, 30);

        LabelPilih.setSize(400, 150);
        rawImg = new ImageIcon("Image/LabelPilih.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelPilih.getWidth(), LabelPilih.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelPilih.setIcon(finalimg);
        LabelPilih.setLocation(500, 450);
        
        LabelAttack.setSize(400, 50);
        LabelAttack.setLocation(550, 470);
        LabelAttack.setText("Attack");
        LabelAttack.setFont(new Font("Courier New",Font.BOLD,50));
        LabelAttack.setForeground(Color.black);
        
        LabelInventory.setSize(400, 50);
        LabelInventory.setLocation(550, 520);
        LabelInventory.setText("Inventory");
        LabelInventory.setFont(new Font("Courier New",Font.BOLD,50));
        LabelInventory.setForeground(Color.black);
    }
    
    public void setHPmario(){
        LabelHPMario.setText(MainFrame.player.hp + "");
        ProgressHPMario.setValue(MainFrame.player.hp);
    }
    
    public void setBattle(){
        LabelMusuh.setLocation(900 ,290);
        ProgressHPMusuh.setMinimum(0);
        
        if (musuh == 1) {
            HPMusuh = MainFrame.m1.hp;
            attackMusuh = MainFrame.m1.attack;
            LabelHPMusuh.setText(MainFrame.m1.hp + "");
            ProgressHPMusuh.setMaximum(MainFrame.m1.hpMax);
            ProgressHPMusuh.setValue(MainFrame.m1.hp);
            
            if (MainFrame.m1.nama.equals("Gumba")) {
                ImageIcon rawImg = new ImageIcon("Image/Unit/gumba0.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelMusuh.getWidth(), LabelMusuh.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelMusuh.setIcon(finalimg);
            }
            else if (MainFrame.m1.nama.equals("Kuppa")) {
                ImageIcon rawImg = new ImageIcon("Image/Unit/kuppakiri0.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelMusuh.getWidth(), LabelMusuh.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelMusuh.setIcon(finalimg);
            }
        }
        if (musuh == 2) {
            HPMusuh = MainFrame.m2.hp;
            attackMusuh = MainFrame.m2.attack;
            LabelHPMusuh.setText(MainFrame.m2.hp + "");
            ProgressHPMusuh.setMaximum(MainFrame.m2.hpMax);
            ProgressHPMusuh.setValue(MainFrame.m2.hp);
            
            if (MainFrame.m2.nama.equals("Gumba")) {
                ImageIcon rawImg = new ImageIcon("Image/Unit/gumba0.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelMusuh.getWidth(), LabelMusuh.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelMusuh.setIcon(finalimg);
            }
            else if (MainFrame.m2.nama.equals("Kuppa")) {
                ImageIcon rawImg = new ImageIcon("Image/Unit/kuppakiri0.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelMusuh.getWidth(), LabelMusuh.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelMusuh.setIcon(finalimg);
            }
        }
        if (musuh == 3) {
            HPMusuh = MainFrame.m3.hp;
            attackMusuh = MainFrame.m3.attack;
            LabelHPMusuh.setText(MainFrame.m3.hp + "");
            ProgressHPMusuh.setMaximum(MainFrame.m3.hpMax);
            ProgressHPMusuh.setValue(MainFrame.m3.hp);
            
            if (MainFrame.m3.nama.equals("Gumba")) {
                ImageIcon rawImg = new ImageIcon("Image/Unit/gumba0.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelMusuh.getWidth(), LabelMusuh.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelMusuh.setIcon(finalimg);
            }
            else if (MainFrame.m3.nama.equals("Kuppa")) {
                ImageIcon rawImg = new ImageIcon("Image/Unit/kuppakiri0.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelMusuh.getWidth(), LabelMusuh.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelMusuh.setIcon(finalimg);
            }
        }
        LabelMario.setLocation(-100, 450);
        LabelHPMario.setText(MainFrame.player.hp + "");
        ProgressHPMario.setMinimum(0);
        ProgressHPMario.setMaximum(MainFrame.player.hpMax);
        ProgressHPMario.setValue(MainFrame.player.hp);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LabelMario = new javax.swing.JLabel();
        LabelMusuh = new javax.swing.JLabel();
        LabelHPMario = new javax.swing.JLabel();
        LabelHPMusuh = new javax.swing.JLabel();
        ProgressHPMario = new javax.swing.JProgressBar();
        ProgressHPMusuh = new javax.swing.JProgressBar();
        LabelInventory = new javax.swing.JLabel();
        LabelAttack = new javax.swing.JLabel();
        LabelPilih = new javax.swing.JLabel();
        LabelBackground = new javax.swing.JLabel();

        setLayout(null);

        LabelMario.setText("LabelMario");
        add(LabelMario);
        LabelMario.setBounds(20, 240, 80, 20);

        LabelMusuh.setText("LabelMusuh");
        add(LabelMusuh);
        LabelMusuh.setBounds(230, 170, 90, 20);

        LabelHPMario.setText("LabelHPMario");
        add(LabelHPMario);
        LabelHPMario.setBounds(20, 180, 100, 20);

        LabelHPMusuh.setText("LabelHPMusuh");
        add(LabelHPMusuh);
        LabelHPMusuh.setBounds(230, 110, 110, 20);
        add(ProgressHPMario);
        ProgressHPMario.setBounds(20, 210, 146, 14);
        add(ProgressHPMusuh);
        ProgressHPMusuh.setBounds(230, 140, 146, 14);

        LabelInventory.setText("LabelInventory");
        LabelInventory.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                LabelInventoryMouseMoved(evt);
            }
        });
        LabelInventory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelInventoryMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LabelInventoryMouseExited(evt);
            }
        });
        add(LabelInventory);
        LabelInventory.setBounds(260, 260, 105, 20);

        LabelAttack.setText("LabelAttack");
        LabelAttack.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                LabelAttackMouseMoved(evt);
            }
        });
        LabelAttack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LabelAttackMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LabelAttackMouseExited(evt);
            }
        });
        add(LabelAttack);
        LabelAttack.setBounds(290, 230, 90, 20);

        LabelPilih.setText("LabelPilih");
        add(LabelPilih);
        LabelPilih.setBounds(310, 200, 70, 20);

        LabelBackground.setText("LabelBackground");
        add(LabelBackground);
        LabelBackground.setBounds(20, 20, 120, 20);
    }// </editor-fold>//GEN-END:initComponents

    private void LabelAttackMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelAttackMouseMoved
        LabelAttack.setForeground(Color.red);
    }//GEN-LAST:event_LabelAttackMouseMoved

    private void LabelAttackMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelAttackMouseExited
        LabelAttack.setForeground(Color.black);
    }//GEN-LAST:event_LabelAttackMouseExited

    private void LabelInventoryMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelInventoryMouseMoved
        LabelInventory.setForeground(Color.red);
    }//GEN-LAST:event_LabelInventoryMouseMoved

    private void LabelInventoryMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelInventoryMouseExited
        LabelInventory.setForeground(Color.black);
    }//GEN-LAST:event_LabelInventoryMouseExited

    private void LabelAttackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelAttackMouseClicked
        Timer attackmario = new Timer(100,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(CTRMaju <= 2)
                {
                    LabelMario.setLocation(LabelMario.getX()+4, LabelMario.getY());
                    Punch.stop();
                    Punch.play();
                }
                if(CTRMaju == 5)
                {
                    ((Timer)e.getSource()).stop();
                    CTRMaju = 0;
                    Timer enemylifedrain = new Timer(100,new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (musuh == 1) {
                                MainFrame.m1.hp -= 1;
                                HPMusuh = MainFrame.m1.hp;
                            }
                            else if (musuh == 2) {
                                MainFrame.m2.hp -= 1;
                                HPMusuh = MainFrame.m2.hp;
                            }
                            else if (musuh == 3) {
                                MainFrame.m3.hp -= 1;
                                HPMusuh = MainFrame.m3.hp;
                            }
                            ProgressHPMusuh.setValue(HPMusuh);
                            LabelHPMusuh.setText(HPMusuh + "");
                            if(CTRMusuh == MainFrame.player.attack)
                            {
                                ((Timer)e.getSource()).stop();
                                CTRMusuh = 0;
                                Timer attackgoomba = new Timer(100,new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        if(CTRMaju <= 2)
                                        {
                                            LabelMusuh.setLocation(LabelMusuh.getX()-4,LabelMusuh.getY());
                                            Punch.stop();
                                            Punch.play();
                                        }
                                        if(CTRMaju == 5)
                                        {
                                            ((Timer)e.getSource()).stop();
                                            CTRMaju = 0;
                                            Timer herolifedrain = new Timer(100,new ActionListener() {
                                                @Override
                                                public void actionPerformed(ActionEvent e) {
                                                    MainFrame.player.hp -= 1;
                                                    ProgressHPMario.setValue(MainFrame.player.hp);
                                                    LabelHPMario.setText(MainFrame.player.hp+"");
                                                    if(CTR == attackMusuh)
                                                    {
                                                        ((Timer)e.getSource()).stop();
                                                        CTR = 0;
                                                    }
                                                    CTR++;
                                                }
                                            });
                                            herolifedrain.start();
                                        }
                                        else if(CTRMaju > 2)
                                        {
                                            LabelMusuh.setLocation(LabelMusuh.getX()+4,LabelMusuh.getY());                    
                                        }
                                        CTRMaju++;
                                    }
                                });
                                attackgoomba.start();
                            }
                            CTRMusuh++;
                        }
                    });
                    enemylifedrain.start();
                }
                else if(CTRMaju > 2)
                {
                    LabelMario.setLocation(LabelMario.getX()-4, LabelMario.getY());    
                }
                CTRMaju++;
            }
        });
            
        attackmario.start();
        ProgressHPMusuh.setValue(HPMusuh);
        
        if(HPMusuh - MainFrame.player.attack <= 0)
        {
            attackmario.stop();
            if (musuh == 1) {
                MainFrame.m1.hp -= MainFrame.player.attack;
            }
            if (musuh == 2) {
                MainFrame.m2.hp -= MainFrame.player.attack;
            }
            if (musuh == 3) {
                MainFrame.m3.hp -= MainFrame.player.attack;
            }
            MainFrame MainFrame = (MainFrame)this.getParent().getParent().getParent().getParent();
            MainFrame.ExitBattle();
            MainFrame.player.uang += 100;
            MainFrame.PanelBattleMusic.stop();
        }
        else if(MainFrame.player.hp - attackMusuh <= 0)
        {
            attackmario.stop();
            MainFrame.player.hp -= attackMusuh;
            MainFrame MainFrame = (MainFrame)this.getParent().getParent().getParent().getParent();
            MainFrame.ExitBattle();
            MainFrame.player.hp = 1;
            MainFrame.PanelBattleMusic.stop();
        }
    }//GEN-LAST:event_LabelAttackMouseClicked

    private void LabelInventoryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelInventoryMouseClicked
        MainFrame MainFrame = (MainFrame)this.getParent().getParent().getParent().getParent();
        MainFrame.PanelInventory.setVisible(true);
        MainFrame.PanelInventory.setInventory();
        this.setVisible(false);
    }//GEN-LAST:event_LabelInventoryMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelAttack;
    private javax.swing.JLabel LabelBackground;
    private javax.swing.JLabel LabelHPMario;
    private javax.swing.JLabel LabelHPMusuh;
    private javax.swing.JLabel LabelInventory;
    private javax.swing.JLabel LabelMario;
    private javax.swing.JLabel LabelMusuh;
    private javax.swing.JLabel LabelPilih;
    private javax.swing.JProgressBar ProgressHPMario;
    private javax.swing.JProgressBar ProgressHPMusuh;
    // End of variables declaration//GEN-END:variables
}
