/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 *
 * @author LENOVO
 */
public class BossChamber extends javax.swing.JPanel {

    /**
     * Creates new form BossChamber
     */
    boolean menang=false;
    boolean kalah=false;
    int ctr=1;
    int ctrgerak=0;
    int turnchat=0;
    int ctrchat=0;
    int ctrfight=0;
    int ctrgerakmusuh=0;
    int ctrtertawa=0;
    int waktutertawa=0;
    int langkah=0;
    int ctrgantibackground=0;
    int waktutimermusuhjalan=0;
    int ctrtimerprogressmusuh=1;
    public BossChamber() {
        initComponents();
        this.setSize(850,650);
        
        
        
        LabelBackground.setSize(850, 650);
        ImageIcon rawImg = new ImageIcon("Image/Map/Boss_Chamber1.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
        
        ProgressHpMusuh.setVisible(false);
        
        LabelMario.setSize(50,50);
        rawImg = new ImageIcon("Image/Mario/dmariokanan0.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelMario.setIcon(finalimg);
        LabelMario.setLocation(410, 600);
        
        LabelBowser.setSize(50,50);
        rawImg = new ImageIcon("Image/Unit/Bowserlaugh0.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelBowser.getWidth(), LabelBowser.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelBowser.setIcon(finalimg);
        LabelBowser.setLocation(410, 300);
        
    }
    JFXPanel j8 = new JFXPanel();
    String uri8 = new File("Sound/Bowser Laugh.mp3").toURI().toString();
    MediaPlayer BowserLaugh = new MediaPlayer(new Media(uri8));
    
    JFXPanel j9 = new JFXPanel();
    String uri9 = new File("Sound/Voice Fight.mp3").toURI().toString();
    MediaPlayer VoiceFight = new MediaPlayer(new Media(uri9));
        
    JFXPanel j10 = new JFXPanel();
    String uri10 = new File("Sound/MarioJump.mp3").toURI().toString();
    MediaPlayer MarioJump = new MediaPlayer(new Media(uri10));
    
    JFXPanel j11 = new JFXPanel();
    String uri11 = new File("Sound/Bowser Hurt.mp3").toURI().toString();
    MediaPlayer BowserHurt = new MediaPlayer(new Media(uri11));
    
    JFXPanel j12 = new JFXPanel();
    String uri12 = new File("Sound/Lose.wav").toURI().toString();
    MediaPlayer Lost = new MediaPlayer(new Media(uri12));
    
    
    int CTRGerakku=0;
    int ctrloncat=0;
    boolean kanan = true;
    boolean kiri  = false;

    
    public void loncatatas()
    {
        Timer timerloncatatas = new Timer(100,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(kanan==true)
                {
                    if(ctrloncat<4)
                    {
                        LabelMario.setLocation(LabelMario.getX(), LabelMario.getY()-40);
                        ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioloncatkanan.png");
                        Image img = rawImg.getImage();
                        Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                        ImageIcon finalimg = new ImageIcon(newImg);
                        LabelMario.setIcon(finalimg);
                    }
                    if(ctrloncat==6)
                    {
                        LabelMario.setLocation(LabelMario.getX(),560);
                        ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan0.png");
                        Image img = rawImg.getImage();
                        Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                        ImageIcon finalimg = new ImageIcon(newImg);
                        LabelMario.setIcon(finalimg);
                        ((Timer)e.getSource()).stop();
                        ctrloncat=0;
                    }
                    else if(ctrloncat>=4)
                    {
                        LabelMario.setLocation(LabelMario.getX(), LabelMario.getY()+40);
                        ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioturunkanan.png");
                        Image img = rawImg.getImage();
                        Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                        ImageIcon finalimg = new ImageIcon(newImg);
                        LabelMario.setIcon(finalimg);
                    }
                }
                if(kiri==true)
                {
                    if(ctrloncat<4)
                    {
                        LabelMario.setLocation(LabelMario.getX(), LabelMario.getY()-40);
                        ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioloncatkiri.png");
                        Image img = rawImg.getImage();
                        Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                        ImageIcon finalimg = new ImageIcon(newImg);
                        LabelMario.setIcon(finalimg);
                    }
                    if(ctrloncat==6)
                    {
                        LabelMario.setLocation(LabelMario.getX(),560);
                        ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokiri0.png");
                        Image img = rawImg.getImage();
                        Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                        ImageIcon finalimg = new ImageIcon(newImg);
                        LabelMario.setIcon(finalimg);
                        ((Timer)e.getSource()).stop();
                        ctrloncat=0;
                    }
                    else if(ctrloncat>=4)
                    {
                        LabelMario.setLocation(LabelMario.getX(), LabelMario.getY()+40);
                        ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioturunkiri.png");
                        Image img = rawImg.getImage();
                        Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                        ImageIcon finalimg = new ImageIcon(newImg);
                        LabelMario.setIcon(finalimg);
                    } 

                }
                    ctrloncat++;
                    if(LabelMario.getY()==LabelBowser.getY()-25&&(LabelMario.getX()>=LabelBowser.getX()-10&&LabelMario.getX()<=LabelBowser.getX()+10))
                    {
                        BowserHurt.stop();
                        BowserHurt.play();
                        if(kanan==true)
                        {
                            ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioloncatkanan.png");
                            Image img = rawImg.getImage();
                            Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                            ImageIcon finalimg = new ImageIcon(newImg);
                            LabelMario.setIcon(finalimg);
                            ((Timer)e.getSource()).stop();
                            ctrloncat=0;

                            Timer timerprogresshpmusuh = new Timer(10,new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    ProgressHpMusuh.setValue(ProgressHpMusuh.getValue()-1);
                                    if(ctrtimerprogressmusuh==20)
                                    {
                                        ((Timer)e.getSource()).stop();
                                        ctrtimerprogressmusuh=0;
                                        ctrloncat=0;
                                        Timer timermantulkanan = new Timer(100,new ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent e) {
                                                if(ctrloncat<4)
                                                {
                                                    LabelMario.setLocation(LabelMario.getX() + 25, LabelMario.getY()-40);
                                                    ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioloncatkanan.png");
                                                    Image img = rawImg.getImage();
                                                    Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                                    ImageIcon finalimg = new ImageIcon(newImg);
                                                    LabelMario.setIcon(finalimg);
                                                }
                                                if(ctrloncat==6)
                                                {
                                                    LabelMario.setLocation(LabelMario.getX()+25,560);
                                                    ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan0.png");
                                                    Image img = rawImg.getImage();
                                                    Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                                    ImageIcon finalimg = new ImageIcon(newImg);
                                                    LabelMario.setIcon(finalimg);
                                                    ((Timer)e.getSource()).stop();
                                                    ctrloncat=0;
                                                }
                                                else if(ctrloncat>=4)
                                                {
                                                    LabelMario.setLocation(LabelMario.getX()+ 25, LabelMario.getY()+40);
                                                    ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioturunkanan.png");
                                                    Image img = rawImg.getImage();
                                                    Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                                    ImageIcon finalimg = new ImageIcon(newImg);
                                                    LabelMario.setIcon(finalimg);
                                                } 
                                                ctrloncat++;
                                            }
                                        });
                                        timermantulkanan.start();
                                    }
                                    ctrtimerprogressmusuh++;
                                }
                            });
                            timerprogresshpmusuh.start();
                        }
                        if(kiri==true)
                        {
                            Timer timerprogresshpmusuh = new Timer(1,new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                ProgressHpMusuh.setValue(ProgressHpMusuh.getValue()-1);
                                if(ctrtimerprogressmusuh==20)
                                {
                                    ((Timer)e.getSource()).stop();
                                    ctrtimerprogressmusuh=0;
                                    ctrloncat=0;
                                    Timer timermantulkiri = new Timer(100,new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            if(ctrloncat<4)
                                            {
                                                LabelMario.setLocation(LabelMario.getX() - 25, LabelMario.getY()-40);
                                                ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioloncatkiri.png");
                                                Image img = rawImg.getImage();
                                                Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                                ImageIcon finalimg = new ImageIcon(newImg);
                                                LabelMario.setIcon(finalimg);
                                            }
                                            if(ctrloncat==6)
                                            {
                                                LabelMario.setLocation(LabelMario.getX()-25,560);
                                                ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokiri0.png");
                                                Image img = rawImg.getImage();
                                                Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                                ImageIcon finalimg = new ImageIcon(newImg);
                                                LabelMario.setIcon(finalimg);
                                                ((Timer)e.getSource()).stop();
                                                ctrloncat=0;
                                            }
                                            else if(ctrloncat>=4)
                                            {
                                                LabelMario.setLocation(LabelMario.getX()- 25, LabelMario.getY()+40);
                                                ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioturunkiri.png");
                                                Image img = rawImg.getImage();
                                                Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                                ImageIcon finalimg = new ImageIcon(newImg);
                                                LabelMario.setIcon(finalimg);
                                            } 
                                            ctrloncat++;
                                        }
                                    });
                                    timermantulkiri.start();
                                }
                                ctrtimerprogressmusuh++;
                            }
                        });
                        timerprogresshpmusuh.start();
                        }
                    }
            }
        });
        timerloncatatas.start();
    }
    public void loncatkanan()
    {
        kanan=true;
        kiri=false;
        Timer timerloncatkanan = new Timer(100,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(ctrloncat<4)
                {
                    if(LabelMario.getX()+50<810)
                    {
                    LabelMario.setLocation(LabelMario.getX() + 50, LabelMario.getY()-40);
                    ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioloncatkanan.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelMario.setIcon(finalimg);
                    }
                    else
                    {
                        LabelMario.setLocation(760,LabelMario.getY()-40);
                    }
                }
                if(ctrloncat==6)
                {
                    if(LabelMario.getX()+50<810)
                    {
                        LabelMario.setLocation(LabelMario.getX()+50,560);
                        ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan0.png");
                        Image img = rawImg.getImage();
                        Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                        ImageIcon finalimg = new ImageIcon(newImg);
                        LabelMario.setIcon(finalimg);
                        ((Timer)e.getSource()).stop();
                        ctrloncat=0;
                    }
                    else
                    {
                        LabelMario.setLocation(760,560);
                        ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan0.png");
                        Image img = rawImg.getImage();
                        Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                        ImageIcon finalimg = new ImageIcon(newImg);
                        LabelMario.setIcon(finalimg);
                        ((Timer)e.getSource()).stop();
                        ctrloncat=0;
                    }
                }
                else if(ctrloncat>=4)
                {
                    if(LabelMario.getX()+50<810)
                    {
                    LabelMario.setLocation(LabelMario.getX()+ 50, LabelMario.getY()+40);
                    ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioturunkanan.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelMario.setIcon(finalimg);
                    }
                    else
                    {
                        LabelMario.setLocation(760,LabelMario.getY()+40);
                    }
                }
                ctrloncat++;
                if(LabelMario.getY()==LabelBowser.getY()-25&&(LabelMario.getX()>=LabelBowser.getX()-10&&LabelMario.getX()<=LabelBowser.getX()+10))
                {
                    BowserHurt.stop();
                    BowserHurt.play();
                    ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioloncatkanan.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelMario.setIcon(finalimg);
                    ((Timer)e.getSource()).stop();
                    ctrloncat=0;
                    
                    Timer timerprogresshpmusuh = new Timer(10,new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            ProgressHpMusuh.setValue(ProgressHpMusuh.getValue()-1);
                            if(ctrtimerprogressmusuh==20)
                            {
                                ((Timer)e.getSource()).stop();
                                ctrtimerprogressmusuh=0;
                                ctrloncat=0;
                                Timer timermantulkanan = new Timer(100,new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        if(ctrloncat==1)
                                        {
                                            MarioJump.play();
                                        }
                                        if(ctrloncat<4)
                                        {
                                            if(LabelMario.getX()+25<810)
                                            {
                                                LabelMario.setLocation(LabelMario.getX() + 25, LabelMario.getY()-40);
                                                ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioloncatkanan.png");
                                                Image img = rawImg.getImage();
                                                Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                                ImageIcon finalimg = new ImageIcon(newImg);
                                                LabelMario.setIcon(finalimg);
                                            }
                                            else
                                            {
                                                LabelMario.setLocation(760,LabelMario.getY());
                                            }
                                        }
                                        if(ctrloncat==6)
                                        {
                                            if(LabelMario.getX()+25<810)
                                            {
                                            LabelMario.setLocation(LabelMario.getX()+25,560);
                                            ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan0.png");
                                            Image img = rawImg.getImage();
                                            Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                            ImageIcon finalimg = new ImageIcon(newImg);
                                            LabelMario.setIcon(finalimg);
                                            ((Timer)e.getSource()).stop();
                                            ctrloncat=0;
                                            }
                                            else 
                                            {
                                                LabelMario.setLocation(760, LabelMario.getY());
                                                ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan0.png");
                                                Image img = rawImg.getImage();
                                                Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                                ImageIcon finalimg = new ImageIcon(newImg);
                                                LabelMario.setIcon(finalimg);
                                                ((Timer)e.getSource()).stop();
                                                ctrloncat=0;
                                            }
                                        }
                                        else if(ctrloncat>=4)
                                        {
                                            if(LabelMario.getX()+25<810)
                                            {
                                            LabelMario.setLocation(LabelMario.getX()+ 25, LabelMario.getY()+40);
                                            ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioturunkanan.png");
                                            Image img = rawImg.getImage();
                                            Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                            ImageIcon finalimg = new ImageIcon(newImg);
                                            LabelMario.setIcon(finalimg);
                                            }
                                            else
                                            {
                                                LabelMario.setLocation(760, LabelMario.getY());
                                            }
                                        } 
                                        ctrloncat++;
                                    }
                                });
                                timermantulkanan.start();
                            }
                            ctrtimerprogressmusuh++;
                        }
                    });
                    timerprogresshpmusuh.start();
                }
            }
        });
        timerloncatkanan.start();

    }

        
        
        
        
    public void loncatkiri()
    {   
        kanan=false;
        kiri=true;
        Timer timerloncatkanan = new Timer(100,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(ctrloncat<4)
                {
                    if(LabelMario.getX()-50>10)
                    {
                        LabelMario.setLocation(LabelMario.getX() - 50, LabelMario.getY()-40);
                        ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioloncatkiri.png");
                        Image img = rawImg.getImage();
                        Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                        ImageIcon finalimg = new ImageIcon(newImg);
                        LabelMario.setIcon(finalimg);
                    }
                    else
                    {
                        LabelMario.setLocation(60, LabelMario.getY()-40);
                    }
                }
                if(ctrloncat==6)
                {
                    if(LabelMario.getX()-50>10)
                    {
                    LabelMario.setLocation(LabelMario.getX()-50,560);
                    ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokiri0.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelMario.setIcon(finalimg);
                    ((Timer)e.getSource()).stop();
                    ctrloncat=0;
                    }
                    else
                    {
                        LabelMario.setLocation(60, 560);
                        ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokiri0.png");
                        Image img = rawImg.getImage();
                        Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                        ImageIcon finalimg = new ImageIcon(newImg);
                        LabelMario.setIcon(finalimg);
                        ((Timer)e.getSource()).stop();
                        ctrloncat=0;
                    }
                }
                else if(ctrloncat>=4)
                {
                    if(LabelMario.getX()-50>10)
                    {
                    LabelMario.setLocation(LabelMario.getX()- 50, LabelMario.getY()+40);
                    ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioturunkiri.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelMario.setIcon(finalimg);
                    }
                    else
                    {
                        LabelMario.setLocation(60, LabelMario.getY()+40);
                    }
                } 
                ctrloncat++;
                
                if(LabelMario.getY()==LabelBowser.getY()-25&&(LabelMario.getX()>=LabelBowser.getX()-10&&LabelMario.getX()<=LabelBowser.getX()+10))
                {
                    BowserHurt.stop();
                    BowserHurt.play();
                    Timer timerprogresshpmusuh = new Timer(1,new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            ProgressHpMusuh.setValue(ProgressHpMusuh.getValue()-1);
                            if(ctrtimerprogressmusuh==20)
                            {
                                ((Timer)e.getSource()).stop();
                                ctrtimerprogressmusuh=0;
                                ctrloncat=0;
                                Timer timermantulkiri = new Timer(100,new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        if(ctrloncat==1)
                                        {
                                            MarioJump.play();
                                        }
                                        if(ctrloncat<4)
                                        {
                                            if(LabelMario.getX()-25>10)
                                            {
                                            LabelMario.setLocation(LabelMario.getX() - 25, LabelMario.getY()-40);
                                            ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioloncatkiri.png");
                                            Image img = rawImg.getImage();
                                            Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                            ImageIcon finalimg = new ImageIcon(newImg);
                                            LabelMario.setIcon(finalimg);
                                            }
                                            else
                                            {
                                                LabelMario.setLocation(LabelMario.getX(),LabelMario.getY()-40);
                                            }
                                        }
                                        if(ctrloncat==6)
                                        {
                                            if(LabelMario.getX()-25>10)
                                            {
                                            LabelMario.setLocation(LabelMario.getX()-25,560);
                                            ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokiri0.png");
                                            Image img = rawImg.getImage();
                                            Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                            ImageIcon finalimg = new ImageIcon(newImg);
                                            LabelMario.setIcon(finalimg);
                                            ((Timer)e.getSource()).stop();
                                            ctrloncat=0;
                                            }
                                            else
                                            {
                                                LabelMario.setLocation(60, 560);
                                                ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokiri0.png");
                                                Image img = rawImg.getImage();
                                                Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                                ImageIcon finalimg = new ImageIcon(newImg);
                                                LabelMario.setIcon(finalimg);
                                                ((Timer)e.getSource()).stop();
                                                ctrloncat=0;
                                            }
                                        }
                                        else if(ctrloncat>=4)
                                        {
                                            if(LabelMario.getX()-25>10)
                                            {
                                            LabelMario.setLocation(LabelMario.getX()- 25, LabelMario.getY()+40);
                                            ImageIcon rawImg = new ImageIcon("Image/Mario/dmarioturunkiri.png");
                                            Image img = rawImg.getImage();
                                            Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                                            ImageIcon finalimg = new ImageIcon(newImg);
                                            LabelMario.setIcon(finalimg);
                                            }
                                            else
                                            {
                                                LabelMario.setLocation(LabelMario.getX(),LabelMario.getY()+40);                                                
                                            }
                                        } 
                                        ctrloncat++;
                                    }
                                });
                                timermantulkiri.start();
                            }
                            ctrtimerprogressmusuh++;
                        }
                    });
                    timerprogresshpmusuh.start();

                }
            }
        });
        timerloncatkanan.start();
    }
    
    public void kanan()
    {
        kanan=true;
        kiri=false;
        Timer timerGerakPlayerKanan = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(LabelMario.getX()+50<810)
            {
            LabelMario.setLocation(LabelMario.getX() + 10, LabelMario.getY());
            ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan"+CTRGerakku+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelMario.setIcon(finalimg);
            }
            CTRGerakku++;
            if(CTRGerakku==5)
            {
                ((Timer)e.getSource()).stop();
                CTRGerakku=0;
            }
        }
    });
        timerGerakPlayerKanan.start();

    }
    
    public void kiri()
    {
        kiri=true;
        kanan=false;
        Timer timerGerakPlayerKiri = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(LabelMario.getX()-50>10)
            {
                LabelMario.setLocation(LabelMario.getX() - 10, LabelMario.getY());
                ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokiri"+CTRGerakku+".png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelMario.setIcon(finalimg);
            }
            CTRGerakku++;
            if(CTRGerakku==5)
            {
                ((Timer)e.getSource()).stop();
                CTRGerakku=0;
            }
        }
    });
        timerGerakPlayerKiri.start();

    }

    public void atas()
    {
         Timer timerGerakPlayerAtas = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            LabelMario.setLocation(LabelMario.getX(), LabelMario.getY() - 10);
            ImageIcon rawImg = new ImageIcon("Image/Mario/marioup"+ctrgerak+".png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelMario.setIcon(finalimg);
            ctrgerak++;
            if(ctrgerak==5)
            {
                ((Timer)e.getSource()).stop();
            }
        }
    });
         timerGerakPlayerAtas.start();
    }
    
    public void cetakchat()
    {
        if(turnchat==0)
        {
            
            LabelTalk.setSize(300,100);
            ImageIcon rawImg = new ImageIcon("Image/chatroom.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelTalk.getWidth(), LabelTalk.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelTalk.setIcon(finalimg);
            LabelTalk.setLocation(410, 500);
            LabelChatMario.setLocation(420,510);
            LabelChatMario.setFont(new Font("Consolas", Font.BOLD, 30));
            String []tampung={"It's"," Over"," Bowser"};
            Timer timerChattingMario = new Timer(300,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 LabelChatMario.setText(LabelChatMario.getText()+(tampung[ctrchat]));
                 ctrchat++;
                 if(ctrchat==3)
                 {
                     ((Timer)e.getSource()).stop();
                     ctrchat=0;
                 }
            }
        });
             timerChattingMario.start();
             turnchat++;
        }
        else if(turnchat==1)
        {
            LabelChatMario.setText("");
            LabelTalk.setLocation(410, 200);
            LabelChatMario.setLocation(400, 210);
            String[] tampung = {"  BWA ","HA ","HA ","HA ","HA "};
            Timer gantibowser = new Timer(100,new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ImageIcon rawImg = new ImageIcon("Image/Unit/Bowserlaugh"+ctrtertawa+".png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelBowser.getWidth(), LabelBowser.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelBowser.setIcon(finalimg);
                    ctrtertawa++;
                    if(ctrtertawa==14)
                    {
                        ctrtertawa=0;
                    }
                    if(waktutertawa==28)
                    {
                        ((Timer)e.getSource()).stop();
                        waktutertawa=0;
                    }
                    waktutertawa++;
                }
            });
            Timer timerChattingMusuh = new Timer(300,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 LabelChatMario.setText(LabelChatMario.getText()+(tampung[ctrchat]));
                 if(ctrchat==4)
                 {
                     ((Timer)e.getSource()).stop();
                     ctrchat=0;
                 }
                 ctrchat++;
            }
        });
            timerChattingMusuh.start();
            BowserLaugh.play();
            gantibowser.start();

            
            
            turnchat++;
        }
        else if(turnchat==2)
        {
            ctrchat=0;
            LabelChatMario.setText("");
            LabelChatMario.setSize(300, 100);
            LabelTalk.setLocation(410, 200);
            LabelChatMario.setLocation(410, 190);
            String[] tampung = {" Let's "," Fight!!!"};
            Timer timerChattingMusuh2 = new Timer(300,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 LabelChatMario.setText(LabelChatMario.getText()+(tampung[ctrchat]));
                 if(ctrchat==1)
                 {
                     ((Timer)e.getSource()).stop();
                     ctrchat=0;
                 }
                 ctrchat++;
            }
        });
            timerChattingMusuh2.start();
            turnchat++;
        }
        else if(turnchat==3)
        {
            LabelChatMario.setText("");
            LabelTalk.setSize(0,0);
            Timer timerJalanBackground = new Timer(300,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(ctr<10)
                {
                    ImageIcon rawImg = new ImageIcon("Image/Map/Boss_Chamber"+ctr+".png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelBackground.setIcon(finalimg);
                }
                ctr++;
                if(ctr==10)
                {
                    ImageIcon rawImg = new ImageIcon("Image/Black.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelBackground.setIcon(finalimg);
                    
                    rawImg = new ImageIcon("Image/Black.png");
                    img = rawImg.getImage();
                    newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                    finalimg = new ImageIcon(newImg);
                    LabelMario.setIcon(finalimg);
                    
                    rawImg = new ImageIcon("Image/Black.png");
                    img = rawImg.getImage();
                    newImg = img.getScaledInstance(LabelBowser.getWidth(), LabelBowser.getHeight(), Image.SCALE_SMOOTH);
                    finalimg = new ImageIcon(newImg);
                    LabelBowser.setIcon(finalimg);
                }
                if(ctr==11)
                {
                    ImageIcon rawImg = new ImageIcon("Image/Map/Boss_Chamber10.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelBackground.setIcon(finalimg);
                    
                    rawImg = new ImageIcon("Image/Mario/dmariokanan0.png");
                    img = rawImg.getImage();
                    newImg = img.getScaledInstance(LabelMario.getWidth(), LabelMario.getHeight(), Image.SCALE_SMOOTH);
                    finalimg = new ImageIcon(newImg);
                    LabelMario.setIcon(finalimg);
                    
                    LabelBowser.setSize(70,70);
                    rawImg = new ImageIcon("Image/Unit/Bowserkiri0.png");
                    img = rawImg.getImage();
                    newImg = img.getScaledInstance(LabelBowser.getWidth(), LabelBowser.getHeight(), Image.SCALE_SMOOTH);
                    finalimg = new ImageIcon(newImg);
                    LabelBowser.setIcon(finalimg);
                    LabelBowser.setLocation(740, 545);
                    LabelMario.setLocation(60, 560);
                }
                if(ctr==12)
                {
                    ProgressHpMusuh.setVisible(true);
                    ProgressHpMusuh.setLocation(90, 200);
                    ProgressHpMusuh.setSize(700, 50);
                    ProgressHpMusuh.setValue(0);
                    ProgressHpMusuh.setMinimum(0);
                    ProgressHpMusuh.setMaximum(100);
                    ProgressHpMusuh.setBackground(Color.red);
                    Timer isihp = new Timer(10,new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            ProgressHpMusuh.setValue(ProgressHpMusuh.getValue()+1);
                            if(ProgressHpMusuh.getValue()==100)
                            {
                                ((Timer)e.getSource()).stop();
                                VoiceFight.play();
                                Fight.setSize(200, 200);
                                Fight.setLocation(370, 300);
                                ImageIcon rawImg = new ImageIcon("Image/Fight.png");
                                Image img = rawImg.getImage();
                                Image newImg = img.getScaledInstance(Fight.getWidth(), Fight.getHeight(), Image.SCALE_SMOOTH);
                                ImageIcon finalimg = new ImageIcon(newImg);
                                Fight.setIcon(finalimg);
                                Timer keluarfight = new Timer(100,new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        if(ctrfight==5)
                                        {
                                            ((Timer)e.getSource()).stop();
                                            Fight.setSize(0,0);
                                            Timer timermusuhjalan = new Timer(100,new ActionListener() {
                                                @Override
                                                public void actionPerformed(ActionEvent e) {
                                                    int random = (int)(Math.random()*2);
                                                    if(LabelBowser.getX()<=LabelMario.getX() )//kanan
                                                    {
                                                        if(LabelBowser.getX()+50<810)
                                                        {
                                                            for (int i = 0; i < 2; i++) {
                                                                LabelBowser.setLocation(LabelBowser.getX() + 5, LabelBowser.getY());
                                                                ImageIcon rawImg = new ImageIcon("Image/Unit/Bowserkanan"+ctrgerakmusuh+".png");
                                                                Image img = rawImg.getImage();
                                                                Image newImg = img.getScaledInstance(LabelBowser.getWidth(), LabelBowser.getHeight(), Image.SCALE_SMOOTH);
                                                                ImageIcon finalimg = new ImageIcon(newImg);
                                                                LabelBowser.setIcon(finalimg);
                                                            }
                                                        }
                                                    }
                                                    else if(LabelBowser.getX()>=LabelMario.getX())//kiri
                                                    {
                                                        if(LabelBowser.getX()-50>10)
                                                        {
                                                            for (int i = 0; i < 2; i++) {
                                                                LabelBowser.setLocation(LabelBowser.getX() - 5, LabelBowser.getY());
                                                                ImageIcon rawImg = new ImageIcon("Image/Unit/Bowserkiri"+ctrgerakmusuh+".png");
                                                                Image img = rawImg.getImage();
                                                                Image newImg = img.getScaledInstance(LabelBowser.getWidth(), LabelBowser.getHeight(), Image.SCALE_SMOOTH);
                                                                ImageIcon finalimg = new ImageIcon(newImg);
                                                                LabelBowser.setIcon(finalimg);
                                                            }
                                                        }
                                                    }
                                                    ctrgerakmusuh++;
                                                    
                                                    if(ProgressHpMusuh.getValue()<=0)
                                                    {   ctrchat=0;
                                                        ((Timer)e.getSource()).stop();
                                                        LabelTalk.setSize(300, 100);
                                                        LabelTalk.setLocation(LabelBowser.getX(), LabelBowser.getY()-100);
                                                        LabelChatMario.setLocation(LabelTalk.getX()-10, LabelTalk.getY()-20);
                                                        String[] tampung = {" BWA ","HA ","HA ","HA ","HA "};
                                                        BowserLaugh.play();
                                                        Timer gantibowser = new Timer(100,new ActionListener() {
                                                        @Override
                                                        public void actionPerformed(ActionEvent e) {
                                                            ImageIcon rawImg = new ImageIcon("Image/Unit/Bowserlaugh"+ctrtertawa+".png");
                                                            Image img = rawImg.getImage();
                                                            Image newImg = img.getScaledInstance(LabelBowser.getWidth(), LabelBowser.getHeight(), Image.SCALE_SMOOTH);
                                                            ImageIcon finalimg = new ImageIcon(newImg);
                                                            LabelBowser.setIcon(finalimg);
                                                            ctrtertawa++;
                                                            if(ctrtertawa==14)
                                                            {
                                                                ctrtertawa=0;
                                                            }
                                                            if(waktutertawa==28)
                                                            {
                                                                ((Timer)e.getSource()).stop();
                                                            }
                                                            waktutertawa++;
                                                        }
                                                    });
                                                        gantibowser.start();
                                                        BowserLaugh.play();
                                                        Timer timerChattingMusuh = new Timer(300,new ActionListener() {
                                                            @Override
                                                            public void actionPerformed(ActionEvent e) { 
                                                                LabelChatMario.setText(LabelChatMario.getText()+(tampung[ctrchat]));
                                                                ctrchat++;
                                                                if(ctrchat==5)
                                                                {
                                                                    ((Timer)e.getSource()).stop();
                                                                    ctrchat=0;
                                                                }
                                                            }
                                                        });
                                                        timerChattingMusuh.start();
                                                        turnchat++;
                                                    }
                                                    if(ctrgerakmusuh==10)
                                                    {
                                                        ctrgerakmusuh=0;
                                                    }
                                                }
                                            });
                                            timermusuhjalan.start();
                                        }
                                        ctrfight++;
                                    }
                                });
                                keluarfight.start();
                            }
                        }
                    });
                    isihp.start();
                }
                if(ProgressHpMusuh.getValue()==100)
                {
                    ((Timer)e.getSource()).stop();
                }

            }
        });
            timerJalanBackground.start();
            
        }
        else if(turnchat==4)
        {
            LabelChatMario.setText("");
            String[] tampung = {" It's ","Not ","Over ","Yet "};
            Timer timerChattingMusuh = new Timer(300,new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) { 
                    LabelChatMario.setText(LabelChatMario.getText()+(tampung[ctrchat]));
                    ctrchat++; 
                    if(ctrchat==4)
                    {
                        ((Timer)e.getSource()).stop();
                        ctrchat=0;
                    }
                }
            });
            timerChattingMusuh.start();
            turnchat++;
        }
        else if(turnchat==5)
        {
            menang=true;   
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Fight = new javax.swing.JLabel();
        ProgressHpMusuh = new javax.swing.JProgressBar();
        LabelBowser = new javax.swing.JLabel();
        LabelChatMario = new javax.swing.JLabel();
        LabelTalk = new javax.swing.JLabel();
        LabelMario = new javax.swing.JLabel();
        LabelBackground = new javax.swing.JLabel();

        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });
        setLayout(null);

        Fight.setText("Fight");
        add(Fight);
        Fight.setBounds(10, 230, 35, 20);
        add(ProgressHpMusuh);
        ProgressHpMusuh.setBounds(10, 180, 380, 30);

        LabelBowser.setText("LabelBowser");
        add(LabelBowser);
        LabelBowser.setBounds(10, 100, 90, 20);
        add(LabelChatMario);
        LabelChatMario.setBounds(10, 120, 360, 50);

        LabelTalk.setText("LabelTalk");
        add(LabelTalk);
        LabelTalk.setBounds(10, 70, 90, 20);

        LabelMario.setText("LabelMario");
        add(LabelMario);
        LabelMario.setBounds(10, 43, 76, 20);

        LabelBackground.setText("LabelBackground");
        add(LabelBackground);
        LabelBackground.setBounds(10, 11, 120, 20);
    }// </editor-fold>//GEN-END:initComponents
    
    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        
    }//GEN-LAST:event_formKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Fight;
    private javax.swing.JLabel LabelBackground;
    private javax.swing.JLabel LabelBowser;
    private javax.swing.JLabel LabelChatMario;
    private javax.swing.JLabel LabelMario;
    private javax.swing.JLabel LabelTalk;
    private javax.swing.JProgressBar ProgressHpMusuh;
    // End of variables declaration//GEN-END:variables
}
