/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javax.swing.JOptionPane;
import javax.swing.Timer;


/**
 *
 * @author Hendardi
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainFrame
     */
    int panjang = 875;
    int lebar = 710;
    int tempX = 0;
    int tekan = 0;
    
    public static Player player = null;
    public static NPC NPC = null;
    public static Item item = null;
    public static Musuh m1 = null;
    public static Musuh m2 = null;
    public static Musuh m3 = null;
    public static int waktuReset = 45;
    
    boolean visibleWorldMap = false;
    boolean visibleFireMap = false;
    boolean visibleIceMap = false;
    boolean visibleBossMap = false;
    boolean visibleFireDungeon = false;
    boolean visibleIceDungeon = false;
    boolean visiblePanelBattle = false;
    boolean visibleFirePuzzle = false;
    boolean visibleIcePuzzle = false;
    public static boolean kiri = false;
    public static boolean kanan = false;
    
    MainMenu MainMenu = new MainMenu();
    WorldMap WorldMap = new WorldMap();
    PanelInventory PanelInventory = new PanelInventory();
    PanelQuestInventory PanelQuestInventory = new PanelQuestInventory();
    PanelNPC PanelNPC  = new PanelNPC();
    PanelQuest PanelQuest = new PanelQuest();
    IceMap IceMap = new IceMap();
    FireMap FireMap = new FireMap();
    BossMap BossMap = new BossMap();
    FireDungeon FireDungeon = new FireDungeon();
    IceDungeon IceDungeon = new IceDungeon();
    FirePuzzle FirePuzzle = new FirePuzzle();
    IcePuzzle IcePuzzle = new IcePuzzle();
    BossChamber BossChamber = new BossChamber();
    PanelBattle PanelBattle = new PanelBattle();
    PanelVictory PanelVictory = new PanelVictory();
    
    //Sound
    JFXPanel j = new JFXPanel();
    String uri = new File("Sound/Select.mp3").toURI().toString();
    MediaPlayer Select = new MediaPlayer(new Media(uri));
    
    JFXPanel j2 = new JFXPanel();
    String uri2 = new File("Sound/Main Menu.mp3").toURI().toString();
    MediaPlayer MainMusic = new MediaPlayer(new Media(uri2));
    
    JFXPanel j3 = new JFXPanel();
    String uri3 = new File("Sound/World Map.mp3").toURI().toString();
    MediaPlayer WorldMapMusic = new MediaPlayer(new Media(uri3));
    
    JFXPanel j4 = new JFXPanel();
    String uri4 = new File("Sound/Ice Map.mp3").toURI().toString();
    MediaPlayer IceMapMusic = new MediaPlayer(new Media(uri4));
    
    JFXPanel j5 = new JFXPanel();
    String uri5 = new File("Sound/Fire Map.mp3").toURI().toString();
    MediaPlayer FireMapMusic = new MediaPlayer(new Media(uri5));
    
    JFXPanel j6 = new JFXPanel();
    String uri6 = new File("Sound/Boss Map.mp3").toURI().toString();
    MediaPlayer BossMapMusic = new MediaPlayer(new Media(uri6));
    
    JFXPanel j7 = new JFXPanel();
    String uri7 = new File("Sound/Fight.mp3").toURI().toString();
    MediaPlayer PanelBattleMusic = new MediaPlayer(new Media(uri7));
    
    JFXPanel j8 = new JFXPanel();
    String uri8 = new File("Sound/Super Mario Bross.mp3").toURI().toString();
    MediaPlayer DungeonMusic = new MediaPlayer(new Media(uri8));
    
    JFXPanel j9 = new JFXPanel();
    String uri9 = new File("Sound/MarioJump.mp3").toURI().toString();
    MediaPlayer MarioJump = new MediaPlayer(new Media(uri9));
    
    JFXPanel j10 = new JFXPanel();
    String uri10 = new File("Sound/Victory Sound.mp3").toURI().toString();
    MediaPlayer VictoryMusic = new MediaPlayer(new Media(uri10));
    
    
    Timer UTimer = new Timer(10, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            player.waktuDecimal += 0.01;
            player.waktu = (int)player.waktuDecimal;
            //Ganti Hari
            if (player.waktu % waktuReset == 0) {
                player.waktuDecimal = 1;
                PanelQuest.resetQuest();
                PanelQuestInventory.resetQuest();
                
                if (player.hari == 0) {
                    player.hari = 1;
                    WorldMap.setHariMalam();
                    PanelInventory.setHariMalam();
                    FireMap.setHariMalam();
                    IceMap.setHariMalam();
                    BossMap.setHariMalam();
                    FireDungeon.setHariMalam();
                    IceDungeon.setHariMalam();
                }
                else if (player.hari == 1) {
                    player.hari = 0;
                    WorldMap.setHariSiang();
                    PanelInventory.setHariSiang();
                    FireMap.setHariSiang();
                    IceMap.setHariSiang();
                    BossMap.setHariSiang();
                    FireDungeon.setHariSiang();
                    IceDungeon.setHariSiang();
                }
            }
            if(BossChamber.kalah == true){
                PanelBattleMusic.stop();
                MainMenu.setVisible(true);
                MainMenu.setVisible(false);
                MainMusic.stop();
                MainMusic.play();
                BossChamber.kalah=false;
            }
            //Stop WorldMap Greeting Timer
            if (WorldMap.CTRGreeting == 1) {
                WorldMap.timerGreeting.stop();
                WorldMap.CTRGreeting = 0;
            }
            //Stop WorldMap Gerak Player Timer
            if (WorldMap.CTRGerak == 4) {
                WorldMap.timerGerakPlayerAtas.stop();
                WorldMap.timerGerakPlayerBawah.stop();
                WorldMap.timerGerakPlayerKiri.stop();
                WorldMap.timerGerakPlayerKanan.stop();
                WorldMap.CTRGerak = 0;
                WorldMap.setXYLabelPlayer();
            }
            //Stop FireMap Gerak Player Timer
            if (FireMap.CTRGerak == 4) {
                FireMap.timerGerakPlayerAtas.stop();
                FireMap.timerGerakPlayerBawah.stop();
                FireMap.timerGerakPlayerKiri.stop();
                FireMap.timerGerakPlayerKanan.stop();
                FireMap.CTRGerak = 0;
                FireMap.setXYLabelPlayer();
            }
            //Stop IceMap Gerak Player Timer
            if (IceMap.CTRGerak == 4) {
                IceMap.timerGerakPlayerAtas.stop();
                IceMap.timerGerakPlayerBawah.stop();
                IceMap.timerGerakPlayerKiri.stop();
                IceMap.timerGerakPlayerKanan.stop();
                IceMap.CTRGerak = 0;
                IceMap.setXYLabelPlayer();
            }
            //Stop BossMap Gerak Player Timer
            if (BossMap.CTRGerak == 4) {
                BossMap.timerGerakPlayerAtas.stop();
                BossMap.timerGerakPlayerBawah.stop();
                BossMap.timerGerakPlayerKiri.stop();
                BossMap.timerGerakPlayerKanan.stop();
                BossMap.CTRGerak = 0;
                BossMap.setXYLabelPlayer();
            }
            //Stop FireDungeon Gerak Player Timer
            if (FireDungeon.CTRGerak == 5) {
                FireDungeon.timerGerakPlayerKiri.stop();
                FireDungeon.timerGerakPlayerKanan.stop();
                FireDungeon.timerGerakPlayerIddle.stop();
                FireDungeon.CTRGerak = 0;
                FireDungeon.timerGerakPlayerJatuh.start();
            }
            if (FireDungeon.CTRGerakL == 26 || FireDungeon.gerak == false) {
                FireDungeon.timerGerakPlayerLoncatKiri.stop();
                FireDungeon.timerGerakPlayerLoncatKanan.stop();
                FireDungeon.CTRGerakL = 0;
                FireDungeon.gerak = true;
                FireDungeon.timerGerakPlayerJatuh.start();
            }
            if (FireDungeon.gerakJatuh == false) {
                FireDungeon.timerGerakPlayerJatuh.stop();
                FireDungeon.gerakJatuh = true;
            }
            //Stop IceDungeon Gerak Player Timer
            if (IceDungeon.CTRGerak == 5) {
                IceDungeon.timerGerakPlayerKiri.stop();
                IceDungeon.timerGerakPlayerKanan.stop();
                IceDungeon.timerGerakPlayerIddle.stop();
                IceDungeon.CTRGerak = 0;
                IceDungeon.timerGerakPlayerJatuh.start();
            }
            if (IceDungeon.CTRGerakL == 26 || IceDungeon.gerak == false) {
                IceDungeon.timerGerakPlayerLoncatKiri.stop();
                IceDungeon.timerGerakPlayerLoncatKanan.stop();
                IceDungeon.CTRGerakL = 0;
                IceDungeon.gerak = true;
                IceDungeon.timerGerakPlayerJatuh.start();
            }
            if(IceDungeon.slide == false){
                IceDungeon.timerGerakPlayerSlideKanan.stop();
                IceDungeon.timerGerakPlayerSlideKiri.stop();
                IceDungeon.slide = true;
            }
            if (IceDungeon.gerakJatuh == false) {
                IceDungeon.timerGerakPlayerJatuh.stop();
                IceDungeon.gerakJatuh = true;
            }
            //Stop FirePuzzle Gerak Player Timer
            if (FirePuzzle.CTRGerak == 5) {
                FirePuzzle.timerGerakPlayerAtas.stop();
                FirePuzzle.timerGerakPlayerBawah.stop();
                FirePuzzle.timerGerakPlayerKiri.stop();
                FirePuzzle.timerGerakPlayerKanan.stop();
                FirePuzzle.CTRGerak = 0;
                FirePuzzle.setXYLabelPlayer();
                if (FirePuzzle.FirePuzzleTerrain[player.y][player.x] == "D") {
                    JOptionPane.showMessageDialog(null, "You Died");
                    player.x = 8;
                    player.y = 12;
                    FirePuzzle.setXYLabelPlayer();
                }
            }
            if (FirePuzzle.CTRGerakL == 21) {
                FirePuzzle.timerGerakPlayerLoncatKiri.stop();
                FirePuzzle.timerGerakPlayerLoncatKanan.stop();
                FirePuzzle.CTRGerakL = 0;
                FirePuzzle.setXYLabelPlayer();
                if (FirePuzzle.FirePuzzleTerrain[player.y][player.x] == "D") {
                    JOptionPane.showMessageDialog(null, "You Died");
                    player.x = 8;
                    player.y = 12;
                    FirePuzzle.setXYLabelPlayer();
                }
                else if (FirePuzzle.FirePuzzleTerrain[player.y][player.x] == "E") {
                    if (player.boolClearFireDungeon == false) {
                        JOptionPane.showMessageDialog(null, "Earthquake Happen");
                        player.boolClearFireDungeon = true;
                    }
                    else if (player.boolClearFireDungeon == true) {
                        JOptionPane.showMessageDialog(null, "Nothing Happen");
                    }
                    DungeonMusic.stop();
                    WorldMapMusic.play();
                    FireDungeon.besar = false;
                    FirePuzzle.setVisible(false);
                    WorldMap.setVisible(true);
                    player.x = 8;
                    player.y = 9;
                    WorldMap.setXYLabelPlayer();
                }
            }
            //Stop IcePuzzle Gerak Player Timer
            if (IcePuzzle.CTRGerak == false) {
                IcePuzzle.timerGerakPlayerAtas.stop();
                IcePuzzle.timerGerakPlayerBawah.stop();
                IcePuzzle.timerGerakPlayerKiri.stop();
                IcePuzzle.timerGerakPlayerKanan.stop();
                IcePuzzle.CTRGerak = true;
                IcePuzzle.setXYLabelPlayer();
                if (IcePuzzle.IcePuzzleTerrain[player.y - 1][player.x] == "E") {
                    if (player.boolClearIceDungeon == false) {
                        JOptionPane.showMessageDialog(null, "Earthquake Happen");
                        player.boolClearIceDungeon = true;
                    }
                    else if (player.boolClearIceDungeon == true) {
                        JOptionPane.showMessageDialog(null, "Nothing Happen");
                    }
                    DungeonMusic.stop();
                    WorldMapMusic.play();
                    IceDungeon.besar = false;
                    IceDungeon.timerGerakMusuh.stop();
                    IceDungeon.CTRGerakM = 0;
                    IceDungeon.CTRArahM = 0;
                    IcePuzzle.setVisible(false);
                    WorldMap.setVisible(true);
                    player.x = 8;
                    player.y = 9;
                    WorldMap.setXYLabelPlayer();
                }
            }
            //Stop Fire Dungeon Timer Star
            if (FireDungeon.CTRStar == 5) {
                FireDungeon.timerStar.stop();
                FireDungeon.star = false;
                FireDungeon.CTRStar = 0;
            }
            //Stop Fire Dungeon Fire
            if (FireDungeon.CTRFire == 0) {
                FireDungeon.timerFire.stop();
                FireDungeon.fire = false;
                FireDungeon.CTRFire = 10;
            }
            if(FireDungeon.CTRShowFire == 0){
                FireDungeon.timerShowFire.stop();
                FireDungeon.ShowFire = false;
                FireDungeon.stop_tembak();
                FireDungeon.CTRShowFire = 1;
            }
            //Stop Ice Dungeon Timer Star
            if (IceDungeon.CTRStar == 5) {
                IceDungeon.timerStar.stop();
                IceDungeon.star = false;
                IceDungeon.CTRStar = 0;
            }
            //Stop Ice Dungeon Fire
            if (IceDungeon.CTRFire == 0) {
                IceDungeon.timerFire.stop();
                IceDungeon.fire = false;
                IceDungeon.CTRFire = 10;
            }
            if(IceDungeon.CTRShowFire == 0){
                IceDungeon.timerShowFire.stop();
                IceDungeon.ShowFire = false;
                IceDungeon.stop_tembak();
                IceDungeon.CTRShowFire = 1;
            }
            //Check Player Mati
            if (MainFrame.player.hp <= 0) {
                MainFrame.player.hp = 1;
            }
            //Check Collision
            if (FireDungeon.isVisible() == true) {
                if (FireDungeon.star == true && m1.hp > 0 && player.y == m1.y && player.x == m1.x) {
                    player.InstantKill(m1);
                    player.uang += 100;
                }
                else if (FireDungeon.besar == false && m1.hp > 0 && player.y == m1.y && player.x == m1.x) {
                    if (m1.nama.equals("Kuppa Shell")) {
                        m1.hp = 0;
                        player.uang += 100;
                    }
                    else {
                        FireDungeon.setVisible(false);
                        FireDungeon.timerDamageOverTime.stop();
                        visibleFireDungeon = true;
                        visiblePanelBattle = true;
                        DungeonMusic.stop();
                        PanelBattle.musuh = 1;
                        PanelBattle.setBattle();
                        PanelBattleMusic.play();
                        PanelBattle.setVisible(true);
                        PanelBattle.timerJalanMasuk.start();
                    }
                }
                else if (FireDungeon.besar == true && m1.hp > 0 && player.y == m1.y && player.x == m1.x) {
                    player.InstantKill(m1);
                    player.uang += 100;
                    player = new Player(player);
                    FireDungeon.besar = false;
                }
                
                if (FireDungeon.star == true && m2.hp > 0 && player.y == m2.y && player.x == m2.x) {
                    player.InstantKill(m2);
                    player.uang += 100;
                }
                else if (FireDungeon.besar == false && m2.hp > 0 && player.y == m2.y && player.x == m2.x) {
                    if (m2.nama.equals("Kuppa Shell")) {
                        m2.hp = 0;
                        player.uang += 100;
                    }
                    else {
                        FireDungeon.setVisible(false);
                        FireDungeon.timerDamageOverTime.stop();
                        visibleFireDungeon = true;
                        visiblePanelBattle = true;
                        DungeonMusic.stop();
                        PanelBattle.musuh = 2;
                        PanelBattle.setBattle();
                        PanelBattleMusic.play();
                        PanelBattle.setVisible(true);
                        PanelBattle.timerJalanMasuk.start();
                    }
                }
                else if (FireDungeon.besar == true && m2.hp > 0 && player.y == m2.y && player.x == m2.x) {
                    player.InstantKill(m2);
                    player.uang += 100;
                    player = new Player(player);
                    FireDungeon.besar = false;
                }
                
                if (FireDungeon.star == true && m3.hp > 0 && player.y == m3.y && player.x == m3.x) {
                    player.InstantKill(m3);
                    player.uang += 100;
                }
                else if (FireDungeon.besar == false && m3.hp > 0 && player.y == m3.y && player.x == m3.x) {
                    if (m3.nama.equals("Kuppa Shell")) {
                        m3.hp = 0;
                        player.uang += 100;
                    }
                    else {
                        FireDungeon.setVisible(false);
                        FireDungeon.timerDamageOverTime.stop();
                        visibleFireDungeon = true;
                        visiblePanelBattle = true;
                        DungeonMusic.stop();
                        PanelBattle.musuh = 3;
                        PanelBattle.setBattle();
                        PanelBattleMusic.play();
                        PanelBattle.setVisible(true);
                        PanelBattle.timerJalanMasuk.start();
                    }
                }
                else if (FireDungeon.besar == true && m3.hp > 0 && player.y == m3.y && player.x == m3.x) {
                    player.InstantKill(m3);
                    player.uang += 100;
                    player = new Player(player);
                    FireDungeon.besar = false;
                }
            }
            if (IceDungeon.isVisible() == true) {
                if (IceDungeon.star == true && m1.hp > 0 && player.y == m1.y && player.x == m1.x) {
                    player.InstantKill(m1);
                    player.uang += 100;
                }
                else if (IceDungeon.besar == false && m1.hp > 0 && player.y == m1.y && player.x == m1.x) {
                    if (m1.nama.equals("Kuppa Shell")) {
                        m1.hp = 0;
                        player.uang += 100;
                    }
                    else {
                        IceDungeon.setVisible(false);
                        visibleIceDungeon = true;
                        visiblePanelBattle = true;
                        DungeonMusic.stop();
                        PanelBattle.musuh = 1;
                        PanelBattle.setBattle();
                        PanelBattleMusic.play();
                        PanelBattle.setVisible(true);
                        PanelBattle.timerJalanMasuk.start();
                    }
                }
                else if (IceDungeon.besar == true && m1.hp > 0 && player.y == m1.y && player.x == m1.x) {
                    player.InstantKill(m1);
                    player.uang += 100;
                    player = new Player(player);
                    IceDungeon.besar = false;
                }
                
                if (IceDungeon.star == true && m2.hp > 0 && player.y == m2.y && player.x == m2.x) {
                    player.InstantKill(m2);
                    player.uang += 100;
                }
                else if (IceDungeon.besar == false && m2.hp > 0 && player.y == m2.y && player.x == m2.x) {
                    if (m2.nama.equals("Kuppa Shell")) {
                        m2.hp = 0;
                        player.uang += 100;
                    }
                    else {
                        IceDungeon.setVisible(false);
                        visibleIceDungeon = true;
                        visiblePanelBattle = true;
                        DungeonMusic.stop();
                        PanelBattle.musuh = 2;
                        PanelBattle.setBattle();
                        PanelBattleMusic.play();
                        PanelBattle.setVisible(true);
                        PanelBattle.timerJalanMasuk.start();
                    }
                }
                else if (IceDungeon.besar == true && m2.hp > 0 && player.y == m2.y && player.x == m2.x) {
                    player.InstantKill(m2);
                    player.uang += 100;
                    player = new Player(player);
                    IceDungeon.besar = false;
                }
                
                if (IceDungeon.star == true && m3.hp > 0 && player.y == m3.y && player.x == m3.x) {
                    player.InstantKill(m3);
                    player.uang += 100;
                }
                else if (IceDungeon.besar == false && m3.hp > 0 && player.y == m3.y && player.x == m3.x) {
                    if (m3.nama.equals("Kuppa Shell")) {
                        m3.hp = 0;
                        player.uang += 100;
                    }
                    else {
                        IceDungeon.setVisible(false);
                        visibleIceDungeon = true;
                        visiblePanelBattle = true;
                        DungeonMusic.stop();
                        PanelBattle.musuh = 3;
                        PanelBattle.setBattle();
                        PanelBattleMusic.play();
                        PanelBattle.setVisible(true);
                        PanelBattle.timerJalanMasuk.start();
                    }
                }
                else if (IceDungeon.besar == true && m3.hp > 0 && player.y == m3.y && player.x == m3.x) {
                    player.InstantKill(m3);
                    player.uang += 100;
                    player = new Player(player);
                    IceDungeon.besar = false;
                }
            }
            if (FireDungeon.isVisible() == true && FireDungeon.CTRGerakL >= 20) {
                if (m1.hp > 0 && player.y + 1 == m1.y && player.x == m1.x) {
                    if (m1.nama.equals("Kuppa")) {
                        m1.nama = "Kuppa Shell";
                        m1.dx = 0;
                        Kanan();
                        FireDungeon.timerSetLabelPlayerKanan.start();
                        FireDungeon.setXYLabelPlayer();
                        FireDungeon.setXYLabelMusuh();
                        FireDungeon.setKuppaShell(1);
                    }
                    else{
                        m1.hp = 0;
                        player.uang += 100;
                    }
                }
                if (m2.hp > 0 && player.y + 1 == m2.y && player.x == m2.x) {
                    if (m2.nama.equals("Kuppa")) {
                        m2.nama = "Kuppa Shell";
                        m2.dx = 0;
                        Kanan();
                        FireDungeon.timerSetLabelPlayerKanan.start();
                        FireDungeon.setXYLabelPlayer();
                        FireDungeon.setXYLabelMusuh();
                        FireDungeon.setKuppaShell(2);
                    }
                    else{
                        m2.hp = 0;
                        player.uang += 100;
                    }
                }
                if (m3.hp > 0 && player.y + 1 == m3.y && player.x == m3.x) {
                    if (m3.nama.equals("Kuppa")) {
                        m3.nama = "Kuppa Shell";
                        m3.dx = 0;
                        Kanan();
                        FireDungeon.timerSetLabelPlayerKanan.start();
                        FireDungeon.setXYLabelPlayer();
                        FireDungeon.setXYLabelMusuh();
                        FireDungeon.setKuppaShell(3);
                    }
                    else{
                        m3.hp = 0;
                        player.uang += 100;
                    }
                }
            }
            if (IceDungeon.isVisible() == true && IceDungeon.CTRGerakL >= 20) {
                if (m1.hp > 0 && player.y + 1 == m1.y && player.x == m1.x) {
                    if (m1.nama.equals("Kuppa")) {
                        m1.nama = "Kuppa Shell";
                        m1.dx = 0;
                        Kanan();
                        IceDungeon.timerSetLabelPlayerKanan.start();
                        IceDungeon.setXYLabelPlayer();
                        IceDungeon.setXYLabelMusuh();
                        IceDungeon.setKuppaShell(1);
                    }
                    else{
                        m1.hp = 0;
                        player.uang += 100;
                    }
                }
                if (m2.hp > 0 && player.y + 1 == m2.y && player.x == m2.x) {
                    if (m2.nama.equals("Kuppa")) {
                        m2.nama = "Kuppa Shell";
                        m2.dx = 0;
                        Kanan();
                        IceDungeon.timerSetLabelPlayerKanan.start();
                        IceDungeon.setXYLabelPlayer();
                        IceDungeon.setXYLabelMusuh();
                        IceDungeon.setKuppaShell(2);
                    }
                    else{
                        m2.hp = 0;
                        player.uang += 100;
                    }
                }
                if (m3.hp > 0 && player.y + 1 == m3.y && player.x == m3.x) {
                    if (m3.nama.equals("Kuppa")) {
                        m3.nama = "Kuppa Shell";
                        m3.dx = 0;
                        Kanan();
                        IceDungeon.timerSetLabelPlayerKanan.start();
                        IceDungeon.setXYLabelPlayer();
                        IceDungeon.setXYLabelMusuh();
                        IceDungeon.setKuppaShell(3);
                    }
                    else{
                        m3.hp = 0;
                        player.uang += 100;
                    }
                }
            }
            if(BossChamber.isVisible()==true)
            {
                if(BossChamber.menang==true)
                {
                    BossChamber.setVisible(false);
                    PanelBattleMusic.stop();
                    PanelVictory.setVisible(true);
                    VictoryMusic.play();
                }
            }
        }
    });
    
    Timer timerColdDrink = new Timer(10000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            player.coldDrink = false;
            PanelInventory.setLabelColdDrink();
        }
    });
    
    public MainFrame() {
        initComponents();
        this.setSize(panjang,lebar);
        this.add(MainMenu);
        MainMenu.setVisible(true);
        MainMusic.play();
        this.add(WorldMap);
        WorldMap.setVisible(false);
        this.add(PanelInventory);
        PanelInventory.setVisible(false);
        this.add(PanelQuestInventory);
        PanelQuestInventory.setVisible(false);
        this.add(PanelNPC);
        PanelNPC.setVisible(false);
        this.add(PanelQuest);
        PanelQuest.setVisible(false);
        this.add(IceMap);
        IceMap.setVisible(false);
        this.add(FireMap);
        FireMap.setVisible(false);
        this.add(BossMap);
        BossMap.setVisible(false);
        this.add(FireDungeon);
        FireDungeon.setVisible(false);
        this.add(IceDungeon);
        IceDungeon.setVisible(false);
        this.add(FirePuzzle);
        FirePuzzle.setVisible(false);
        this.add(IcePuzzle);
        IcePuzzle.setVisible(false);
        this.add(PanelBattle);
        PanelBattle.setVisible(false);
        this.add(BossChamber);
        BossChamber.setVisible(false);
        this.add(PanelVictory);
        PanelVictory.setVisible(false);
    }
    
    //Procedure
    public void Atas(){
        player.y -= 1;
    }
    
    public void Bawah(){
        player.y += 1;
    }
    
    public void Kiri(){
        player.x -= 1;
    }
    
    public void Kanan(){
        player.x += 1;
    }
    
    public void ExitInventory(){
        PanelNPC.setVisible(false);
        PanelInventory.setVisible(false);
        PanelQuestInventory.setVisible(false);
        PanelQuest.setVisible(false);
        
        if (visibleWorldMap == true) {
            WorldMap.setVisible(true);
            visibleWorldMap = false;
        }
        else if (visibleFireMap == true) {
            FireMap.setVisible(true);
            visibleFireMap = false;
        }
        else if (visibleIceMap == true) {
            IceMap.setVisible(true);
            visibleIceMap = false;
        }
        else if (visibleBossMap == true) {
            BossMap.setVisible(true);
            visibleBossMap = false;
        }
        else if (visiblePanelBattle == true) {
            PanelBattle.setVisible(true);
        }
        else if (visibleFireDungeon == true) {
            FireDungeon.setVisible(true);
            visibleFireDungeon = false;
        }
        else if (visibleIceDungeon == true) {
            IceDungeon.setVisible(true);
            visibleIceDungeon = false;
        }
        else if (visibleFirePuzzle == true) {
            FirePuzzle.setVisible(true);
            visibleFirePuzzle = false;
        }
        else if (visibleIcePuzzle == true) {
            IcePuzzle.setVisible(true);
            visibleIcePuzzle = false;
        }
    }
    
    public void ExitBattle(){
        if (visibleFireDungeon == true) {
            PanelBattle.setVisible(false);
            visiblePanelBattle = false;
            FireDungeon.setVisible(true);
            visibleFireDungeon = false;
            FireDungeon.timerDamageOverTime.start();
            DungeonMusic.play();
        }
        if (visibleIceDungeon == true) {
            PanelBattle.setVisible(false);
            visiblePanelBattle = false;
            IceDungeon.setVisible(true);
            visibleIceDungeon = false;
            DungeonMusic.play();
        }
        PanelBattle.setVisible(false);
    }
    
    public void goToQuestInventory(){
        PanelQuestInventory.getQuest();
        PanelInventory.setVisible(false);
        PanelQuestInventory.setVisible(true);
    }
    
    public void goToInventory(){
        PanelInventory.setVisible(true);
        PanelQuestInventory.setVisible(false);
    }
    
    public void EscapeRope() {
        PanelInventory.setVisible(false);
        PanelQuestInventory.setVisible(false);
        PanelNPC.setVisible(false);
        PanelQuest.setVisible(false);
        FireMap.setVisible(false);
        IceMap.setVisible(false);
        BossMap.setVisible(false);
        FireDungeon.setVisible(false);
        IceDungeon.setVisible(false);
        FirePuzzle.setVisible(false);
        IcePuzzle.setVisible(false);
        visibleWorldMap = false;
        visibleFireMap = false;
        visibleIceMap = false;
        visibleBossMap = false;
        visibleFireDungeon = false;
        visibleIceDungeon = false;
        visibleFirePuzzle = false;
        visibleIcePuzzle = false;
        FireDungeon.besar = false;
        FireDungeon.star = false;
        FireDungeon.CTRStar = 5;
        FireDungeon.fire = false;
        FireDungeon.CTRFire = 0;
        IceDungeon.besar = false;
        IceDungeon.star = false;
        IceDungeon.CTRStar = 5;
        IceDungeon.fire = false;
        IceDungeon.CTRFire = 0;
        player.x = 8;
        player.y = 9;
        FireDungeon.timerDamageOverTime.stop();
        FireMapMusic.stop();
        IceMapMusic.stop();
        BossMapMusic.stop();
        DungeonMusic.stop();
        WorldMapMusic.play();
        WorldMap.setXYLabelPlayer();
        WorldMap.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });
        getContentPane().setLayout(null);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        char temp = evt.getKeyChar();
        if(PanelVictory.isVisible()==true)
        {
            if(temp==' ')
            {
                PanelVictory.cetakfinalchat();
                
            }
        }
        if (MainMenu.isVisible() == true) {
            if (temp == 'w') {
                MainMenu.Atas();
                Select.stop();
                Select.play();
            }
            else if (temp == 's') {
                MainMenu.Bawah();
                Select.stop();
                Select.play();
            }
            else if (temp == ' ') {
                UTimer.start();
                
                if (MainMenu.getYSelect() == MainMenu.lebar/2-25) {
                    MainMenu.setVisible(false);
                    MainMusic.stop();
                    MainMenu.MainMenuThread.stop();
                    WorldMap.setVisible(true);
                    WorldMapMusic.play();
                    WorldMap.timerGreeting.start();
                    WorldMap.NPCThread.start();
                }
                else  {
                    try {
                        FileInputStream out = new FileInputStream("Save.txt");
                        ObjectInputStream tulis = new ObjectInputStream(out);
                        MainFrame.player = (Player) tulis.readObject();
                        tulis.close();
                        out.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    MainMenu.setVisible(false);
                    MainMusic.stop();
                    MainMenu.MainMenuThread.stop();
                    WorldMap.setVisible(true);
                    WorldMapMusic.play();
                    WorldMap.timerGreeting.start();
                    WorldMap.NPCThread.start();
                    WorldMap.setXYLabelPlayer();
                }
            }
        }
        else if (WorldMap.isVisible() == true) {
            if (temp == 'w') {
                if (WorldMap.WorldMapTerrain[player.y - 1][player.x] == " ") {
                    Atas();
                    WorldMap.Atas();
                }
            }
            else if (temp == 's') {
                if (WorldMap.WorldMapTerrain[player.y + 1][player.x] == " ") {
                    Bawah();
                    WorldMap.Bawah();
                }
            }
            else if (temp == 'a') {
                if (WorldMap.WorldMapTerrain[player.y][player.x - 1] == " ") {
                    Kiri();
                    WorldMap.Kiri();
                }
            }
            else if (temp == 'd') {
                if (WorldMap.WorldMapTerrain[player.y][player.x + 1] == " ") {
                    Kanan();
                    WorldMap.Kanan();
                }
            }
            else if (temp == ' ') {
                if (WorldMap.WorldMapTerrain[player.y][player.x + 1] == "N" || 
                        WorldMap.WorldMapTerrain[player.y][player.x - 1] == "N" || 
                        WorldMap.WorldMapTerrain[player.y + 1][player.x] == "N" ||
                        WorldMap.WorldMapTerrain[player.y - 1][player.x] == "N") {
                    PanelNPC.setData();
                    WorldMap.setVisible(false);
                    PanelNPC.setVisible(true);
                    visibleWorldMap = true;
                }
                else if (WorldMap.WorldMapTerrain[player.y ][player.x + 1] == "B" || 
                        WorldMap.WorldMapTerrain[player.y][player.x - 1] == "B" || 
                        WorldMap.WorldMapTerrain[player.y + 1][player.x] == "B" ||
                        WorldMap.WorldMapTerrain[player.y - 1][player.x] == "B") {
                    WorldMap.setVisible(false);
                    PanelQuest.setVisible(true);
                    visibleWorldMap = true;
                }
            }
            else if (temp == 'e') {
                WorldMap.setVisible(false);
                PanelInventory.setVisible(true);
                PanelInventory.setInventory();
                visibleWorldMap = true;
            }
            
            //Enter Fire Map
            if (player.y <= 0 && player.x > 0 && player.x < 4) {
                tempX = player.x;
                player.x = 8;
                player.y = 11;
                WorldMapMusic.stop();
                WorldMap.setVisible(false);
                FireMap.setVisible(true);
                FireMapMusic.play();
                FireMap.setXYLabelPlayer();
            }
            //Enter Ice Map
            else if (player.y <= 0 && player.x > 12 && player.x < 16) {
                tempX = player.x;
                player.x = 8;
                player.y = 11;
                WorldMapMusic.stop();
                WorldMap.setVisible(false);
                IceMap.setVisible(true);
                IceMapMusic.play();
                IceMap.setXYLabelPlayer();
            }
            //Enter Boss Map
            else if (player.y <= 0 && player.x > 5 && player.x < 12) {
                if (player.boolClearFireDungeon == true && player.boolClearIceDungeon == true) {
                    BossMap.setImageHole();
                }
                tempX = player.x;
                player.y = 11;
                WorldMapMusic.stop();
                WorldMap.setVisible(false);
                BossMap.setVisible(true);
                BossMapMusic.play();
                BossMap.setXYLabelPlayer();
            }
        }
        else if (FireMap.isVisible() == true) {
            if (temp == 'w') {
                if (FireMap.FireMapTerrain[player.y - 1][player.x] == " ") {
                    Atas();
                    FireMap.Atas();
                }
                else if (FireMap.FireMapTerrain[player.y - 1][player.x] == "E") {
                    player.x = 0;
                    player.y = 12;
                    FireMap.setVisible(false);
                    FireMapMusic.stop();
                    FireDungeon.setVisible(true);
                    FireDungeon.setXYLabelPlayer();
                }
            }
            else if (temp == 's') {
                if (FireMap.FireMapTerrain[player.y + 1][player.x] == " ") {
                    Bawah();
                    FireMap.Bawah();
                }
            }
            else if (temp == 'a') {
                if (FireMap.FireMapTerrain[player.y][player.x - 1] == " ") {
                    Kiri();
                    FireMap.Kiri();
                }
            }
            else if (temp == 'd') {
                if (FireMap.FireMapTerrain[player.y][player.x + 1] == " ") {
                    Kanan();
                    FireMap.Kanan();
                }
            }
            else if (temp == 'e') {
                FireMap.setVisible(false);
                PanelInventory.setVisible(true);
                PanelInventory.setInventory();
                visibleFireMap = true;
            }
            
            //Enter World Map
            if (player.y >= 12) {
                FireMapMusic.stop();
                FireMap.setVisible(false);
                WorldMap.setVisible(true);
                WorldMapMusic.play();
                player.y = 1;
                player.x = tempX;
                WorldMap.setXYLabelPlayer();
            }
            //Enter Fire Dungeon
            else if (FireMap.FireMapTerrain[player.y - 1][player.x] == "E") {
                IceDungeon.timerGerakMusuh.stop();
                IceDungeon.CTRGerakM = 0;
                IceDungeon.CTRArahM = 0;
                FireDungeon.timerGerakMusuh.stop();
                FireDungeon.CTRGerakM = 0;
                FireDungeon.CTRArahM = 0;
                player.x = 1;
                player.y = 12;
                DungeonMusic.play();
                FireMap.setVisible(false);
                FireMapMusic.stop();
                FireDungeon.setVisible(true);
                FireDungeon.xKanan = 8;
                FireDungeon.xKiri = FireDungeon.xKanan - 8;
                FireDungeon.xKananMax = 75;
                FireDungeon.xBackground = 0;
                FireDungeon.timerDamageOverTime.start();
                FireDungeon.timerSetLabelPlayerKanan.start();
                FireDungeon.setMusuh();
                FireDungeon.timerGerakMusuh.stop();
                FireDungeon.timerGerakMusuh.start();
                FireDungeon.setXYLabelPlayer();
                FireDungeon.resetXYLabelBackground();
                if (player.arrQuest[0] == true) {
                    player.uang += 100;
                    player.arrQuest[0] = false;
                    JOptionPane.showMessageDialog(null, "Quest Clear");
                    PanelQuest.resetFire();
                    if (player.arrQuest[0] == false && player.arrQuest[1] == false && player.arrQuest[2] == false) {
                        PanelQuest.resetQuest();
                    }
                    PanelQuestInventory.timerCountdownFire.stop();
                    PanelQuestInventory.resetFire();
                }
            }
        }
        else if (IceMap.isVisible() == true) {
            if (temp == 'w') {
                if (IceMap.IceMapTerrain[player.y - 1][player.x] == " ") {
                    Atas();
                    IceMap.Atas();
                }
            }
            else if (temp == 's') {
                if (IceMap.IceMapTerrain[player.y + 1][player.x] == " ") {
                    Bawah();
                    IceMap.Bawah();
                }
            }
            else if (temp == 'a') {
                if (IceMap.IceMapTerrain[player.y][player.x - 1] == " ") {
                    Kiri();
                    IceMap.Kiri();
                }
            }
            else if (temp == 'd') {
                if (IceMap.IceMapTerrain[player.y][player.x + 1] == " ") {
                    Kanan();
                    IceMap.Kanan();
                }
            }
            else if (temp == 'e') {
                IceMap.setVisible(false);
                PanelInventory.setVisible(true);
                PanelInventory.setInventory();
                visibleIceMap = true;
            }
            
            //Enter World Map
            if (player.y >= 12) {
                IceMapMusic.stop();
                IceMap.setVisible(false);
                WorldMap.setVisible(true);
                WorldMapMusic.play();
                player.y = 1;
                player.x = tempX;
                WorldMap.setXYLabelPlayer();
            }
            //Enter Ice Dungeon
            else if (IceMap.IceMapTerrain[player.y - 1][player.x] == "E") {
                IceDungeon.timerGerakMusuh.stop();
                IceDungeon.CTRGerakM = 0;
                IceDungeon.CTRArahM = 0;
                FireDungeon.timerGerakMusuh.stop();
                FireDungeon.CTRGerakM = 0;
                FireDungeon.CTRArahM = 0;
                player.x = 2;
                player.y = 12;
                DungeonMusic.play();
                IceMap.setVisible(false);
                IceMapMusic.stop();
                IceDungeon.setVisible(true);
                IceDungeon.xKanan = 8;
                IceDungeon.xKiri = IceDungeon.xKanan - 8;
                IceDungeon.xKananMax = 75;
                IceDungeon.xBackground = 0;
                IceDungeon.setXYLabelPlayer();
                IceDungeon.resetXYLabelBackground();
                IceDungeon.setMusuh();
                IceDungeon.timerGerakMusuh.start();
                if (player.arrQuest[1] == true) {
                    player.uang += 100;
                    player.arrQuest[1] = false;
                    JOptionPane.showMessageDialog(null, "Quest Clear");
                    PanelQuest.resetIce();
                    if (player.arrQuest[0] == false && player.arrQuest[1] == false && player.arrQuest[2] == false) {
                        PanelQuest.resetQuest();
                    }
                    PanelQuestInventory.timerCountdownIce.stop();
                    PanelQuestInventory.resetIce();
                }
            }
        }
        else if (BossMap.isVisible() == true) {
            if (temp == 'w') {
                if (BossMap.BossMapTerrain[player.y - 1][player.x] == " ") {
                    Atas();
                    BossMap.Atas();
                }
            }
            else if (temp == 's') {
                if (BossMap.BossMapTerrain[player.y + 1][player.x] == " ") {
                    Bawah();
                    BossMap.Bawah();
                }
            }
            else if (temp == 'a') {
                if (BossMap.BossMapTerrain[player.y][player.x - 1] == " ") {
                    Kiri();
                    BossMap.Kiri();
                }
            }
            else if (temp == 'd') {
                if (BossMap.BossMapTerrain[player.y][player.x + 1] == " ") {
                    Kanan();
                    BossMap.Kanan();
                }
            }
            else if (temp == 'e') {
                BossMap.setVisible(false);
                PanelInventory.setVisible(true);
                PanelInventory.setInventory();
                visibleBossMap = true;
            }
            
            //Enter World Map
            if (player.y >= 12) {
                BossMapMusic.stop();
                BossMap.setVisible(false);
                WorldMap.setVisible(true);
                WorldMapMusic.play();
                player.y = 1;
                player.x = tempX;
                WorldMap.setXYLabelPlayer();
            }
            //Enter Boss Chamber
            else if (player.boolClearFireDungeon == true && player.boolClearIceDungeon == true && BossMap.BossMapTerrain[player.y - 1][player.x] == "E") {
                if (player.arrQuest[2] == true) {
                    player.uang += 100;
                    player.arrQuest[2] = false;
                    JOptionPane.showMessageDialog(null, "Quest Clear");
                    PanelQuest.resetBoss();
                    if (player.arrQuest[0] == false && player.arrQuest[1] == false && player.arrQuest[2] == false) {
                        PanelQuest.resetQuest();
                    }
                    PanelQuestInventory.timerCountdownBoss.stop();
                    PanelQuestInventory.resetBoss();
                }
                BossMap.setVisible(false);
                BossMapMusic.stop();
                BossChamber.setVisible(true);
                PanelBattleMusic.play();
            }
        }
        else if(BossChamber.isVisible() == true){
            if(temp == ' ' && tekan < 4) {
                BossChamber.cetakchat();
                tekan++;
            }
            if(temp== 'w' && tekan>=4)
            {
                BossChamber.loncatatas();
                MarioJump.stop();
                MarioJump.play();
            }
            
            if(temp == ' '&& tekan>=4)
            {
                BossChamber.cetakchat();
            }
            if(temp == 'd' && tekan >= 4) {
                BossChamber.kanan();
            }
            if(temp == 'a' && tekan >= 4) {
                BossChamber.kiri();
            }
            if(temp == 'e' && tekan >= 4) {
                BossChamber.loncatkanan();
                MarioJump.stop();
                MarioJump.play();
            }
            if(temp == 'q' && tekan >= 4) {
                BossChamber.loncatkiri();
                MarioJump.stop();
                MarioJump.play();
            }
        }
        else if (FireDungeon.isVisible() == true) {
            if (FireDungeon.CTRGerakL == 0) {
                if (temp == 'w') {
                    FireDungeon.timerSetLabelPlayerKanan.start();
                    FireDungeon.timerSetLabelPlayerKiri.stop();
                    FireDungeon.LoncatKanan();
                    MarioJump.stop();
                    MarioJump.play();
                    kiri = false;
                    kanan = true;
                }
                else if (temp == 'q') {
                    FireDungeon.timerSetLabelPlayerKanan.stop();
                    FireDungeon.timerSetLabelPlayerKiri.start();
                    FireDungeon.LoncatKiri();
                    MarioJump.stop();
                    MarioJump.play();
                    kiri = true;
                    kanan = false;
                }
                else if (temp == 'a') {
                    FireDungeon.timerSetLabelPlayerKanan.stop();
                    FireDungeon.timerSetLabelPlayerKiri.start();
                    kiri = true;
                    kanan = false;
                    if (player.x > FireDungeon.xKiri && FireDungeon.FireDungeonTerrain[player.y][player.x - 1] == " ") {
                        Kiri();
                        FireDungeon.Kiri();
                    }
                }
                else if (temp == 'd') {
                    FireDungeon.timerSetLabelPlayerKanan.start();
                    FireDungeon.timerSetLabelPlayerKiri.stop();
                    kiri = false;
                    kanan = true;
                    if (player.x > FireDungeon.xKanan && player.x < FireDungeon.xKananMax - 8 && FireDungeon.FireDungeonTerrain[player.y][player.x + 1] == " ") {
                        FireDungeon.setXYLabelBackground();
                        Kanan();
                        FireDungeon.Iddle();
                        FireDungeon.setXYLabelMusuh();
                    }
                    else if (player.x < FireDungeon.xKananMax - 1 && player.x < FireDungeon.xKananMax && FireDungeon.FireDungeonTerrain[player.y][player.x + 1] == " ") {
                        Kanan();
                        FireDungeon.Kanan();
                    }
                }
                else if (temp == 'e') {
                    FireDungeon.setVisible(false);
                    PanelInventory.setVisible(true);
                    PanelInventory.setInventory();
                    visibleFireDungeon = true;
                }
                else if (temp == ' ') {
                    if(FireDungeon.fire == true) {
                        FireDungeon.tembak();
                    }
                }
                
                //Enter Fire Puzzle
                if (FireDungeon.FireDungeonTerrain[player.y][player.x + 1] == "E") {
                    FireDungeon.setVisible(false);
                    FirePuzzle.setVisible(true);
                    player.x = 8;
                    player.y = 12;
                    FirePuzzle.setXYLabelPlayer();
                    FireDungeon.timerDamageOverTime.stop();
                }
            }
        }
        else if (IceDungeon.isVisible() == true) {
            if (IceDungeon.CTRGerakL == 0) {
                if (temp == 'w') {
                    IceDungeon.timerSetLabelPlayerKanan.start();
                    IceDungeon.timerSetLabelPlayerKiri.stop();
                    IceDungeon.LoncatKanan();
                    MarioJump.stop();
                    MarioJump.play();
                    kiri = false;
                    kanan = true;
                }
                else if (temp == 'q') {
                    IceDungeon.timerSetLabelPlayerKanan.stop();
                    IceDungeon.timerSetLabelPlayerKiri.start();
                    IceDungeon.LoncatKiri();
                    MarioJump.stop();
                    MarioJump.play();
                    kiri = true;
                    kanan = false;
                }
                else if (temp == 'a') {
                    IceDungeon.timerSetLabelPlayerKanan.stop();
                    IceDungeon.timerSetLabelPlayerKiri.start();
                    kiri = true;
                    kanan = false;
                    if (player.x > IceDungeon.xKiri && IceDungeon.IceDungeonTerrain[player.y][player.x - 1] == " ") {
                        Kiri();
                        IceDungeon.Kiri();
                    }
                    else if (IceDungeon.IceDungeonTerrain[player.y][player.x - 1] == "D") {
                        IceDungeon.SlideKiri();
                    }
                }
                else if (temp == 'd') {
                    IceDungeon.timerSetLabelPlayerKanan.start();
                    IceDungeon.timerSetLabelPlayerKiri.stop();
                    kiri = false;
                    kanan = true;
                    if (player.x > IceDungeon.xKanan && player.x < IceDungeon.xKananMax - 8 && IceDungeon.IceDungeonTerrain[player.y][player.x + 1] == " ") {
                        IceDungeon.setXYLabelBackground();
                        Kanan();
                        IceDungeon.Iddle();
                        IceDungeon.setXYLabelMusuh();
                    }
                    else if (player.x < IceDungeon.xKananMax - 1 && player.x < IceDungeon.xKananMax && IceDungeon.IceDungeonTerrain[player.y][player.x + 1] == " ") {
                        Kanan();
                        IceDungeon.Kanan();
                    }
                    else if (IceDungeon.IceDungeonTerrain[player.y][player.x + 1] == "D") {
                        IceDungeon.SlideKanan();
                    }
                }
                else if (temp == 'e') {
                    IceDungeon.setVisible(false);
                    PanelInventory.setVisible(true);
                    PanelInventory.setInventory();
                    visibleIceDungeon = true;
                }
                else if (temp == ' ') {
                    if(IceDungeon.fire == true) {
                        IceDungeon.tembak();
                    }
                }
                
                //Enter Fire Puzzle
                if (IceDungeon.IceDungeonTerrain[player.y][player.x + 1] == "E") {
                    IceDungeon.setVisible(false);
                    IcePuzzle.setVisible(true);
                    player.x = 8;
                    player.y = 12;
                    IcePuzzle.setXYLabelPlayer();
                }
            }
        }
        else if (FirePuzzle.isVisible() == true) {
            if (FirePuzzle.CTRGerakL == 0) {
                if (temp == 'w') {
                    if (player.y > 0 && FirePuzzle.FirePuzzleTerrain[player.y - 1][player.x] != "0") {
                        Atas();
                        FirePuzzle.Atas();
                    }
                }
                else if (temp == 's') {
                    if (player.y < 12 && FirePuzzle.FirePuzzleTerrain[player.y + 1][player.x] != "0") {
                        Bawah();
                        FirePuzzle.Bawah();
                    }
                }
                else if (temp == 'a') {
                    if (player.x > 0 && FirePuzzle.FirePuzzleTerrain[player.y][player.x - 1] != "0") {
                        Kiri();
                        FirePuzzle.Kiri();
                    }
                }
                else if (temp == 'd') {
                    if (player.x < 16 && FirePuzzle.FirePuzzleTerrain[player.y][player.x + 1] != "0") {
                        Kanan();
                        FirePuzzle.Kanan();
                    }
                }
                else if (temp == 'z') {
                    FirePuzzle.LoncatKiri();
                }
                else if (temp == 'c') {
                    FirePuzzle.LoncatKanan();
                }
                else if (temp == 'e') {
                    FirePuzzle.setVisible(false);
                    PanelInventory.setVisible(true);
                    PanelInventory.setInventory();
                    visibleFirePuzzle = true;
                }
            }
        }
        else if (IcePuzzle.isVisible() == true) {
            if (temp == 'w') {
                if (IcePuzzle.IcePuzzleTerrain[player.y - 1][player.x] != "0" ) {
                    IcePuzzle.Atas();
                }
            }
            else if (temp == 's') {
                if (IcePuzzle.IcePuzzleTerrain[player.y + 1][player.x] != "0") {
                    IcePuzzle.Bawah();
                }
            }
            else if (temp == 'a') {
                if (IcePuzzle.IcePuzzleTerrain[player.y][player.x - 1] != "0") {
                    IcePuzzle.Kiri();
                }
            }
            else if (temp == 'd') {
                if (IcePuzzle.IcePuzzleTerrain[player.y][player.x + 1] != "0") {
                    IcePuzzle.Kanan();
                }
            }
            else if (temp == 'e') {
                IcePuzzle.setVisible(false);
                PanelInventory.setVisible(true);
                PanelInventory.setInventory();
                visibleIcePuzzle = true;
            }
        }
    }//GEN-LAST:event_formKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}