/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oiram;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author Hendardi
 */
public class FireDungeon extends javax.swing.JPanel {

    /**
     * Creates new form FireDungeon
     */
    String FireDungeonTerrain[][] = {
            {"0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0"},
            {"0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0"},
            {"0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"},
            {"0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"},
            {"0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "E"},
            {"0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "E"},
            {"0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0", "0"},
            {"0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0", "0", "P", "0", "0", "0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", " ", " ", " ", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"},
            {"0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "P", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", "0", " ", " ", " ", " ", " ", " ", " ", " ", "0", "0", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0"},
            {"0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0", " ", " ", " ", " ", " ", " ", " ", "0", "0", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0"},
            {"0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0", "0", "0", "0", "0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0", "0", "0", "0", " ", " ", " ", " ", "0", "0", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0"},
            {"0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0", "0", "0", "0", "0", "0", " ", " ", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", " ", " ", " ", " ", " ", " ", " ", " ", "0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0", "0", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0"},
            {" ", " ", " ", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", " ", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", " ", " ", " ", " ", " ", " ", " ", "0", "0", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0", "0", "0", " ", " ", " ", " ", " ", " ", " ", "0", "0", "0"},
            {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"},
            {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"},
    };
    
    int panjang = 3750;
    int lebar = 750;
    int xKanan = 8;
    int xKiri = xKanan - 8;
    int xKananMax = 75;
    int xBackground = 0;
    int CTRGerak = 0;
    int CTRGerakL = 0;
    int CTRGerakM = 0;
    int CTRArahM = 0;
    int randomPowerUp = 0;
    int CTRStar = 0;
    int CTRFire = 10;
    int CTRShowFire = 1;
    int xFire;
    int yFire;
    boolean gerak = true;
    boolean gerakJatuh = true;
    boolean besar = false;
    boolean star = false;
    boolean fire = false;
    boolean ShowFire = false;
    
    Timer timerStar = new Timer(1000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            CTRStar++;
        }
    });
    
    Timer timerFire = new Timer(1000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            CTRFire -= 1;
        }
    });
    
    Timer timerShowFire = new Timer(250, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            CTRShowFire -= 1;
        }
    });
    
    Timer timerDamageOverTime = new Timer(1000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (MainFrame.player.coldDrink == false) {
                MainFrame.player.hp -= 1;
                ImageIcon rawImg = new ImageIcon("Image/Transparent.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
        }
    });
    
    Timer timerSetLabelPlayerKiri = new Timer (500, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (CTRGerakL == 0) {
                if (besar == false) {
                    ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokiri0.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelPlayer.setIcon(finalimg);
                }
                if (besar == true) {
                    ImageIcon rawImg = new ImageIcon("Image/Mario/bigmariokiri0.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelPlayer.setIcon(finalimg);
                }
                if (star == true) {
                    ImageIcon rawImg = new ImageIcon("Image/Mario/starmariokiri"+CTRStar+".png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelPlayer.setIcon(finalimg);
                }
                if (fire == true) {
                    ImageIcon rawImg = new ImageIcon("Image/Mario/firemariokiri.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelPlayer.setIcon(finalimg);
                }
            }
        }
    });
    
    Timer timerSetLabelPlayerKanan = new Timer (500, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (CTRGerakL == 0) {
                if (besar == false) {
                    ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan0.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelPlayer.setIcon(finalimg);
                }
                if (besar == true) {
                    ImageIcon rawImg = new ImageIcon("Image/Mario/bigmariokanan0.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelPlayer.setIcon(finalimg);
                }
                if (star == true) {
                    ImageIcon rawImg = new ImageIcon("Image/Mario/starmariokanan"+CTRStar+".png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelPlayer.setIcon(finalimg);
                }
                if (fire == true) {
                    ImageIcon rawImg = new ImageIcon("Image/Mario/firemariokanan.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelPlayer.setIcon(finalimg);
                }
            }
        }
    });
    
    Timer timerGerakPlayerKiri = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX() - 10, LabelPlayer.getY());
            
            if (besar == false) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokiri"+CTRGerak+".png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (besar == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/bigmariokiri"+CTRGerak+".png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (star == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/starmariokiri"+CTRGerak+".png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (fire == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/firemariokiri.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerKanan = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX() + 10, LabelPlayer.getY());
            
            if (besar == false) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan"+CTRGerak+".png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (besar == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/bigmariokanan"+CTRGerak+".png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (star == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/starmariokanan"+CTRGerak+".png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (fire == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/firemariokanan.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerIddle = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY());
            
            if (besar == false) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan"+CTRGerak+".png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (besar == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/bigmariokanan"+CTRGerak+".png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            
            if (star == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/starmariokanan"+CTRGerak+".png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (fire == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/firemariokanan.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            CTRGerak++;
        }
    });
    
    Timer timerGerakPlayerLoncatKiri = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (besar == false) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokiri3.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (besar == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/bigmariokiri3.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (star == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/starmariokiri"+CTRGerak+".png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (fire == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/firemariokiri.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            
            if (CTRGerakL <= 5) {
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() - 10);
                if (CTRGerakL == 5 && FireDungeonTerrain[MainFrame.player.y - 1][MainFrame.player.x] == " ") {
                    MainFrame.player.y -= 1;
                }
                else if (FireDungeonTerrain[MainFrame.player.y - 1][MainFrame.player.x] == "P") {
                    gerak = false;
                    MainFrame.player = new Player(MainFrame.player);
                    randomPowerUp = (int)(Math.random() * 3);
                    
                    if (randomPowerUp == 0) {
                        MainFrame.player = new PlayerBesar(MainFrame.player);
                        besar = true;
                        star = false;
                        CTRStar = 5;
                        fire = false;
                        CTRFire = 0;
                    }
                    else if (randomPowerUp == 1) {
                        MainFrame.player = new PlayerStar(MainFrame.player);
                        besar = false;
                        star = true;
                        CTRStar = 0;
                        fire = false;
                        CTRFire = 0;
                        timerStar.start();
                    }
                    else if (randomPowerUp == 2) {
                        MainFrame.player = new PlayerFire(MainFrame.player);
                        besar = false;
                        star = false;
                        CTRStar = 0;
                        fire = true;
                        CTRFire = 10;
                        timerFire.start();
                    }
                }
                else if (FireDungeonTerrain[MainFrame.player.y - 1][MainFrame.player.x] == "0") {
                    gerak = false;
                }
            }
            else if (CTRGerakL <= 10 && gerak == true) {
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() - 10);
                if (CTRGerakL == 10 && FireDungeonTerrain[MainFrame.player.y - 1][MainFrame.player.x] == " ") {
                    MainFrame.player.y -= 1;
                }
                else if (FireDungeonTerrain[MainFrame.player.y - 1][MainFrame.player.x] == "P") {
                    gerak = false;
                    randomPowerUp = (int)(Math.random() * 3);
                    
                    if (randomPowerUp == 0) {
                        MainFrame.player = new PlayerBesar(MainFrame.player);
                        besar = true;
                        star = false;
                        CTRStar = 5;
                        fire = false;
                        CTRFire = 0;
                    }
                    else if (randomPowerUp == 1) {
                        MainFrame.player = new PlayerStar(MainFrame.player);
                        besar = false;
                        star = true;
                        CTRStar = 0;
                        fire = false;
                        CTRFire = 0;
                        timerStar.start();
                    }
                    else if (randomPowerUp == 2) {
                        MainFrame.player = new PlayerFire(MainFrame.player);
                        besar = false;
                        star = false;
                        CTRStar = 0;
                        fire = true;
                        CTRFire = 10;
                        timerFire.start();
                    }
                }
                else if (FireDungeonTerrain[MainFrame.player.y - 1][MainFrame.player.x] == "0") {
                    gerak = false;
                }
            }
            else if (CTRGerakL <= 15 && gerak == true) {
                LabelPlayer.setLocation(LabelPlayer.getX() - 10, LabelPlayer.getY());
                if (CTRGerakL == 15 && FireDungeonTerrain[MainFrame.player.y][MainFrame.player.x - 1] == " ") {
                    MainFrame.player.x -= 1;
                }
                else if (FireDungeonTerrain[MainFrame.player.y][MainFrame.player.x - 1] == "0") {
                    gerak = false;
                }
            }
            else if (CTRGerakL <= 20 && gerak == true) {
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() + 10);
                if (CTRGerakL == 20 && FireDungeonTerrain[MainFrame.player.y + 1][MainFrame.player.x] == " ") {
                    MainFrame.player.y += 1;
                    if (MainFrame.player.x > xKanan && MainFrame.player.x < xKananMax - 8) {
                        xKanan += 1;
                        xKiri = xKanan - 8;
                        xBackground -= 1;
                        LabelBackground.setLocation(xBackground * 50, 0);
                    }
                }
                else if (FireDungeonTerrain[MainFrame.player.y + 1][MainFrame.player.x] == "0") {
                    gerak = false;
                }
            }
            else if (CTRGerakL <= 25 && gerak == true) {
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() + 10);
                if (CTRGerakL == 25 && FireDungeonTerrain[MainFrame.player.y + 1][MainFrame.player.x] == " ") {
                    MainFrame.player.y += 1;
                }
                else if (FireDungeonTerrain[MainFrame.player.y + 1][MainFrame.player.x] == "0") {
                    gerak = false;
                }
            }
            CTRGerakL++;
        }
    });
    
    Timer timerGerakPlayerLoncatKanan = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (besar == false) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan3.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (besar == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/bigmariokanan3.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (star == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/starmariokanan"+CTRGerak+".png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            if (fire == true) {
                ImageIcon rawImg = new ImageIcon("Image/Mario/firemariokanan.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelPlayer.setIcon(finalimg);
            }
            
            if (CTRGerakL <= 5) {
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() - 10);
                if (CTRGerakL == 5 && FireDungeonTerrain[MainFrame.player.y - 1][MainFrame.player.x] == " ") {
                    MainFrame.player.y -= 1;
                }
                else if (FireDungeonTerrain[MainFrame.player.y - 1][MainFrame.player.x] == "P") {
                    gerak = false;
                    randomPowerUp = (int)(Math.random() * 3);
                    
                    if (randomPowerUp == 0) {
                        MainFrame.player = new PlayerBesar(MainFrame.player);
                        besar = true;
                        star = false;
                        CTRStar = 5;
                        fire = false;
                        CTRFire = 0;
                    }
                    else if (randomPowerUp == 1) {
                        MainFrame.player = new PlayerStar(MainFrame.player);
                        besar = false;
                        star = true;
                        CTRStar = 0;
                        fire = false;
                        CTRFire = 0;
                        timerStar.start();
                    }
                    else if (randomPowerUp == 2) {
                        MainFrame.player = new PlayerFire(MainFrame.player);
                        besar = false;
                        star = false;
                        CTRStar = 0;
                        fire = true;
                        CTRFire = 10;
                        timerFire.start();
                    }
                }
                else if (FireDungeonTerrain[MainFrame.player.y - 1][MainFrame.player.x] == "0") {
                    gerak = false;
                }
            }
            else if (CTRGerakL <= 10 && gerak == true) {
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() - 10);
                if (CTRGerakL == 10 && FireDungeonTerrain[MainFrame.player.y - 1][MainFrame.player.x] == " ") {
                    MainFrame.player.y -= 1;
                }
                else if (FireDungeonTerrain[MainFrame.player.y - 1][MainFrame.player.x] == "P") {
                    gerak = false;
                    randomPowerUp = (int)(Math.random() * 3);
                    
                    if (randomPowerUp == 0) {
                        MainFrame.player = new PlayerBesar(MainFrame.player);
                        besar = true;
                        star = false;
                        CTRStar = 5;
                        fire = false;
                        CTRFire = 0;
                    }
                    else if (randomPowerUp == 1) {
                        MainFrame.player = new PlayerStar(MainFrame.player);
                        besar = false;
                        star = true;
                        CTRStar = 0;
                        fire = false;
                        CTRFire = 0;
                        timerStar.start();
                    }
                    else if (randomPowerUp == 2) {
                        MainFrame.player = new PlayerFire(MainFrame.player);
                        besar = false;
                        star = false;
                        CTRStar = 0;
                        fire = true;
                        CTRFire = 10;
                        timerFire.start();
                    }
                }
                else if (FireDungeonTerrain[MainFrame.player.y - 1][MainFrame.player.x] == "0") {
                    gerak = false;
                }
            }
            else if (CTRGerakL <= 15 && gerak == true) {
                if (FireDungeonTerrain[MainFrame.player.y][MainFrame.player.x + 1] == "0") {
                    gerak = false;
                }
                else if (MainFrame.player.x > xKanan && MainFrame.player.x <= xKananMax - 8) {
                    LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY());
                    if (CTRGerakL == 15 && FireDungeonTerrain[MainFrame.player.y][MainFrame.player.x + 1] == " ") {
                        MainFrame.player.x += 1;
                        xKanan += 1;
                        xKiri = xKanan - 8;
                        xBackground -= 1;
                        LabelBackground.setLocation(xBackground * 50, 0);
                    }
                }
                else {
                    LabelPlayer.setLocation(LabelPlayer.getX() + 10, LabelPlayer.getY());
                    if (CTRGerakL == 15 && FireDungeonTerrain[MainFrame.player.y][MainFrame.player.x + 1] == " ") {
                        MainFrame.player.x += 1;
                    }
                }
            }
            else if (CTRGerakL <= 20 && gerak == true) {
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() + 10);
                if (CTRGerakL == 20 && FireDungeonTerrain[MainFrame.player.y + 1][MainFrame.player.x] == " ") {
                    MainFrame.player.y += 1;
                }
                else if (FireDungeonTerrain[MainFrame.player.y + 1][MainFrame.player.x] == "0") {
                    gerak = false;
                }
            }
            else if (CTRGerakL <= 25 && gerak == true) {
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() + 10);
                if (CTRGerakL == 25 && FireDungeonTerrain[MainFrame.player.y + 1][MainFrame.player.x] == " ") {
                    MainFrame.player.y += 1;
                }
                else if (FireDungeonTerrain[MainFrame.player.y + 1][MainFrame.player.x] == "0") {
                    gerak = false;
                }
            }
            CTRGerakL++;
        }
    });
    
    Timer timerGerakPlayerJatuh = new Timer(25, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (FireDungeonTerrain[MainFrame.player.y + 1][MainFrame.player.x] == " ") {
                MainFrame.player.y += 1;
                LabelPlayer.setLocation(LabelPlayer.getX(), LabelPlayer.getY() + 50);
                
                if (besar == false) {
                    ImageIcon rawImg = new ImageIcon("Image/Mario/dmariokanan0.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelPlayer.setIcon(finalimg);
                }
                if (besar == true) {
                    ImageIcon rawImg = new ImageIcon("Image/Mario/bigmariokanan0.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelPlayer.setIcon(finalimg);
                }
                if (star == true) {
                    ImageIcon rawImg = new ImageIcon("Image/Mario/starmariokanan"+CTRGerak+".png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelPlayer.setIcon(finalimg);
                }
                if (fire == true) {
                    ImageIcon rawImg = new ImageIcon("Image/Mario/firemariokanan.png");
                    Image img = rawImg.getImage();
                    Image newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon finalimg = new ImageIcon(newImg);
                    LabelPlayer.setIcon(finalimg);
                }
            }
            else if (FireDungeonTerrain[MainFrame.player.y + 1][MainFrame.player.x] == "0") {
                gerakJatuh = false;
                LabelPlayer.setLocation((MainFrame.player.x + xBackground) * 50, MainFrame.player.y  * 50);
            }
        }
    });
    
    Timer timerGerakMusuh = new Timer(100, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (MainFrame.m1.hp > 0) {
                if (FireDungeonTerrain[MainFrame.m1.y][MainFrame.m1.x - 1] == " ") {
                    if (MainFrame.m1.dx == 1) {
                        LabelMusuh1.setLocation(LabelMusuh1.getX() - 10, LabelMusuh1.getY());
                    }
                    else if (MainFrame.m1.dx == -1) {
                        LabelMusuh1.setLocation(LabelMusuh1.getX() + 10, LabelMusuh1.getY());
                    }
                }
            }
            else {
                ImageIcon rawImg = new ImageIcon("Image/Transparent.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelMusuh1.getWidth(), LabelMusuh1.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelMusuh1.setIcon(finalimg); 
            }
            if (MainFrame.m2.hp > 0) {
                if (FireDungeonTerrain[MainFrame.m2.y][MainFrame.m2.x - 1] == " ") {
                    if (MainFrame.m2.dx == 1) {
                        LabelMusuh2.setLocation(LabelMusuh2.getX() - 10, LabelMusuh2.getY());
                    }
                    else if (MainFrame.m2.dx == -1) {
                        LabelMusuh2.setLocation(LabelMusuh2.getX() + 10, LabelMusuh2.getY());
                    }
                }
            }
            else {
                ImageIcon rawImg = new ImageIcon("Image/Transparent.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelMusuh1.getWidth(), LabelMusuh1.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelMusuh2.setIcon(finalimg); 
            }
            if (MainFrame.m3.hp > 0) {
                if (FireDungeonTerrain[MainFrame.m3.y][MainFrame.m3.x - 1] == " ") {
                    if (MainFrame.m3.dx == 1) {
                        LabelMusuh3.setLocation(LabelMusuh3.getX() - 10, LabelMusuh3.getY());
                    }
                    else if (MainFrame.m3.dx == -1) {
                        LabelMusuh3.setLocation(LabelMusuh3.getX() + 10, LabelMusuh3.getY());
                    }
                }
            }
            else {
                ImageIcon rawImg = new ImageIcon("Image/Transparent.png");
                Image img = rawImg.getImage();
                Image newImg = img.getScaledInstance(LabelMusuh1.getWidth(), LabelMusuh1.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon finalimg = new ImageIcon(newImg);
                LabelMusuh3.setIcon(finalimg); 
            }
            CTRGerakM++;
            CTRArahM++;
            
            if (ShowFire == true) {
                cek_tertembak(MainFrame.m1.x, MainFrame.m1.y, MainFrame.m1);
                cek_tertembak(MainFrame.m2.x, MainFrame.m2.y, MainFrame.m2);
                cek_tertembak(MainFrame.m3.x, MainFrame.m3.y, MainFrame.m3);
            }
            if (CTRGerakM == 5) {
                if (FireDungeonTerrain[MainFrame.m1.y][MainFrame.m1.x - 1] == " ") {
                    MainFrame.m1.x -= MainFrame.m1.dx;
                }
                if (FireDungeonTerrain[MainFrame.m2.y][MainFrame.m2.x - 1] == " ") {
                    MainFrame.m2.x -= MainFrame.m2.dx;
                }
                if (FireDungeonTerrain[MainFrame.m3.y][MainFrame.m3.x - 1] == " ") {
                    MainFrame.m3.x -= MainFrame.m3.dx;
                }
                setXYLabelMusuh();
                CTRGerakM = 0;
            }
            if (CTRArahM == 20) {
                MainFrame.m1.dx *= -1;
                MainFrame.m2.dx *= -1;
                MainFrame.m3.dx *= -1;
                CTRArahM = 0;
            }
        }
    });
    
    public FireDungeon() {
        initComponents();
        this.setSize(panjang, lebar);
        
        //LabelBackground
        LabelBackground.setSize(panjang,lebar);
        LabelBackground.setLocation(0, 0);
        ImageIcon rawImg = new ImageIcon("Image/Map/Fire Dungeon.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
        
        //LabelPlayer
        LabelPlayer.setSize(50, 50);
        rawImg = new ImageIcon("Image/Mario/dmariokanan0.png");
        img = rawImg.getImage();
        newImg = img.getScaledInstance(LabelPlayer.getWidth(), LabelPlayer.getHeight(), Image.SCALE_SMOOTH);
        finalimg = new ImageIcon(newImg);
        LabelPlayer.setIcon(finalimg);
        
        //LabelFire
        LabelFire.setText("");
    }
    
    public void setXYLabelPlayer(){
        LabelPlayer.setLocation((MainFrame.player.x + xBackground) * 50, MainFrame.player.y  * 50);
    }
    
    public void LoncatKiri(){
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        timerGerakPlayerIddle.stop();
        CTRGerak = 0;
        timerGerakPlayerLoncatKiri.start();
    }
    
    public void LoncatKanan(){
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        timerGerakPlayerIddle.stop();
        CTRGerak = 0;
        timerGerakPlayerLoncatKanan.start();
    }
    
    public void Kiri(){
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        timerGerakPlayerIddle.stop();
        CTRGerak = 0;
        timerGerakPlayerKiri.start();
    }
    
    public void Kanan(){
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        timerGerakPlayerIddle.stop();
        CTRGerak = 0;
        timerGerakPlayerKanan.start();
    }
    
    public void Iddle(){
        timerGerakPlayerKiri.stop();
        timerGerakPlayerKanan.stop();
        timerGerakPlayerIddle.stop();
        CTRGerak = 0;
        timerGerakPlayerIddle.start();
    }
    
    public void setXYLabelBackground(){
        xKanan += 1;
        xKiri = xKanan - 8;
        xBackground -= 1;
        LabelBackground.setLocation(xBackground * 50, 0);
    }
    
    public void resetXYLabelBackground(){
        LabelBackground.setLocation(0, 0);
    }
    
    public void setHariSiang(){
        ImageIcon rawImg = new ImageIcon("Image/Map/Fire Dungeon.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
    }
    
    public void setHariMalam(){
        ImageIcon rawImg = new ImageIcon("Image/Map/Fire Dungeon Dark.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelBackground.getWidth(), LabelBackground.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelBackground.setIcon(finalimg);
    }
    
    public void setMusuh(){
        int random = (int)(Math.random() * 2);
        if (random == 0) {
            MainFrame.m1 = new Gumba(11, 11);
            LabelMusuh1.setLocation( (MainFrame.m1.x + xBackground) * 50, MainFrame.m1.y * 50 );
            LabelMusuh1.setSize(50, 50);
            ImageIcon rawImg = new ImageIcon("Image/Unit/gumba0.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelMusuh1.getWidth(), LabelMusuh1.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelMusuh1.setIcon(finalimg);
        }
        else {
            MainFrame.m1 = new Kuppa(11, 11);
            LabelMusuh1.setLocation( (MainFrame.m1.x + xBackground) * 50, MainFrame.m1.y * 50 );
            LabelMusuh1.setSize(50, 50);
            ImageIcon rawImg = new ImageIcon("Image/Unit/kuppakiri0.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelMusuh1.getWidth(), LabelMusuh1.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelMusuh1.setIcon(finalimg);    
        }
        
        random = (int)(Math.random() * 2);
        if(random == 0)
        {
            MainFrame.m2 = new Gumba(35, 10);
            LabelMusuh2.setLocation( (MainFrame.m2.x + xBackground) * 50, MainFrame.m2.y * 50 );
            LabelMusuh2.setSize(50, 50);
            ImageIcon rawImg = new ImageIcon("Image/Unit/gumba0.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelMusuh2.getWidth(), LabelMusuh2.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelMusuh2.setIcon(finalimg);
        }
        else if(random == 1)
        {
            MainFrame.m2 = new Kuppa(35, 10);
            LabelMusuh2.setLocation( (MainFrame.m2.x + xBackground) * 50, MainFrame.m2.y * 50 );
            LabelMusuh2.setSize(50, 50);
            ImageIcon rawImg = new ImageIcon("Image/Unit/kuppakiri0.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelMusuh2.getWidth(), LabelMusuh2.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelMusuh2.setIcon(finalimg);
        }
        
        random = (int)(Math.random() * 2);
        if(random == 0)
        {
            MainFrame.m3 = new Gumba(45, 12);
            LabelMusuh3.setLocation( (MainFrame.m3.x + xBackground) * 50, MainFrame.m3.y * 50 );
            LabelMusuh3.setSize(50, 50);
            ImageIcon rawImg = new ImageIcon("Image/Unit/gumba0.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelMusuh3.getWidth(), LabelMusuh3.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelMusuh3.setIcon(finalimg);
        }
        else if(random == 1)
        {
            MainFrame.m3 = new Kuppa(45, 12);
            LabelMusuh3.setLocation( (MainFrame.m3.x + xBackground) * 50, MainFrame.m3.y * 50 );
            LabelMusuh3.setSize(50, 50);
            ImageIcon rawImg = new ImageIcon("Image/Unit/kuppakiri0.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelMusuh3.getWidth(), LabelMusuh3.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelMusuh3.setIcon(finalimg);
        }
    }
    
    public void setKuppaShell(int i){
        if (i == 1) {
            ImageIcon rawImg = new ImageIcon("Image/Unit/kuppa.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelMusuh1.getWidth(), LabelMusuh1.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelMusuh1.setIcon(finalimg);
        }
        else if (i == 2) {
            ImageIcon rawImg = new ImageIcon("Image/Unit/kuppa.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelMusuh2.getWidth(), LabelMusuh2.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelMusuh2.setIcon(finalimg);
        }
        else if (i == 3) {
            ImageIcon rawImg = new ImageIcon("Image/Unit/kuppa.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelMusuh3.getWidth(), LabelMusuh3.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelMusuh3.setIcon(finalimg);
        }
    }
    
    public void setXYLabelMusuh(){
        LabelMusuh1.setLocation((MainFrame.m1.x + xBackground) * 50, MainFrame.m1.y  * 50);
        LabelMusuh2.setLocation((MainFrame.m2.x + xBackground) * 50, MainFrame.m2.y  * 50);
        LabelMusuh3.setLocation((MainFrame.m3.x + xBackground) * 50, MainFrame.m3.y  * 50);
    }
    
    public void tembak() {
        if (MainFrame.kanan == true) {
            ShowFire = true;
            timerShowFire.start();
            xFire = MainFrame.player.x + 1;
            yFire = MainFrame.player.y;
            LabelFire.setLocation((MainFrame.player.x + 1 + xBackground) * 50, MainFrame.player.y * 50);
            LabelFire.setSize(150, 50);
            ImageIcon rawImg = new ImageIcon("Image/Fire-Right.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelFire.getWidth(), LabelFire.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelFire.setIcon(finalimg);
        }
        else{
            ShowFire = true;
            timerShowFire.start();
            xFire = MainFrame.player.x - 1;
            yFire = MainFrame.player.y;
            LabelFire.setLocation((MainFrame.player.x - 3 + xBackground) * 50, MainFrame.player.y * 50);
            LabelFire.setSize(150, 50);
            ImageIcon rawImg = new ImageIcon("Image/Fire-Left.png");
            Image img = rawImg.getImage();
            Image newImg = img.getScaledInstance(LabelFire.getWidth(), LabelFire.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon finalimg = new ImageIcon(newImg);
            LabelFire.setIcon(finalimg);
        }
    }
    
    public void stop_tembak() {
        ImageIcon rawImg = new ImageIcon("Image/Transparent.png");
        Image img = rawImg.getImage();
        Image newImg = img.getScaledInstance(LabelFire.getWidth(), LabelFire.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon finalimg = new ImageIcon(newImg);
        LabelFire.setIcon(finalimg);
    }

    public void cek_tertembak(int x, int y, Musuh m) {
        if (MainFrame.kanan == true) {
            for (int i = 0; i < 3; i++) {
                if (x <= xFire + i && y == yFire) {
                    m.hp = 0;
                }
            }
        }
        else {
            for (int i = 0; i < 3; i++) {
                if (x >= xFire - i && y == yFire) {
                    m.hp = 0;
                }
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LabelPlayer = new javax.swing.JLabel();
        LabelFire = new javax.swing.JLabel();
        LabelMusuh1 = new javax.swing.JLabel();
        LabelMusuh2 = new javax.swing.JLabel();
        LabelMusuh3 = new javax.swing.JLabel();
        LabelBackground = new javax.swing.JLabel();

        setLayout(null);

        LabelPlayer.setText("LabelPlayer");
        add(LabelPlayer);
        LabelPlayer.setBounds(20, 50, 80, 20);

        LabelFire.setText("LabelFire");
        add(LabelFire);
        LabelFire.setBounds(20, 170, 63, 20);

        LabelMusuh1.setText("LabelMusuh1");
        add(LabelMusuh1);
        LabelMusuh1.setBounds(20, 80, 100, 20);

        LabelMusuh2.setText("LabelMusuh2");
        add(LabelMusuh2);
        LabelMusuh2.setBounds(20, 110, 100, 20);

        LabelMusuh3.setText("LabelMusuh3");
        add(LabelMusuh3);
        LabelMusuh3.setBounds(20, 140, 100, 20);

        LabelBackground.setText("LabelBackground");
        add(LabelBackground);
        LabelBackground.setBounds(20, 20, 120, 20);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelBackground;
    private javax.swing.JLabel LabelFire;
    private javax.swing.JLabel LabelMusuh1;
    private javax.swing.JLabel LabelMusuh2;
    private javax.swing.JLabel LabelMusuh3;
    private javax.swing.JLabel LabelPlayer;
    // End of variables declaration//GEN-END:variables
}
